import { StyleSheet } from 'react-native';
import { w, h, totalSize } from '../../utils/Dimensions';
import { UiColor, TextColor, TextSize } from '../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE,
  },
  textSignUp: {

    fontSize: TextSize.bigSize,
    fontWeight: 'bold',
  },
  textUser: {
    marginVertical: w(3.5),
    fontSize: TextSize.h3,
    fontWeight: '700',

    textAlign: "center"
  },
  container: {
    marginTop: w(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    backgroundColor: "#D6DCE0",
    borderRadius: w(2),
    width: w(42),
    height: h(7),
    marginBottom: w(4),
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN,
    marginHorizontal: w(1),
    alignItems: 'center', justifyContent: 'center'
  },
  inputs: {
    alignSelf: "center"
  },
  inputIcon: {
    width: w(3),
    height: h(3),
    marginLeft: w(3),
    justifyContent: 'center',
    tintColor: "#2295A1"
  },
  inputIcon1: {
    width: w(3),
    height: h(3),
    marginRight: w(80),
    justifyContent: 'center',
    tintColor: "#2295A1"
  },
  tabCard: {
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginLeft: w(2),
    marginRight: w(2),
    marginTop: 20,
    backgroundColor: "white",
    padding: 8,
    borderRadius: w(2),
    minHeight: h(10),
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: h(2)
  },
  card: {
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginLeft: w(2),
    marginRight: w(2),
    marginTop: 20,
    backgroundColor: "white",
    padding: 8,
    borderRadius: w(2),
    minHeight: h(10),
    alignItems: 'center',
    justifyContent: 'center',
  },
  card1: {
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginLeft: w(2),
    marginRight: w(2),
    backgroundColor: "white",
    padding: 8,
    borderRadius: w(2),
    minHeight: h(10),
    marginBottom: h(2)
  },

  linearGradient: {
    margin: w(5),
    height: h(7),
    width: w(65),
    alignItems: 'center',
    justifyContent: 'center',

    flexDirection: "row",
    borderRadius: w(8),
    alignSelf: "center"
  },

  signUpText: {
    textAlign: 'center',
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    fontWeight: 'bold',


  },
  textConfirmed: {
    color: UiColor.GREEN,
    fontSize: h(2.2),
    width: w(42),
    textDecorationLine: "underline",
    textAlign: 'center',
    fontWeight: 'bold',

  },
  textPrevious: {
    fontSize: h(2.2),
    color: UiColor.GRAY,
    width: w(42),
    textDecorationLine: "underline",
    textAlign: 'center',
    fontWeight: 'bold'
  }
});
