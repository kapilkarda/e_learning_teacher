import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import { API_BASE_URL } from "../../constant/constant";
import AsyncStorage from "@react-native-community/async-storage";
import { h, w } from "../../utils/Dimensions";
import Loader from '../../constant/Loader'

let confrmOpration = [];
let previousopration = [];

const Operations = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [confirmationTab, setconfirmationTab] = useState(true);
  const [preOprationTab, setpreOprationTab] = useState(false);
  const [tabDetails, settabDetails] = useState(false);
  const [pretabDetails, setpretabDetails] = useState(false);

  const [confirmOpration, setconfirmOpration] = useState([]);
  const [previousOperation, setpreviousOperation] = useState([]);
  const [isservice_selector, serviceselect] = useState("");
  const [isstage, stage] = useState("");
  const [issubject, subject] = useState("");
  const [isservice_type, service_type] = useState("");
  const [istime, time] = useState("");
  const [showdata, setShowData] = useState(false);

  const [ispreservice_selector, preserviceselect] = useState("");
  const [isprestage, prestage] = useState("");
  const [ispresubject, presubject] = useState("");
  const [ispreservice_type, preservice_type] = useState("");
  const [ispretime, pretime] = useState("");

  useEffect(() => {
    getCurrentRequest();
    // OprationConfirm()
    const unsubscribe = props.navigation.addListener('didFocus', () => {
      getCurrentRequest();
    });
    return () => unsubscribe;
  }, [props]);

  const getCurrentRequest = async () => {
    setIsLoading(true)
    let teacherId = await AsyncStorage.getItem("teacherId");
    fetch(API_BASE_URL + "/allOperation", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        // teacher_id : await AsyncStorage.getItem("teacherId"),
        teacher_id: teacherId
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log("response", response);
        previousopration = response.data.previous_operation;
        setconfirmOpration(response.data.confirm_operation);
        setpreviousOperation(previousopration);
      })
      .catch(error => {setIsLoading(false)});
  };

  const confirmationInfo = (index, item) => {
    settabDetails(true);
    serviceselect(item.service_selector);
    stage(item.stage);
    subject(item.subject);
    service_type(item.service_type);
    time(item.time);
  };

  const hideConfirmationInfo = () => {
    settabDetails(false);
    serviceselect("");
    stage("");
    subject("");
    service_type("");
    time("");
  };


  const previouInfo = (index, item) => {
    console.log(item);
    // setpretabDetails(true);
    // preserviceselect(item.service_selector);
    // prestage(item.stage);
    // presubject(item.subject);
    // preservice_type(item.service_type);
    // pretime(item.time);
  };

  const hidepreInfo = () => {
    setpretabDetails(false);
    preserviceselect("");
    prestage("");
    presubject("");
    preservice_type("");
    pretime("");
  };

  const ConfirmOprationTab = () => {
    setconfirmationTab(true);
    setpreOprationTab(false);
  };

  const previousOprationTab = () => {
    setconfirmationTab(false);
    setpreOprationTab(true);
  };

  const ShowHideComponent = (item) => {
    for (let oper of confirmOpration) {
      if (oper.request_id !== item.request_id) {
        oper.showData = false;
      }
    }
    if (item.showData !== true) {
      item.showData = true;
    } else if (item.showData === true) {
      item.showData = false;
    }
    if (showdata == false) {
      setShowData(true)
    } else {
      setShowData(false)
    }
  };
  const goToOperationConfirm = (item) => {
    console.log(item)
    Actions.OperationConf({item: item})
  }
  return (
    <View style={styles.mainContainer}>
      <Loader loading={isLoading} />
      {HeaderWithGoBackAndOption(Actions.Alert, "")}
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.textSignUp}>Operation</Text>
          <View style={styles.container}>
            <View style={styles.tabCard}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  width: w(88),
                  height: h(4),
                }}
              >
                <TouchableOpacity onPress={ConfirmOprationTab}>
                  <Text style={confirmationTab ? styles.textConfirmed : styles.textPrevious}>Confirmed operations</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={previousOprationTab}>
                  <Text style={preOprationTab ? styles.textConfirmed : styles.textPrevious}> Previous operations </Text>
                </TouchableOpacity>
              </View>
            </View>

            {/* ............confirmopration..................   */}
            {confirmationTab == true && (
              <View>
                {
                  confirmOpration.length == 0 &&
                  <Text style={{ marginTop: h(10) }}>
                    No data Found
                  </Text>
                }
                {confirmOpration.map((item, i) => (
                  <View key={i} style={styles.card1}>
                    <View style={{ flexDirection: "row" }}>
                      <View>
                        <Text style={styles.textUser}>Date</Text>
                        <View style={styles.inputContainer}>
                          <Text style={styles.inputs}>{item.date}</Text>
                        </View>
                      </View>
                      <View>
                        <Text style={styles.textUser}>Service Selector</Text>
                        <View style={styles.inputContainer}>
                          <Text style={styles.inputs}>
                            {item.service_selector}
                          </Text>
                        </View>
                      </View>
                    </View>
                    {(item.showData) ? (
                      <View style={{ minHeight: h(70) }}>
                        {/* <View style={{ flexDirection: "row" }}>
                          <View>
                            <Text style={styles.textUser}>Date</Text>
                            <View style={styles.inputContainer}>
                              <Text
                                style={styles.inputs}>
                                {item.date}</Text>
                            </View>
                          </View>
                          <View>
                            <Text style={styles.textUser}>Service Selector</Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.service_selector}</Text>
                            </View>
                          </View>
                        </View> */}
                        <View style={{ flexDirection: "row" }}>
                          <View>
                            <Text style={styles.textUser}>Stage</Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.stage}</Text>
                            </View>
                          </View>
                          <View>
                            <Text style={styles.textUser}>Subject</Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.subject}</Text>
                            </View>
                          </View>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                          <View>
                            <Text style={styles.textUser}>Service Type </Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.service_type}</Text>
                            </View>
                          </View>
                          <View>
                            <Text style={styles.textUser}>Time</Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.time}</Text>
                            </View>
                          </View>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                          <View>
                            <Text style={styles.textUser}>Mobile Number </Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.teacher_phone}</Text>
                            </View>
                          </View>
                        </View>
                        <TouchableOpacity onPress={() => goToOperationConfirm(item)}>
                          <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            colors={["#1c83a0", "#32cba5"]}
                            style={styles.linearGradient}
                          >
                            <Text style={styles.signUpText}>Direct operations</Text>
                          </LinearGradient>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={hideConfirmationInfo}>
                          <Image
                            style={styles.inputIcon}
                            source={IconAsset.ic_up}
                            resizeMode="contain"
                          />
                        </TouchableOpacity> */}
                      </View>
                    ) : null}
                    <TouchableOpacity
                      // onPress={() => confirmationInfo(i, item)}
                      onPress={() => ShowHideComponent(item)}
                    >
                      <Image
                        style={styles.inputIcon}
                        source={IconAsset.ic_down}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                ))}
              </View>
            )}
            {/* ............previousOperation..................   */}
            {preOprationTab == true && (
              <View>
                {
                  previousOperation.length == 0 &&
                  <Text style={{ marginTop: h(10) }}>
                    No data Found
                  </Text>
                }
                {previousOperation.map((item, i) => (
                  <View key={i} style={styles.card}>
                    <View style={{ flexDirection: "row" }}>
                      <View>
                        <Text style={styles.textUser}>Date</Text>
                        <View style={styles.inputContainer}>
                          <Text style={styles.inputs}>{item.date}</Text>
                        </View>
                      </View>
                      <View>
                        <Text style={styles.textUser}>Service Selector</Text>
                        <View style={styles.inputContainer}>
                          <Text style={styles.inputs}>
                            {item.service_selector}
                          </Text>
                        </View>
                      </View>
                    </View>
                    {(item.showData) ? (
                      <View style={{ minHeight: h(70) }}>
                        {/* <View style={{ flexDirection: "row" }}>
                          <View>
                            <Text style={styles.textUser}>Date</Text>
                            <View style={styles.inputContainer}>
                              <Text
                                style={styles.inputs}>
                                {item.date}</Text>
                            </View>
                          </View>
                          <View>
                            <Text style={styles.textUser}>Service Selector</Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.service_selector}</Text>
                            </View>
                          </View>
                        </View> */}
                        <View style={{ flexDirection: "row" }}>
                          <View>
                            <Text style={styles.textUser}>Stage</Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.stage}</Text>
                            </View>
                          </View>
                          <View>
                            <Text style={styles.textUser}>Subject</Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.subject}</Text>
                            </View>
                          </View>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                          <View>
                            <Text style={styles.textUser}>Service Type </Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.service_type}</Text>
                            </View>
                          </View>
                          <View>
                            <Text style={styles.textUser}>Time</Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.time}</Text>
                            </View>
                          </View>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                          <View>
                            <Text style={styles.textUser}>Mobile Number </Text>
                            <View style={styles.inputContainer}>
                              <Text style={styles.inputs}>{item.teacher_phone}</Text>
                            </View>
                          </View>
                        </View>
                        {/* <TouchableOpacity onPress={() => goToOperationConfirm(item)}>
                          <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            colors={["#1c83a0", "#32cba5"]}
                            style={styles.linearGradient}
                          >
                            <Text style={styles.signUpText}>Direct operations</Text>
                          </LinearGradient>
                        </TouchableOpacity> */}
                        {/* <TouchableOpacity onPress={hideConfirmationInfo}>
                          <Image
                            style={styles.inputIcon}
                            source={IconAsset.ic_up}
                            resizeMode="contain"
                          />
                        </TouchableOpacity> */}
                      </View>
                    ) : null}
                    <TouchableOpacity
                      // onPress={() => confirmationInfo(i, item)}
                      onPress={() => ShowHideComponent(item)}
                    >
                      <Image
                        style={styles.inputIcon1}
                        source={IconAsset.ic_down}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                    {/* <TouchableOpacity onPress={() => previouInfo(i, item)}>
                      <Image
                        style={styles.inputIcon}
                        source={IconAsset.ic_down}
                        resizeMode="contain"
                      />
                    </TouchableOpacity> */}
                  </View>
                ))}
              </View>
            )}

            {/* ............ConfiramtionOperationTabDetails..................   */}
            {
              tabDetails &&
              <View style={styles.card}>
                <View style={{ flexDirection: "row" }}>
                  <View>
                    <Text style={styles.textUser}>Date</Text>
                    <View style={styles.inputContainer}>
                      <Text
                        style={styles.inputs} />
                    </View>
                  </View>
                  <View>
                    <Text style={styles.textUser}>Service Selector</Text>
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputs}>{isservice_selector}</Text>
                    </View>
                  </View>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <View>
                    <Text style={styles.textUser}>Stage</Text>
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputs}>{isstage}</Text>
                    </View>
                  </View>
                  <View>
                    <Text style={styles.textUser}>Subject</Text>
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputs}>{issubject}</Text>
                    </View>
                  </View>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <View>
                    <Text style={styles.textUser}>Service Type </Text>
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputs}>{isservice_type}</Text>
                    </View>
                  </View>
                  <View>
                    <Text style={styles.textUser}>Time</Text>
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputs}>{istime}</Text>
                    </View>
                  </View>
                </View>

                <TouchableOpacity onPress={Actions.OperationConf}>
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={["#1c83a0", "#32cba5"]}
                    style={styles.linearGradient}
                  >
                    <Text style={styles.signUpText}>Direct operations</Text>
                  </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity onPress={hideConfirmationInfo}>
                  <Image
                    style={styles.inputIcon}
                    source={IconAsset.ic_up}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
            }

            {/* ............PrevioiusOperationTabDetails..................   */}
            {
              pretabDetails &&
              <View style={styles.card}>
                <View style={{ flexDirection: "row" }}>
                  <View>
                    <Text style={styles.textUser}>Date</Text>
                    <View style={styles.inputContainer}>
                      <Text
                        style={styles.inputs} />
                    </View>
                  </View>
                  <View>
                    <Text style={styles.textUser}>Service Selector</Text>
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputs}>{ispreservice_selector}</Text>
                    </View>
                  </View>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <View>
                    <Text style={styles.textUser}>Stage</Text>
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputs}>{isprestage}</Text>
                    </View>
                  </View>
                  <View>
                    <Text style={styles.textUser}>Subject</Text>
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputs}>{ispresubject}</Text>
                    </View>
                  </View>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <View>
                    <Text style={styles.textUser}>Service Type </Text>
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputs}>{preservice_type}</Text>
                    </View>
                  </View>
                  <View>
                    <Text style={styles.textUser}>Time</Text>
                    <View style={styles.inputContainer}>
                      <Text style={styles.inputs}>{ispretime}</Text>
                    </View>
                  </View>
                </View>

                <TouchableOpacity onPress={Actions.OperationConf({ Service_Selector: ispreservice_selector, stage: isstage, subject: issubject, service_type: isservice_type, time: istime })}>
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={["#1c83a0", "#32cba5"]}
                    style={styles.linearGradient}
                  >
                    <Text style={styles.signUpText}>Direct operations</Text>
                  </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity onPress={hidepreInfo}>
                  <Image
                    style={styles.inputIcon}
                    source={IconAsset.ic_up}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
            }
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
export default Operations;
