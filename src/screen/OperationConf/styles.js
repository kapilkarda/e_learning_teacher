import {StyleSheet} from 'react-native';
import {w, h, totalSize} from '../../utils/Dimensions';
import {UiColor, TextColor, TextSize} from '../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE,
  },
  textSignUp: {
    fontSize: TextSize.bigSize,
    fontWeight: 'bold',
  },
  headerView1: {
    backgroundColor: UiColor.WHITE,
  },
  headerImg: {
    width:"100%",
    height:h(10)
  },
  icon_menu: {
    tintColor: UiColor.GRAY,
    width: w(7),
    height: w(7),
    alignSelf: "center",
    marginLeft: w(3),
    marginTop:w(5)
  },
  icon_list_type: {
    width: w(9),
    height: w(9),
    alignSelf: "center",
    marginRight: w(3),
    tintColor: UiColor.GRAY,
    marginTop:w(5)
  },
  textUser: {
    marginVertical: w(3.5),
    fontSize: TextSize.h3,
    fontWeight: '700',
    textAlign:"center"
  },
  container: {
    marginTop: w(5),
    marginBottom: w(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    backgroundColor: "#D6DCE0",
    borderRadius: w(2),
    width: w(42),
    height: h(7),
    marginBottom: w(4),
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: w(0.3),
    borderColor:UiColor.GREEN,
    marginHorizontal:w(1),
    alignItems:'center',
    justifyContent:'center'
  },
  inputContainerTime: {
    backgroundColor: "#D6DCE0",
    borderRadius: w(2),
    width: w(56),
    height: h(7),
    marginBottom: w(4),
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: w(0.3),
    borderColor:UiColor.GREEN,
    marginHorizontal:w(1),
    alignItems:'center',
    justifyContent:'center'
  },
  inputContainerLike: {
    // backgroundColor: "#D6DCE0",
    borderRadius: w(2),
    width: w(30),
    height: h(7),
    marginBottom: w(4),
    flexDirection: 'row',
    alignItems: 'center',
    // borderWidth: w(0.3),
    // borderColor:UiColor.GREEN,
    marginHorizontal:w(1),
    alignItems:'center',
    justifyContent:'center'
  },
  inputs:{
    alignSelf:"center",
    color: '#333'
  },
  inputIcon: {
    width: w(3),
    height: h(3),
    marginLeft: w(3),
    justifyContent: 'center',
    tintColor:"#2295A1"
  },
  card:{
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,

    marginLeft: w(2),
    marginRight: w(2),
    marginTop:20,
    backgroundColor:"white",
    padding: 8,
    borderRadius:w(2),
  },
 
  linearGradient: {
    margin:w(5),
    height:h(6),
    width:w(40),
    alignItems:'center',
    justifyContent:'center',
 
    flexDirection:"row",
    borderRadius: w(8)
  },
  
  signUpText: {
   textAlign: 'center',
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    // fontWeight: 'bold',
     
   
  },
  textConfirmed:{
    color:UiColor.GREEN,
    fontSize:TextSize.h2,
    textDecorationLine:"underline",
    width:w(46)
  },
  textPrevious:{
    fontSize:TextSize.h2,
    color:UiColor.GRAY,
    width:w(44),
    textDecorationLine:"underline",
  },
  fev_icon: {
    width: w(10)
  },
  fev_icon_active: {
    width: w(10),
    tintColor: UiColor.GREEN
  }
});
