import React, { useState, useEffect } from 'react';
import {
  View,
  TextInput,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,Alert
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { IconAsset, Strings } from '../../theme';
import { HeaderWithGoBackAndOption ,HeaderWithBackLogin} from '../../components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';
import CountDown from 'react-native-countdown-component';
import moment from 'moment';
import CircularTimer from 'react-native-circular-timer';
import CountdownCircle from 'react-native-countdown-circle'
import Loader from '../../constant/Loader';
import { API_BASE_URL } from "../../constant/constant";
import AsyncStorage from "@react-native-community/async-storage";
import ProgressCircle from 'react-native-progress-circle';
import { w } from '../../utils/Dimensions';

class OperationConf extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      totalDuration: 0,
      itemData: props.item ? props.item: {},
      time: {}, seconds: 0,
      maxSecond: 0,
      startDisabled: false,
      stopDisabled: true,
      lessonStatus: '',
      favoriteStatus: '',
    };
    this.timer = 0;
    this.progress = 0;
    this.onButtonStart = this.onButtonStart.bind(this);
    this.onButtonStop = this.onButtonStop.bind(this);
    // this.onButtonClear = this.onButtonClear.bind(this);
    this.start = this.start.bind(this);
    this.countDown = this.countDown.bind(this);
  }
   componentDidMount() {
    const { navigation } = this.props;
    this.getCounterDetail()
    this.focusListener = navigation.addListener("didFocus", () => {
      this.getCounterDetail();
      console.log('sdfsdfdsfd')
    });
     }
   componentWillReceiveProps(props) {
    // console.log(props)
    if (props.item) {
      this.setState({
        itemData: props.item
      });
    }
  }
   componentWillUnmount(){
    clearInterval(this.state.timer);
  }
   start() {
    let timeLeftVar = this.secondsToTime(this.state.seconds);
    this.setState({ time: timeLeftVar });
  }
   secondsToTime(secs){
    let hours = Math.floor(secs / (60 * 60));
    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);
    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);
    var hDisplay = hours > 0 ? (hours < 10 ? "0" + hours + ":" : hours + ":") : "00:";
    var mDisplay = minutes > 0 ? (minutes < 10 ? "0" + minutes + ":" : minutes + ":") : "00:";
    var sDisplay = seconds > 0 ? (seconds < 10 ? "0" + seconds : seconds) : "00";
    let obj = {
      "h": hDisplay,
      "m": mDisplay,
      "s": sDisplay
    };
    return obj;
  }
   countDown(TimeStatus) {
    console.log(this.state.seconds)
    let seconds = this.state.seconds - 1;
    this.progress = Math.ceil((seconds / this.state.maxSecond) * 100)
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });
    if (seconds == 0) { 
      this.startStopCounter(TimeStatus)
      clearInterval(this.timer);
    }
  }
  
   onButtonStart(TimeStatus) {
    console.log(this.state.seconds)
    this.setState({startDisabled: true, stopDisabled: false});
    if (this.state.seconds > 0) {
      this.timer = setInterval(() => {this.countDown(TimeStatus)}, 1000);
    }
  }
   onButtonStop(TimeStatus) {
    clearInterval(this.timer);
    this.setState({startDisabled: false, stopDisabled: true});
    this.startStopCounter(TimeStatus)
  }
   startStopCounter = async (TimeStatus) => {
     console.log(TimeStatus,"TimeStatus")
    this.setState({
      isLoading: true
    })
    let teacherId = await AsyncStorage.getItem("teacherId");
    console.log(this.state.itemData.request_id,"request_id")
    console.log(toString(this.state.seconds),"second")
    fetch(API_BASE_URL + "/startStopLesson", {
       method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({ 
        "remaning_time" : this.state.seconds,
        // "remaning_time" : toString(this.state.seconds
        "request_id" : this.state.itemData.request_id,
        "teacher_id" : teacherId,
        lesson_status: TimeStatus
      })
    })
      .then(res => res.json())
      .then(response => {
        this.setState({
          isLoading: false
        })
      if (response.result == 'true') {
          Actions.Operations()
        }
      })
      .catch(error => {
        this.setState({
          isLoading: false
        })
        console.log(error)
      });
  };
 getCounterDetail = async () => {
    this.setState({
      isLoading: true
    })
    let teacherId = await AsyncStorage.getItem("teacherId");
    fetch(API_BASE_URL + "/getRequestTime", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        "request_id" : this.state.itemData.request_id,
        "teacher_id" : teacherId
      })
    })
      .then(res => res.json())
      .then(response => {
        this.setState({
          isLoading: false
        })
        console.log(response)
        if (response.result == 'true') {
          if (response.data.lesson_status == '0' || response.data.lesson_status == '1') {
            console.log(Number(response.data.remaning_time))
            this.setState({
              seconds: Number(response.data.remaning_time),
              maxSecond: Number(response.data.total_time),
              // seconds: Number(10000),
              // maxSecond: Number(10800),
              time: this.secondsToTime(Number(response.data.remaning_time)),
              startDisabled: false,
              stopDisabled: true,
              lessonStatus: response.data.lesson_status,
              favoriteStatus: response.data.favourite_status
            },() => {
              this.progress = Math.ceil((this.state.seconds / this.state.maxSecond) * 100)
            })
          } else {
            this.setState({
              lessonStatus: response.data.lesson_status,
              favoriteStatus: response.data.favourite_status
            })
          }
        }
      })
      .catch(error => {
        this.setState({
          isLoading: false
        })
        console.log(error)
      });
  };
  
 
 stopWatchPop = (TimeStatus) => {
    this.onButtonStop(TimeStatus)
   
  }
  handleBackButton = (TimeStatus) => {
    Alert.alert(
      "Log out",
      "Are you sure you want to stop lesson?",
      [
        { text: "cancel", onPress: () => console.log("Cancel Pressed") },
        { text: "OK", onPress: () => {
          this.stopWatchPop(TimeStatus)
        }}
      ],
      { cancelable: false }
    );
    return true;
  };
  render() {
    const { itemData, favoriteStatus } = this.state;
    return (
      <View style={styles.mainContainer}>
        <Loader loading={this.state.isLoading} />
        {/* {HeaderWithBackLogin(Actions.Operations, '')} */}
        <View style={[styles.headerView1]}>
          <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 0}}  colors={['#1c83a0', '#32cba5', ]} style={styles.headerImg}>
          </LinearGradient>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}  >
          <View style={styles.icon_menu}></View>
            <View>
              <TouchableOpacity 
               onPress={() => this.handleBackButton()}>
           
                <Image
                  source={IconAsset.ic_right}
                  style={styles.icon_list_type}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <ScrollView>
          <View style={styles.container}>
            <Text style={styles.textSignUp}>Confirm Operation</Text>
             <View style={styles.container}>
            {
              itemData && itemData.student_name &&
              <View style={styles.card}>
                <View style={{ flexDirection: 'row' }}>
                  <View>
                    <Text style={styles.textUser}>Date</Text>
                    <View style={styles.inputContainer}>
                       <TextInput
                        style={styles.inputs}
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        value={moment(itemData.date).format('DD MMMM YYYY')}
                        editable={false}
                      />
                    </View>
                  </View>
                  <View>
                    <Text style={styles.textUser}>Service Selector</Text>
                    <View style={styles.inputContainer}>
                      <TextInput
                        style={styles.inputs}
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                         value={itemData.service_selector}
                        editable={false}
                      />
                    </View>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                 <View>
                    <Text style={styles.textUser}>Subject</Text>
                    <View style={styles.inputContainer}>
                      <TextInput
                        style={styles.inputs}
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        value={itemData.subject}
                        editable={false}
                      />
                    </View>
                  </View>
                  <View>
                    <Text style={styles.textUser}>Service Type </Text>
                    <View style={styles.inputContainer}>
                      <TextInput
                        style={styles.inputs}
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                         value={itemData.service_type}
                        editable={false}
                      />
                    </View>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View>
                    <Text style={styles.textUser}>Time</Text>
                    <View style={styles.inputContainerTime}>
                      <TextInput
                        style={styles.inputs}
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                         value={itemData.time}
                        editable={false}
                      />
                    </View>
                  </View>
                 
                </View>
              </View>
            }
               {
                this.state.lessonStatus == '1' || this.state.lessonStatus == '0' ? 
                <View>
                  <View style={{ marginTop: 40, alignItems: 'center'}}>
                     <ProgressCircle
                        percent={this.progress}
                        radius={70}
                        borderWidth={12}
                        color="#2295a1"
                        shadowColor="#e5e8eb"
                        bgColor="#fff">
                        <Text style={{ fontSize: 16, color:"#2295a1", fontWeight: 'bold' }}>{this.state.time.h}{this.state.time.m}
                    {this.state.time.s} sec</Text>
                    </ProgressCircle>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-evenly',
                      marginLeft: 40,
                      marginRight: 40,
                    }}>
                      {
                      this.state.stopDisabled == false &&
                      <TouchableOpacity onPress={() =>this.handleBackButton("2")} disabled={this.state.stopDisabled}>
                      <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#1c83a0', '#32cba5']}
                        style={styles.linearGradient}>
                        <Text style={styles.signUpText} >Stop a lesson</Text>
                      </LinearGradient>
                    </TouchableOpacity>
                   }
                    {
                      this.state.stopDisabled &&
                    <TouchableOpacity onPress={() =>this.onButtonStart("1")}  disabled={this.state.startDisabled}>
                      <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#1c83a0', '#32cba5']}
                        style={styles.linearGradient}>
                        <Text style={styles.signUpText}>Start a lesson</Text>
                      </LinearGradient>
                    </TouchableOpacity>}
                  </View>
                </View>
                :
               null
              }
              {
                this.start.lessonStatus == '2' &&
                <View style={{ marginTop: 40, marginBottom: 30, }}>
                  <Text style={{fontSize: w(6), color: '#2295a1', fontWeight: 'bold'}}>Session Completed</Text>
                </View>
              }
             </View>
          </View>
        </ScrollView>
      </View>
    );
  };
}
export default OperationConf;
