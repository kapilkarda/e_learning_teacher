import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Alert,
  Platform,
  Linking
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
import { AirbnbRating } from "react-native-ratings";
import styles from "./styles";
import { API_BASE_URL } from "../../constant/constant";
import AsyncStorage from "@react-native-community/async-storage";
import { w, h } from "../../utils/Dimensions";
const GOOGLE_PACKAGE_NAME = 'com.whatsapp';
const APPLE_STORE_ID = 'id284882215';
const AppRating = () => {
  const [ DefaultRating, setDefaultRating] = useState(1);
  // const [MaxRating, setMaxRating] = useState(5);

  const ratingCompleted = (e) => {
    console.log(e)
    setDefaultRating(e);
    ratingApp(e)
  }
  const ratingStart = () => {
    //Initialize count by 5 to start counter for 5 sec
    Alert.alert(
      'Rate us',
      'Would you like to share your review with us? This will help and motivate us a lot.',
      [
        { text: 'Sure', onPress: () => openStore() },
        {
          text: 'No Thanks!',
          onPress: () => console.log('No Thanks Pressed'),
          style: 'cancel',
        },
      ],
      { cancelable: false }
    );
  };
  const openStore = () => {
    if (Platform.OS != 'ios') {
      Linking.openURL(`market://details?id=${GOOGLE_PACKAGE_NAME}`).catch(err =>
        alert('Please check for the Google Play Store')
      );
    } else {
      Linking.openURL(
        `itms://itunes.apple.com/in/app/apple-store/${APPLE_STORE_ID}`
      ).catch(err => alert('Please check for the App Store'));
    }
  };
  const ratingApp = async code => {
    let teacherII = await AsyncStorage.getItem("teacherId");
    fetch(API_BASE_URL + "/rating_app", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        type : "teacher",
        rate : code,
        user_id : teacherII
      })
    })
      .then(res => res.json())
      .then(response => {
        if (response.result == "true") {
          console.log("response", response);
          Alert.alert(
            'Alert',
            response.msg,
            [
              { text: 'OK', onPress: () => Actions.Settings},
            ],
            { cancelable: false },
          );
        } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
  };
  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
      <View style={styles.mainContainer}>
        <ScrollView>
          {HeaderWithGoBackAndOption(Actions.Settings, "")}
          <View style={styles.container}>
            <Text style={styles.textSignUp}> Rate App</Text>
            <Text style={styles.textRate}>
              {" "}
              Rate this app and tell others what you think
            </Text>
            <View style={styles.img}>
              
              <AirbnbRating
                count={5}
                reviews={[]}
                style={{ paddingVertical: 10 }}
                defaultRating={5}
                isDisabled= {true}
                // onFinishRating={ratingCompleted}
                size={30}
                selectedColor= '#2295a1'
              />
              
            </View>
            <View style={{marginTop: w(10)}}>
              <TouchableOpacity
                onPress={() => {
                  ratingStart();
                }}
                style={styles.okBtn}
              >
                <Text style={styles.okText}>Give Rating</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};
export default AppRating;
