import {StyleSheet} from 'react-native';
import {w, h, totalSize} from '../../utils/Dimensions';
import {UiColor, TextColor, TextSize} from '../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE,
  },
  textSignUp: {
   
    fontSize: TextSize.bigSize,
    fontWeight: 'bold',
  },
  textUser: {
    marginVertical: w(3.5),
    fontSize: TextSize.h1,
    fontWeight: '200',
  },
  container: {
    marginTop: w(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  starimg: {
       marginTop:w(5),
       width:w(12),
       height:h(2),
    alignSelf:"center"
  },
  textRate:{
    marginTop:w(5),

  },
  img:{
    flexDirection:"row",marginTop:15,tintColor:"#2295A1"

  },

  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
  },
  childView: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 30,
  },
  button: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 30,
    padding: 15,
    backgroundColor: '#8ad24e',
  },
  StarImage: {
    width: 40,
    height: 40,
    resizeMode: 'cover',
  },
  textStyle: {
    textAlign: 'center',
    fontSize: 23,
    color: '#000',
    marginTop: 15,
  },
  textStyleSmall: {
    textAlign: 'center',
    fontSize: 16,
    color: '#000',
    marginTop: 15,
  },
  okBtn: {
    height: h(6),
    width: w(40),
    backgroundColor: UiColor.GREEN,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginTop: h(3),
    borderRadius: 5
  },
  okText: {
    color: "#fff",
    fontSize: h(3),
    fontWeight: "bold"
  },

});
