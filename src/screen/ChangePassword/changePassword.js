import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings} from '../../theme';
import {HeaderWithGoBackAndOption} from '../../components/AppHeader';
import styles from './styles';
import Loader from '../../constant/Loader';
import AsyncStorage from "@react-native-community/async-storage";
import { API_BASE_URL } from "../../constant/constant";

const ChangePassword = (props) => {
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const validation = () => {
    handleCnfPass(oldPassword,newPassword, confirmPassword);
  };
  const handleCnfPass = (oldPassword,newPassword, confirmPassword) => {
    if (oldPassword == "") {
      alert("Please enter your current password");
    }  else if (newPassword == ""){
      alert("Please enter new password");
    } else if (newPassword.length < 6){
      alert("Please enter minimum 6 digit password");
    }  else if (confirmPassword == ""){
      alert("Please enter confirm new password");
    }  else if (confirmPassword != newPassword){
      alert("New password and confirm new password doesn't match");
    }
    else {
      serverApi(oldPassword,newPassword)
    }
  };
  const serverApi = async (oldPassword,newPassword) => {
    setIsLoading(true)
    const teacherId = await AsyncStorage.getItem("teacherId");
    console.log("serverApoii");
    fetch(API_BASE_URL + "/changePassword", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        teacher_id: teacherId,
        old_password: oldPassword,
        new_password: newPassword
      })
    })
      .then(res => res.json())
      .then(response => {
        console.log(response)
        if (response.result == "true") {
          console.log("response", response);
          // Actions.Login();
          Alert.alert(
            'Alert',
            response.msg,
            [
              { text: 'OK', onPress: () => {
                clearAsyncStorage()
              } },
            ],
            { cancelable: false },
          );
          setIsLoading(false)
        } else {
          alert(response.msg);
          setIsLoading(false)
        }
      })
      .catch(error => {
        setIsLoading(false)
      });
  };
  const clearAsyncStorage = async() => {
    await AsyncStorage.clear();
    Actions.Login()
  }
  return (
    
      <View style={styles.mainContainer}>
        <Loader loading={isLoading} />
        <ScrollView>
        {HeaderWithGoBackAndOption(Actions.Settings,"")}
          <View style={styles.container}>
            <Text style={styles.textSignUp}>Change Password</Text>
            <Text style={styles.textUser}>Current password</Text>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_lock}
                resizeMode="contain"
              />
              {/**********
             Password
             ***********/}

              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                secureTextEntry={true}
                onChangeText={password => setOldPassword(password)}
                value={oldPassword}
              />
            </View>
            <Text style={styles.textUser}>New password</Text>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_lock}
                resizeMode="contain"
              />
              {/**********
             Password
             ***********/}

              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                secureTextEntry={true}
                onChangeText={password => setNewPassword(password)}
                value={newPassword}
              />
            </View>
            <Text style={styles.textUser}>Confirm new password</Text>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_lock}
                resizeMode="contain"
              />
              {/**********
               Confirm Password
              ***********/}
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                secureTextEntry={true}
                underlineColorAndroid="transparent"
                onChangeText={password => setConfirmPassword(password)}
                value={confirmPassword}
              />
            </View>

            {/**********
              Enter Arrow
             ***********/}
             <TouchableOpacity onPress={validation}>
            <Image
              style={styles.inputArrowIcon}
              source={IconAsset.ic_right}
              resizeMode="contain"
            />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
  
  );
};
export default ChangePassword;
