import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  ImageBackground,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";

import styles from "./styles";

const SelectRegister = () => {
  const [mobile, setmobile] = useState("");
  // var TriangleCornerBottomRight = React.createClass
  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
      <View style={styles.mainContainer}>
        <ImageBackground
          source={require("../../asssets/icons/2.png")}
          style={{ width: "100%", height: "100%", justifyContent: "center" }}
        >
          <Text style={styles.textRegister}>Select Register</Text>

          <View style={styles.container}>
            <TouchableOpacity onPress={Actions.Login}>
              <Image
                source={require('../../asssets/icons/Teacher.png')}
              
              />

              <View style={styles.titlecontainer}>
                <Text style={styles.title}>Teacher</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.Login}>
              <Image source={require("../../asssets/icons/student.png")} />
              <View style={styles.titlecontainer}>
                <Text style={styles.title}>Student</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    </KeyboardAvoidingView>
  );
};
export default SelectRegister;
