import { StyleSheet } from "react-native";
import { w, h, totalSize } from "../../utils/Dimensions";
import { UiColor, TextColor, TextSize } from "../../theme";

export default StyleSheet.create({
  mainContainer: {
    flex: 1
  },

  container: {
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    marginVertical: w(22),
    marginLeft: w(5),
    marginRight: w(5)
  },
  textRegister: {
    fontSize: 28,
    alignSelf: "center",
    textAlign: "center",
    color: UiColor.GREEN
  },
  titlecontainer: {
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    alignSelf: "center",
    top: w(16),
    left: w(5.2)
  },
  title: {
    fontSize: w(8),
    fontWeight: "800",
    color: UiColor.WHITE
  }
});
