import React from "react";
import { View, Text, ScrollView, KeyboardAvoidingView, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";
import { HeaderWithOption } from "../../components/AppHeader";
import styles from "./styles";
import { API_BASE_URL } from "../../constant/constant";
import AsyncStorage from "@react-native-community/async-storage";
import { w, h } from "../../utils/Dimensions";
import Loader from '../../constant/Loader';
import moment from 'moment';

class AlertComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      newArray: [],
      teacherId: '',
      notify: '',
      newTime: '',
      isLoading: false
    };
  }

  componentWillMount() {
    const { navigation } = this.props;
    this.getTeacherid();
    this.focusListener = navigation.addListener("didFocus", () => {
      this.getTeacherid();
      console.log('sdfsdfdsfd')
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.notify == 'notify') {
      this.getTeacherid();
    }
  }

  getTeacherid = async () => {
    let teacherId = await AsyncStorage.getItem("teacherId");
    let notifyme = await AsyncStorage.getItem("notify");
    console.log("teacherId", teacherId);
    if (teacherId) {
      this.setState({
        teacherId: teacherId,
        notify: notifyme
      }, () => {
        this.AlertApi();
        
      })
    }
  };

  AlertApi = () => {
    this.setState({
      isLoading: true
    })
    fetch(API_BASE_URL + "/getAlert", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        "type": "Teacher",
        "id": this.state.teacherId
      })
    })
      .then(res => res.json())
      .then(response => {
        // this.setState({
        //   isLoading: false
        // })
        console.log(response, "response")
        if (response.result == 'true') {
          this.setState({
            newArray: response.data,
            isLoading:false
          })
        }
        else{
          this.setState({
            isLoading:false,
            newArray: [],
          })
        }
        
      })
      .catch(error => {
        this.setState({
          isLoading: false
        })
        console.log(error)
      });
  };

  goToRequest = (item) => {
    console.log('allitem', item)
    if (item.type == "Confirm Request") {
      Actions.Operations();
    } else {
      Actions.Request({ requestId: item.request_id })
    }
    // Actions.Request({ requestId: item.request_id })
  }

  newTimeArray = (item) => {
    let daaa = item.created_at;
    let newTime = moment(daaa).format('dddd DD MMM YYYY');
    return newTime;
  }
  newDateArray = (item) => {
    
    let daaa = item.created_at;
    let newTime = moment(daaa).format('LT');
    return newTime;
  }
  componentWillUnmount() {
    this.focusListener.remove();
    console.log('unnnnnnnnnnnnnnnnnnnnnnnnnnn');
  }
  render() {
    return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
        <View style={styles.mainContainer}>
          <Loader loading={this.state.isLoading} />
          {HeaderWithOption()}
          <ScrollView>
            <View style={styles.container}>
              <Text style={styles.textSignUp}>Alerts </Text>
            </View>

            <View style={{ marginBottom: h(2) }}>
              {this.state.newArray.map((item, i) => (
                <View key={i} style={styles.card}>
                  <TouchableOpacity onPress={() => this.goToRequest(item)}>
                    <View>
                      <Text style={styles.itemDescripation}>
                        {item.message}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: w(85),
                        marginTop: 15,
                        flexDirection: "row",
                        justifyContent: "space-between"
                      }}>
                      <Text style={styles.itemDay}>{item.date}</Text>
                      <Text style={styles.itemDay}>{item.time}</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              ))}
              {
                this.state.newArray.length === 0 &&
                <Text style={{textAlign: 'center', marginTop: 20}}>No alert found</Text>
              }
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  };
}
export default AlertComponent;
