import {StyleSheet} from 'react-native';
import {w, h, totalSize} from '../../utils/Dimensions';
import {UiColor, TextColor, TextSize} from '../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE,
  },
  textSignUp: {
   
    fontSize: TextSize.bigSize,
    fontWeight: 'bold',
  },
  textUser: {
    marginVertical: w(3.5),
    fontSize: TextSize.h1,
    fontWeight: '200',
  },
  container: {
    marginTop: w(10),
    justifyContent: 'center',
    alignItems: 'center',
    flex:1
  },
  card:{
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,

    marginLeft: 20,
    marginRight: 20,
    marginTop:20,
    backgroundColor:"white",
    padding: 8,
    flexDirection:'row',
    borderRadius:w(2),
  },
  itemDescripation:{
    // width:w(80),
    // height:h(8)
  },
  itemDay:{
    marginHorizontal:w(1),
    color:UiColor.GREEN
    
  },
  

});
