import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Alert
} from "react-native";
import { Actions } from "react-native-router-flux";
import LinearGradient from "react-native-linear-gradient";
import { IconAsset } from "../../theme";
import { HeaderWithBackLogin } from "../../components/AppHeader";
import styles from "./styles";
import Loader from "../../constant/Loader"
import { API_BASE_URL } from "../../constant/constant";
import AsyncStorage from "@react-native-community/async-storage";
const SignUp = (props) => {

  const [userName, setuserName] = useState("");
  const [mobile, setmobile] = useState("");
  const [locale, setlocale] = useState("en");
  const [isLoading, setIsLoading] = useState(false);
  
  const validation = () => {
      if (userName == "") {
        alert("Please enter Username");
      } else if (mobile == "") {
        alert("Please enter Mobile No");
      } else {
        setIsLoading(true);
        fetch(API_BASE_URL + "/teacher_signup", {
          method: "POST",
          headers: new Headers({
            "Content-Type": "application/json",
            Accept: "application/json"
          }),
          body: JSON.stringify({
            username: userName,
            usermobile: mobile,
            device_type: 'android',
            device_token: "vcbgvhngjghj"
          })
        })
        .then(res => res.json())
        .then(response => {
          setIsLoading(false);
          if (response.result == "true") {
            console.log("response", response.data.status);
            AsyncStorage.setItem('teacherId', response.data.teacher_id);
            AsyncStorage.setItem('teacherPhone', response.data.teacher_phone);
            AsyncStorage.setItem("status", response.data.status)
            Alert.alert(
              'Alert',
              response.msg,
              [
                { text: 'OK', onPress: () => Actions.otpVerifed() },
              ],
              { cancelable: false },
            );
          } else {
            alert(response.msg);
          }
        })
        .catch(error => {
          setIsLoading(false);
          console.log(error)
        });
      }
  };


  return (
    
    <View style={styles.mainContainer}>
      {HeaderWithBackLogin(Actions.Login, "")}
      {/* {isLoading && <Spinner />} */}
      <Loader loading={isLoading} />
      <ScrollView>
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
          <View style={styles.container} locale={locale}>
            <Text style={styles.textSignUp} locale={locale}>
              SignUp
            </Text>

            {/* User Name */}

            <Text style={styles.textUser}>User Name</Text>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_user}
                resizeMode="contain"
              />
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                onChangeText={userName => setuserName(userName)}
                value={userName}
              />
            </View>

            {/* Mobile Number */}

            <Text style={styles.textUser}>Mobile number</Text>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_mobile}
                resizeMode="contain"
              />
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                keyboardType="number-pad"
                maxLength={10}
                underlineColorAndroid="transparent"
                onChangeText={mobile => setmobile(mobile)}
                value={mobile}
              />
            </View>
            <View style={{}}>
              <TouchableOpacity onPress={validation}>
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                  colors={["#1c83a0", "#32cba5"]}
                  style={styles.linearGradient}
                >
                  <Text style={styles.signUpText}>Sign Up</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};
export default SignUp;
