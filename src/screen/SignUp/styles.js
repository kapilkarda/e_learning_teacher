import { StyleSheet } from "react-native";
import { w, h, totalSize } from "../../utils/Dimensions";
import { UiColor, TextColor, TextSize } from "../../theme";

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE
  },
  textSignUp: {
    marginVertical: w(5),
    fontSize: TextSize.bigSize,
    fontWeight: "bold"
  },
  textUser: {
    marginVertical: w(3.5),
    fontSize: TextSize.h1,
    fontWeight: "400"
  },
  container: {
    marginTop: w(5),
    justifyContent: "center",
    alignItems: "center"
  },
  inputContainer: {
    backgroundColor: UiColor.WHITE,
    borderRadius: w(8),
    width: w(65),
    height: h(7),
    marginBottom: w(4),
    flexDirection: "row",
    alignItems: "center",
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN
  },

  inputIcon: {
    width: w(4),
    height: h(4),
    marginLeft: w(3),
    justifyContent: "center",
    tintColor: "#1A7B9F"
  },
  buttonContainer: {
    height: h(7),
    width: w(60),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: w(5)
  },

  signupButton: {
    marginTop: w(5),
    backgroundColor: UiColor.GREEN
  },
  linearGradient: {
    margin: w(10),
    height: h(7),
    width: w(65),
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: "row",
    borderRadius: w(8)
  },

  signUpText: {
    textAlign: "center",
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    fontWeight: "bold"
  },
  inputs: {
    width: w(53),
    marginLeft: w(3)
  }
});
