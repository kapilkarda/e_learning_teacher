import serverCall from "./serverCall";

const handleCnfPass = (password,cpassword) => {
  if (password == "") {
    alert("Please enter password");
  } else if (password.length < 6) {
    alert("Please enter minimum 6 digit password");
  } else {
    serverCall.serverApi(password,cpassword)
  }
};

export default {
  handleCnfPass
};