import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings} from '../../theme';
import {HeaderWithGoBackAndOption} from '../../components/AppHeader';
import LinearGradient from "react-native-linear-gradient";
import styles from './styles';
import serverCall from "./serverCall";


const ConfirmPass = () => {
  const [password, setpassword] = useState('');
  const [cpassword, setcpassword] = useState('');

  const conPassword = () => {
    serverCall.serverApi();
  };

  return (

      <View style={styles.mainContainer}>
        <ScrollView>
        {HeaderWithGoBackAndOption(Actions.Resetpass,"")}
          <View style={styles.container}>
            
            <Text style={styles.textSignUp}>confirm password</Text>
            <Text style={styles.textUser}>
              Enter a number to confirm
            </Text>
            <View style={styles.inputContainer}>
             
              {/**********
               Confirm Password
              ***********/}
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                keyboardType="number-pad"
                secureTextEntry={true}
                underlineColorAndroid="transparent"
                onChangeText={cpassword => setcpassword(cpassword)}
                value={cpassword}
              />
            </View>

            {/**********
              LoginButton
             ***********/}
            <TouchableOpacity
            
              onPress={conPassword}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={["#1c83a0", "#32cba5"]}
                style={styles.linearGradient}
              >
              <Text style={styles.signUpText}>{Strings.CONFIRMATION}</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>

  );
};
export default ConfirmPass;
