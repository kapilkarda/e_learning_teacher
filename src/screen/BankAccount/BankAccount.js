import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Alert
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import Loader from '../../constant/Loader';
import { API_BASE_URL } from "../../constant/constant";
import AsyncStorage from "@react-native-community/async-storage";

const BankAccount = (props) => {
  const [bankName, setBankName] = useState("");
  const [accountNumber, setAccountNumber] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getAccount();
    const unsubscribe = props.navigation.addListener('didFocus', () => {
      getAccount();
    });
    return () => unsubscribe;
  }, []);
  const getAccount = async () => {
    const teacherId = await AsyncStorage.getItem("teacherId");
    setIsLoading(true)
    fetch(API_BASE_URL + "/getTeacherBankDetails", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        teacher_id: teacherId,
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log(response,"jkjkrfjfkfk");
        if (response.result == "true") {
          setBankName(response.data.bankname)
          setAccountNumber(response.data.bank_account_no)
        } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
  };
  const updateBankAcount = async () => {
    if (bankName == "") {
      alert("Please enter bank name");
    } else if (accountNumber == "") {
      alert("Please enter account number");
    } else {
      const teacherId = await AsyncStorage.getItem("teacherId");
      setIsLoading(true)
      fetch(API_BASE_URL + "/updateBankAccount", {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/json",
          Accept: "application/json"
        }),
        body: JSON.stringify({
          teacher_id: teacherId,
          bankname: bankName,
          bank_account_no: accountNumber,
        })
      })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log(response);
        if (response.result == "true") {
          Alert.alert(
            'Alert',
            response.msg,
            [
              { 
                text: 'OK', onPress: () => {
                  
                  Actions.Settings()
                } 
              },
            ],
            { cancelable: false },
          );
        } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
    }
  };
  return (
   
      <View style={styles.mainContainer}>
        <Loader loading={isLoading} />
        <ScrollView>
          {HeaderWithGoBackAndOption(Actions.Settings,"")}
          <View style={styles.container}>
            <Text style={styles.textSignUp}>Update the bank account</Text>
            <View style={{marginTop:32}}>
            <Text style={styles.textUser}>Bank Name</Text>
            </View>
            <View style={styles.inputContainer}>

              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                // placeholder={"Enter number"}
                onChangeText={bankName => setBankName(bankName)}
                value={bankName}
              />
            </View>
            <Text style={styles.textUser}>Account number</Text>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                keyboardType="number-pad"
                underlineColorAndroid="transparent"
                onChangeText={accountNumber => setAccountNumber(accountNumber)}
                value={accountNumber}
              />
            </View>
            
            <TouchableOpacity onPress={updateBankAcount}>
              {/* <Image source={require("../../asssets/icons/button.png")} /> */}
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={["#1c83a0", "#32cba5"]}
                style={styles.linearGradient}
              >
               <Text style={styles.signUpText}>Update</Text>

              </LinearGradient>
              
            </TouchableOpacity>
            
          
          </View>
        </ScrollView>
      </View>
    
  );
};
export default BankAccount;
