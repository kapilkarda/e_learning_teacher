import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Switch,
  Share,
  Alert 
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithOption } from "../../components/AppHeader";
import Loader from '../../constant/Loader';
import { API_BASE_URL } from "../../constant/constant";
import AsyncStorage from "@react-native-community/async-storage";

import styles from "./styles";

const Settings = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [notifications, setNotifications] = useState(false);
  const [notificationStatus, setNotificationStatus] = useState('');

  const notificationToggle = (e) => {
    setNotifications(e)
    updateNotification(e);
  }
  useEffect(() => {
    getAccount();
    const unsubscribe = props.navigation.addListener('didFocus', () => {
      getAccount();
    });
    return () => unsubscribe;
  }, []);
  const getAccount = async () => {
    const teacherId = await AsyncStorage.getItem("teacherId");
    setIsLoading(true)
    fetch(API_BASE_URL + "/getTeacherBankDetails", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        teacher_id: teacherId,
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        if (response.result == "true") {
          setNotificationStatus(response.data.notification_type);
          if (response.data.notification_type == 'enable') {
            setNotifications(true)
          } else {
            setNotifications(false)
          }
        } else {
          alert(response.msg);
        }
      })
      .catch(error =>{setIsLoading(false); console.log(error)});
  };
  const updateNotification = async (status) => {
    const teacherId = await AsyncStorage.getItem("teacherId");
    setIsLoading(true)
    fetch(API_BASE_URL + "/notificationOnOff", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        teacher_id: teacherId,
        status: status === true ? 'ON': 'OFF',
      })
    })
    .then(res => res.json())
    .then(response => {
      setIsLoading(false)
      getAccount();
      if (response.result == "true") {
        Alert.alert(
          'Alert',
          response.msg,
          [
            { 
              text: 'OK'
            },
          ],
          { cancelable: true },
        );
      } else {
        alert(response.msg);
      }
    })
    .catch(error => { setIsLoading(false); console.log(error)});
  };

  const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          'http://google.com | A framework for building native apps using React'
      },
      {
        // Android only:
        dialogTitle: 'Share BAM goodness',
        // iOS only:
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToTwitter'
        ]
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
      <Loader loading={isLoading} />
      <View style={styles.mainContainer}>
        <ScrollView>
          {HeaderWithOption()}
          <View style={styles.container}>
            <Text style={styles.textSignUp}>Settings </Text>
            <TouchableOpacity onPress={Actions.AccountSetting}>
           <View style={styles.borderContainer}>
              <View style={{ flexDirection: "row" }}>
              <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_edit
                  }
                  resizeMode="contain"
                />
                <Text style={styles.textContainer}>Account Settings </Text>
              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Image
                  style={styles.arrrowIcon}
                  source={IconAsset.ic_rights}
                  resizeMode="contain"
                />
              </View>
            </View>
           </TouchableOpacity>
           <TouchableOpacity onPress={Actions.BankAccount}>
          <View style={styles.borderContainer}>
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_edit}
                  resizeMode="contain"
                />
                <Text style={styles.textContainer}>Update the bank account </Text>
              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Image
                  style={styles.arrrowIcon}
                  source={IconAsset.ic_rights}
                  resizeMode="contain"
                />
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={Actions.ChangePassword}>
           <View style={styles.borderContainer}>
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_edit}
                  resizeMode="contain"
                />
                <Text style={styles.textContainer}>Change Password </Text>
              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Image
                  style={styles.arrrowIcon}
                  source={IconAsset.ic_rights}
                  resizeMode="contain"
                />
              </View>
            </View>
           </TouchableOpacity>
         
          <View style={styles.borderContainer}>
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_bell}
                  resizeMode="contain"
                />
                <Text style={styles.textContainer}>Notifications</Text>
              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Switch
                  //    ios_backgroundColor={Colors.white}
                  // trackColor={{true: Colors.booger}}
                  onValueChange={notificationToggle}
                  value={notifications}
                />
              </View>
            </View>
          
            <TouchableOpacity onPress={Actions.PrivacyPolicy}>
           <View style={styles.borderContainer}>
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_policy}
                  resizeMode="contain"
                />
                <Text style={styles.textContainer}>Privacy policy</Text>
              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
               
              </View>
            </View>
           </TouchableOpacity>
           <TouchableOpacity onPress={Actions.UsagePolicy}>
            <View style={styles.borderContainer}>
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_use_policy}
                  resizeMode="contain"
                />
                <Text style={styles.textContainer}>Usage policy </Text>
              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                
              </View>
            </View>
           </TouchableOpacity>
           <TouchableOpacity onPress={onShare}>
           <View style={styles.borderContainer}>
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_share}
                  resizeMode="contain"
                />
                <Text style={styles.textContainer}>Share the app </Text>
              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                
              </View>
            </View>
           </TouchableOpacity>
           <TouchableOpacity onPress={Actions.RateApp}>
            <View style={styles.borderContainer}>
              <View style={{ flexDirection: "row" }}>
              <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_ratting}
                  resizeMode="contain"
                />
                <Text style={styles.textContainer}>Application Rating </Text>
              </View>
              
              
            </View>
            </TouchableOpacity>
            <View style={styles.spaceContainer}>
              <View style={{ flexDirection: "row" }}>
               
               
              </View>
              
              
            </View>
            
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};
export default Settings;
