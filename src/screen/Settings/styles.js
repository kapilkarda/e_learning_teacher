import {StyleSheet} from 'react-native';
import {w, h, totalSize} from '../../utils/Dimensions';
import {UiColor, TextColor, TextSize} from '../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE,
  },
  textSignUp: {
   marginBottom:w(5),
    fontSize: TextSize.bigSize,
    fontWeight: 'bold',
  },
  textUser: {

    fontSize: TextSize.h1,
    fontWeight: '200',
  },
  container: {
    marginTop: w(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  

  arrrowIcon: {
    width: w(5),
    height: h(7),
    alignSelf:'center',
    tintColor:UiColor.GREEN
  },
  
  inputIcon: {
    width: h(6),
    height: h(6),
    alignSelf:'center',
    
  },
  
  signUpText: {
   textAlign: 'center',
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    fontWeight: 'bold',
     
   
  },
  borderContainer: {
 
    borderWidth: w(0.1),
    borderColor: UiColor.GREEN,
    height:w(15),
    width: w(85),
    flexDirection:'row',
    justifyContent:'space-between',
    paddingHorizontal:15
  },
  textContainer:{

    textAlign: 'center',
    color: UiColor.BLACK,
    fontSize: TextSize.h1,
    fontWeight: '500',
    alignSelf:"center",
    marginLeft:w(6)
  },
  spaceContainer:{
   

    height:w(8),
    width: w(85),
    flexDirection:'row',
    justifyContent:'space-between',
    paddingHorizontal:15
  }
});
