import { StyleSheet } from 'react-native';
import { w, h, totalSize } from '../../utils/Dimensions';
import { UiColor, TextColor, TextSize } from '../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE,
  },
  textSignUp: {
    marginLeft: w(5),
    fontSize: TextSize.h2,
    fontWeight: '200',
  },
  textUser: {
    fontSize: TextSize.h1,
    fontWeight: '100',
  },
  container: {
    alignSelf: 'center',
    position: 'relative'
  },
  profileName: {
    position: 'absolute',
    bottom: w(1.2),
    width: w(100),
    textAlign: 'center',
    color: UiColor.WHITE,
    fontWeight: '600',
  },
  inputIcon: {
    width: h(3),
    height: h(3),
    marginTop: h(0.3),
    tintColor: '#2295a1'
  },
  inputIconTeacher: {
    width: h(4),
    height: h(4),
    // marginLeft: 0,
    marginTop: -6,
  },
  splashImg: {
    width: w(100),
    height: h(24)
  }




});
