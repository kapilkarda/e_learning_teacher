import React from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Linking,
  BackHandler,
  Alert
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import { w, h } from "../../utils/Dimensions";
import AsyncStorage from "@react-native-community/async-storage";
import { API_BASE_URL } from "../../constant/constant";
class DrawerBar extends React.Component {
  // const DrawerBar = () => {
  constructor(props) {
    super(props);
    this.state = {
      teacherName: '',
      setteacherName:''
    };
  }
  async componentWillMount() {
    this.getTeacherDetail()
    let tName = await AsyncStorage.getItem("teacherName");
    console.log(tName,"djkjdjkdjkjc");
    this.setState({
      teacherName: tName
    });
    this.focusListener = navigation.addListener("didFocus", async () => {
      let tName = await AsyncStorage.getItem("teacherName");
      console.log(tName,"djkjdjkdjkjc");
      this.setState({
        teacherName: tName
      });
    });
    this.getTeacherDetail()
  }
  componentWillUnmount() {
    console.log('unmount');

  }

  handleBackButton = () => {
    Alert.alert(
      "Log out",
      "Are you sure you want to log out?",
      [
        { text: "cancel", onPress: () => console.log("Cancel Pressed") },
        { text: "OK", onPress: () => {
          this.clearAsyncStorage()
        }}
      ],
      { cancelable: false }
    );
    return true;
  };
  clearAsyncStorage = async() => {
    await AsyncStorage.clear();
    Actions.Login()
  }
   getTeacherDetail = async () => {
    let teacherId = await AsyncStorage.getItem("teacherId");
    console.log(teacherId,"jdjdkj")
    fetch(API_BASE_URL + "/getTeacherProfile", {
      method: "Post",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        teacher_id: await AsyncStorage.getItem("teacherId"),
      })
    })
      .then(res => res.json())
      .then(response => {
       
       console.log(response,"kkdkdkk")
        if (response.result == 'true') {
          console.log(response,"teacherProfile");
       this.setState({
        setteacherName:response.data[0].teacher_name
       })
          
         
         } else {
         
        }
      })
      .catch(error => console.log(error));
  };
techerprofile(){
  Actions.SignupSec()
  this.getTeacherDetail()
}
  
  render() {
    
    return (

      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
        <View style={styles.mainContainer}>
          <ScrollView>
            <View style={styles.container}>
              <Image
                source={require("../../asssets/icons/drawerImg.png")}
                style={styles.splashImg}
              />
               </View>
            <View
              style={{
               alignItems:'center',
               justifyContent:'center',
               marginTop: 15,
               marginBottom: 10,
              }}>
           <Text style={{fontSize: h(3), fontWeight: 'bold',}}>{this.state.setteacherName}</Text>
            </View>
            <TouchableOpacity onPress={() => this.techerprofile()}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  marginLeft: 40,
                  marginTop: 15
                }} >
               <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_dummy_user}
                  resizeMode="contain"/>
                 <Text style={styles.textSignUp}> Teacher Profile</Text>
              </View>
            </TouchableOpacity>
             <TouchableOpacity onPress={Actions.Operations}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  marginLeft: 40,
                  marginTop: 15
                }}  >
             <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_request}
                  resizeMode="contain" />
                 <Text style={styles.textSignUp}> My requests</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.Wallet}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  marginLeft: 40,
                  marginTop: 15
                }} >
              <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_wallet}
                  resizeMode="contain" />
                <Text style={styles.textSignUp}> Wallet</Text>
              </View>
            </TouchableOpacity>
            {/* <TouchableOpacity onPress={Actions.Favorite}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  marginLeft: 40,
                  marginTop: 15
                }}  >
             <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_fav}
                  resizeMode="contain" />
                <Text style={styles.textSignUp}> Favorite</Text>
              </View>
            </TouchableOpacity> */}
            <TouchableOpacity onPress={Actions.ContactForm}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  marginLeft: 40,
                  marginTop: 15
                }} >
               <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_call}
                  resizeMode="contain"/>
                 <Text style={styles.textSignUp}> call us</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.Alert}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  marginLeft: 40,
                  marginTop: 15
                }} >
              <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_alert}
                  resizeMode="contain" />
                <Text style={styles.textSignUp}> Alerts</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.Settings}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  marginLeft: 40,
                  marginTop: 15
                }} >
              <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_settings}
                  resizeMode="contain"/>
                
                <Text style={styles.textSignUp}> Settings</Text>
              </View>
            </TouchableOpacity>
            <View
              style={{
                width: "100%",
                borderColor: "#29C577",
                borderWidth: 1,
                marginTop: 25
              }} />
            <TouchableOpacity onPress={this.handleBackButton}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  marginLeft: 40,
                  marginTop: 15
                }} >
              <Image
                  style={styles.inputIcon}
                  source={IconAsset.ic_tEntry}
                  resizeMode="contain" />
                <Text style={styles.textSignUp}> Log out</Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
};
export default DrawerBar;
