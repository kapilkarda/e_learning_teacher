import {
  StyleSheet
} from "react-native";
import {
  w,
  h,
  totalSize
} from "../../utils/Dimensions";
import {
  UiColor,
  TextColor,
  TextSize
} from "../../theme";

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE
  },

  container: {
    // marginTop: w(10),
    justifyContent: "center",
    alignItems: "center"
  },

  card: {
    height: h(15),
    width: w(90),
    shadowColor: "#00000021",
    shadowOffset: {
      width: 0,
      height: 6
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginLeft: 2,
    marginRight: 2,
    marginTop: 20,
    backgroundColor: "white",
    padding: 10,
    borderRadius: w(2),
    marginBottom: 10,
    justifyContent: "space-between",
    flexDirection:'row',
    // position: 'relative'
  },
  textSignUp: {
    fontSize: TextSize.bigSize,
    fontWeight: "bold"
  },
  textEmpty: {
    fontSize: w(4),
  },
  linearGradient: {
    height: h(3),
    width: w(18),
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    right: 0,
    top: 0,
    borderRadius: w(8)
  },
  signUpText: {
    textAlign: "center",
    color: UiColor.WHITE,
    fontSize: TextSize.h5,
    fontWeight: "bold"
  },
  btnContainer: {
    minWidth: w(10),
    height: h(4),
    borderWidth: 1,
    alignItems: "center",
    textAlign: 'center',
    justifyContent: "center",
    borderRadius: w(12),
    borderColor: UiColor.GREEN,
    marginHorizontal: w(2),
    marginBottom:h(1)
    // fontSize:12
  }
});