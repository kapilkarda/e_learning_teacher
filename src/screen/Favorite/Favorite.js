import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  FlatList,
  Linking, Platform 
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithOption } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import { w, h, totalSize } from "../../utils/Dimensions";
import { UiColor, TextColor, TextSize } from "../../theme";
import styles from "./styles";
import AsyncStorage from "@react-native-community/async-storage";
import { API_BASE_URL, TEACHER_API_BASE_URL } from "../../constant/constant";
import { AirbnbRating } from "react-native-ratings";
import Loader from '../../constant/Loader'
const favoritelist = [
  {
    uri: "https://bootdey.com/img/Content/avatar/avatar6.png",
    name: " karim eisaa eisaa ",
    sub1: "physics",
    sub2: "math"
  },
  {
    uri: "https://bootdey.com/img/Content/avatar/avatar6.png",
    name: " karim eisaa eisaa ",
    sub1: "physics",
    sub2: "math"
  },
  {
    uri: "https://bootdey.com/img/Content/avatar/avatar6.png",
    name: " karim eisaa eisaa ",
    sub1: "physics",
    sub2: "math"
  },
  {
    uri: "https://bootdey.com/img/Content/avatar/avatar6.png",
    name: " karim eisaa eisaa ",
    sub1: "physics",
    sub2: "math"
  },
  {
    uri: "https://bootdey.com/img/Content/avatar/avatar6.png",
    name: " karim eisaa eisaa ",
    sub1: "physics",
    sub2: "math"
  },
  {
    uri: "https://bootdey.com/img/Content/avatar/avatar6.png",
    name: " karim eisaa eisaa ",
    sub1: "physics",
    sub2: "math"
  }
];

class Favorite extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tempArray: [],
      isStudent_id: "",
      fav_list: "",
      level: [],
      isLoading:false
    };
  }

  componentWillMount = async () => {
    var teacherId = await AsyncStorage.getItem("teacherId");
    console.log("teacherId", teacherId);
    this.FavouriteList(teacherId);
  };

  

  newArray = item => {
    // const array = item.learning_levels.split(",");
    const array = ['asdasd','asdasdasd', 'asdasdasd','sdfsfds'];
    let arrayOne = array;
    console.log("arrayOne", array);
    return arrayOne.map((item, i) => (
      <View style={styles.btnContainer}>
        <Text style={{ fontSize: 12 }}>{item}</Text>
      </View>
    ));
  };
  
  dialCall = (number) => {
  
      let phoneNumber = '';
  
      if (Platform.OS === 'android') {
        phoneNumber = `tel:${number}`;
      }
      else {
        phoneNumber = `telprompt:${number}`;
      }
  
      Linking.openURL(phoneNumber);
  };
  // ...............................FavourList Api integration .......................
 
  FavouriteList = async (teacherId) => {
    console.log("studentId", teacherId);
    this.setState({ isLoading: true })
    fetch(API_BASE_URL + "/favoriteList", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        teacher_id: teacherId
      })
    })
      .then(res => res.json())
      .then(response => {
        console.log("response", response);
        if (response.result == "true") {
          this.setState({
            tempArray: response.data,
            isLoading:false
          });
        } else {
          console.log(response.msg);
          this.setState({
           isLoading:false
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoading:false
        });
      });
  };

  render() {
    console.log(this.state.tempArray, "teppppppp");
    return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
        <Loader loading={this.state.isLoading} />
        <View style={styles.mainContainer}>
          {/* <ScrollView> */}
            {HeaderWithOption(Actions.Search, "")}
            <View style={styles.container}>
              <View>
                <Text style={styles.textSignUp}> Favorite </Text>
              </View>
              <ScrollView showsVerticalScrollIndicator={false}>
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  data={this.state.tempArray}
                  renderItem={({ item }) => (
                    <View>
                      <View style={styles.card}>
                        <View style={{justifyContent:'center'}}>
                          <View style={{
                                width: h(8),
                                height: h(8),
                                // marginLeft: w(1),
                                alignItems: 'center',
                                justifyContent: "center",
                                tintColor: UiColor.GREEN,
                                borderColor: UiColor.GREEN, borderWidth: 2,
                                borderRadius: w(50),
                                overflow: 'hidden',
                              }}>
                            <Image
                              style={{
                                width: h(5),
                                height: h(5),
                                // marginLeft: w(1),
                                tintColor: UiColor.GREEN,
                              }}
                              // source={{uri:item.teacher_image}}
                              source={item.student_image === '' ? IconAsset.ic_dummy_user: {uri:item.student_image}}
                              resizeMode="contain"
                            />
                          </View>
                          
                        </View>
                        <View style={{justifyContent:'center'}}>
                        <Text style={{ marginLeft: w(2), fontSize: 12 }}>
                              {item.student_name}
                            </Text>
                        </View>
                        <View style={{width:w(35),minHeight:h(10),justifyContent:'center',marginTop:h(4)}}>
                        {/* {this.newArray(item)} */}
                          {/* <View>
                            <AirbnbRating
                              count={5}
                              reviews={[]}
                              reviewSize= {0}
                              style={{ paddingVertical: 5 }}
                              defaultRating={item.rate}
                              size={10}
                              selectedColor= '#2295a1'
                            />
                          </View> */}
                        </View>
                        <View style={{position: 'relative'}}>
                          <TouchableOpacity style={{height:h(5),width:w(20)}} onPress={() => this.dialCall(item.student_phone)}>
                            <LinearGradient
                              start={{ x: 0, y: 0 }}
                              end={{ x: 1, y: 0 }}
                              colors={["#1c83a0", "#32cba5"]}
                              style={styles.linearGradient}
                            >
                              <Text style={styles.signUpText}>Redial</Text>
                            </LinearGradient>
                          </TouchableOpacity>
                        </View>
                        
                        
                      </View>
                    </View>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
                {
                  this.state.tempArray && this.state.tempArray.length === 0 &&
                  <View style={{marginTop: w(20)}}>
                    <Text style={styles.textEmpty}> No any favorite data </Text>
                  </View>
                }
                <View style={{height: h(22)}}></View>
              </ScrollView>
            </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
export default Favorite;
