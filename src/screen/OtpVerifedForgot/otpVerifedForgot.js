import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithGoBackAndOption,HeaderWithBackLogin } from "../../components/AppHeader";
import styles from "./styles";
import LinearGradient from "react-native-linear-gradient";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { API_BASE_URL } from "../../constant/constant";
import AsyncStorage from "@react-native-community/async-storage";
import Loader from '../../constant/Loader';
const OtpVerifedForgot = (props) => {
  const [code, setcode] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const validation = () => {
    if (code == "") {
      alert("Please enter code");
    }  else {
      serverApi(code)
    }
  };

  const serverApi = async code => {
    console.log("serverApoii");
    setIsLoading(true)
    fetch(API_BASE_URL + "/Verify", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        otp: code.code,
        id: props.teacherId
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        if (response.status == "true" || response.status === true) {
          console.log("response", response);
          Actions.createPass();
        } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
  };
  return (
    <View style={styles.mainContainer}>
      <Loader loading={isLoading} />
      <ScrollView>
        {HeaderWithBackLogin(Actions.Login, "")}
        <View style={styles.container}>
          <Image
            style={styles.inputIcon}
            source={IconAsset.ic_right}
            resizeMode="contain"
          />
          <Text style={styles.textUser}>
            Please enter the number sent on your phone
          </Text>
          <View style={styles.inputContainer}>
            {/**********
              MobileNumber
             ***********/}

            <OTPInputView
              style={{
                width: "80%",
                height: 100,
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center"
              }}
              pinCount={4}
              // code={code}
              onCodeChanged = {code => { setcode({code})}}
              autoFocusOnLoad
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              onCodeFilled={code => {
                console.log(`Code is ${code}, you are good to go!`);
              }}
            />
          </View>

          {/**********
              LoginButton
             ***********/}
          <TouchableOpacity onPress={validation}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={["#1c83a0", "#32cba5"]}
              style={styles.linearGradient}
            >
              <Text style={styles.signUpText}>{Strings.CONFIRM}</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default OtpVerifedForgot;
