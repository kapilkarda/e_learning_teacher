import {StyleSheet} from 'react-native';
import {w, h, totalSize} from '../../utils/Dimensions';
import {UiColor, TextColor, TextSize} from '../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE,
  },
  textSignUp: {
    marginVertical: w(5),
    fontSize: TextSize.bigSize,
    fontWeight: '600',
  },
  textUser: {
    marginVertical: w(3.5),
    fontSize: TextSize.h3,
    fontWeight: '400',
  },
  container: {
    marginTop: w(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    backgroundColor: UiColor.WHITE,
    borderRadius: w(8),
    width: w(65),
    height: h(7),
    marginTop:w(12),
    // flexDirection: 'row',
    alignItems: 'center',
    justifyContent:"center",
  
    borderWidth: w(0.3),
    borderColor:UiColor.GREEN
  },

  inputIcon: {
    width: w(20),
    height: h(20),
    marginLeft: w(3),
    justifyContent: 'center',
    
  },
  buttonContainer: {
    height: h(7),
    width: w(75),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: w(5),
  },
  haveAccount: {
    marginTop: w (5),
    color: UiColor.BLACK,
    fontSize: TextSize.h4,
    fontWeight: "bold",
    textDecorationLine: "underline"
  },
  forgotPass: {
    marginTop:w(5),
    color: UiColor.BLACK,
    fontSize: TextSize.h4,
    fontWeight: "bold",
    textDecorationLine: "underline",
    
  },

  signupButton: {
    marginTop: w(5),
    backgroundColor: UiColor.GREEN,
  },
  linearGradient: {
  marginTop:w(12),
    height:h(7),
    width:w(65),
    alignItems:'center',
    justifyContent:'center',
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection:"row",
    borderRadius: w(8)
  },
  
  signUpText: {
   textAlign: 'center',
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    fontWeight: 'bold',
     
   
  },
  inputs:{
    width: w(55),
  },
  underlineStyleBase: {
    width: w(7),
    height: h(6),
    borderWidth: 0,
    borderBottomWidth: 1,
  },

  underlineStyleHighLighted: {
    borderColor: "#03DAC6",
  },
});
