// import React, { useState, useEffect } from "react";
// import {
//   View,
//   Image,
//   TextInput,
//   Text,
//   ScrollView,
//   TouchableOpacity,
//   Alert
// } from "react-native";
// import { Actions } from "react-native-router-flux";
// import { IconAsset, UiColor } from "../../theme";
// import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
// import Dialog, { DialogContent } from "react-native-popup-dialog";
// import LinearGradient from "react-native-linear-gradient";
// import styles from "./styles";
// import { w, h } from "../../utils/Dimensions";
// import ModalDropdown from "react-native-modal-dropdown";
// import ImagePicker from "react-native-image-picker";
// import SearchableDropdown from "react-native-searchable-dropdown";
// import { API_BASE_URL } from "../../constant/constant";
// import DocumentPicker from "react-native-document-picker";
// import AsyncStorage from "@react-native-community/async-storage";
// //  ---Image--- //
// import closeImg from "../../asssets/icons/close.png";
// import RNPicker from "rn-modal-picker";
// import Loader from '../../constant/Loader';

// let itemsss = [];
// let newArry = [];
// let nameArray = [];
// let temporaryArray = [];
// let temporaryArrayname = [];
// let tempArray = [];
// let learningStr = '';
// let materialStr = '';
// let materiallist = [];
// let learninglistArray = [];
// const SignupSec = props => {

//   const [teacherName, setteacherName] = useState("");
//   const [isLoading, setIsLoading] = useState(false);
//   const [loadingTitle, setLoadingTitle] = useState('');
  
//   // const [materiallist, setMaterialList] = useState([]);
//   const [city, setcity] = useState([]);
//   const [cityStr, setCity] = useState('');
//   const [bank, setbank] = useState("");
//   const [setid, setNewId] = useState("");
//   const [learningid, setLearningid] = useState("")
//   const [imgPath, setPath] = useState("");
//   const [newmaterial, setNewmaterial] = useState("");
//   const [newmaterialname, setNewmaterialname] = useState("");
//   // const [learninglistArray, setLearningList] = useState([]);
//   const [dialog, setDialog] = useState(false);
//   const [dialog1, setDialog1] = useState(false);
//   const [personalCard, setPersonalCard] = useState({});

 
//   const [gender, setGender] = useState("");
//   const [teacherid, setTeacherid] = useState("");
//   const [teacherno, setTeacherno] = useState("");
//   const [singleFile, setSingleFile] = useState({});
//   const [filePath, setFilePath] = useState("");
//   const [filePathDefault, setFilePathDefault] = useState("");
//   const [qualification, setqualification] = useState("");
//   const [nationality, setSelectedNationality] = useState([]);
//   const [nationalityStr, setNationality] = useState('');
//   const [cityId, setCItyId] = useState('');
//   const [nationalityId, setNationalityId] = useState('');
//   const [cityList, setCityList] = useState([]);
//   const [pdfCertificatePath, setPdfCertificatePath] = useState('');
//   const [pdfPersonalCardPath, setPdfPersonalCardPath] = useState('');
//   const [learningLevel, setlearningLevel] = useState('');
//   const [imgPaths, setImgPaths] = useState('')
//   const [profileStatus, setProfileStatus] = useState('');
//   const [uiRender, setUiRender] = useState(false);
//   // const [certificatesize,certificatefileSize] = useState("");
//   // const [certificatefilename,certificatesfilename] = useState("");
//   // const [personalcardsize,PersonalCardSize] = useState("");
//   // const [personalcardname,personalCardName] = useState("");
//   // const [singleFilesize, setSingleFileSize] = useState("");
//   // const [singleFilename, setSingleFileName] = useState("");
//   // const [personalcardsize1,PersonalCardSize2] = useState("");
//   // const [personalcardname1,personalCardName1] = useState("");
//  const genderList = ["Male", "Female"];
//  useEffect(() => {
//  getTeacherDetail();
//     getTeacherid();
//     // setTimeout(() => {
//     //   getNationalityList();
//     //   getTeacherid();
//     //   getLearningLevel();
//     //   getLearningMaterials();
//     // }, 2000);
//     // setTimeout(() => {
//     //   setIsLoading(false)
//     // }, 3000);
//     const unsubscribe = props.navigation.addListener('didFocus', () => {
//        getTeacherid();
//       // setTimeout(() => {
//       //   getTeacherDetail();
//       // }, 5000);
//     });
//     return () => unsubscribe;
//   }, []);

//   const getTeacherid = async () => {
//     let teacherId = await AsyncStorage.getItem("teacherId");
//     let teacher_no = await AsyncStorage.getItem("teacher_phone");
//     console.log(teacherId, teacher_no)
//     setTeacherid(teacherId);
//     setTeacherno(teacher_no);
//     let profileSetup = await AsyncStorage.getItem('profileSetupStatus');
//     setProfileStatus(profileSetup);
//   };

//   const genderValue = (id, value) => {
//     setGender(value);
//   };

//   const dialogopen = () => {
//     setDialog(true);
//   };

//   const dialogopen1 = () => {
//     setDialog1(true);
//   };

  
//   // .......................................getteacher detail..........................................

//   const getTeacherDetail = async () => {
//     setIsLoading(true)
//     fetch(API_BASE_URL + "/getTeacherProfile", {
//       method: "Post",
//       headers: new Headers({
//         "Content-Type": "application/json",
//         Accept: "application/json"
//       }),
//       body: JSON.stringify({
//         teacher_id: await AsyncStorage.getItem("teacherId"),
//       })
//     })
//       .then(res => res.json())
//       .then(response => {
//         setIsLoading(false)
//         console.log(response);
//         if (response.result == 'true') {
//           console.log(response,"teacherProfile");
//          getNationalityList();
//           getLearningLevel();
//           getLearningMaterials();
//           setteacherName(response.data[0].teacher_name)
//           setGender(response.data[0].teacher_gender)
//           setqualification(response.data[0].qualification)
//           setbank(response.data[0].bank_account_no)
//           setFilePath(response.data[0].teacher_image)
//          learningStr = response.data[0].learning_levels;
//           materialStr = response.data[0].learning_materials;
//           setCity(response.data[0].city)
//           setNationality(response.data[0].nationality)
//           setImgPaths(response.data[0].teacher_image)
//           setPdfCertificatePath(response.data[0].certificate)
//           setPdfPersonalCardPath(response.data[0].personal_card)
//           // setSingleFileName(response.data[0].certi_file_name)
//           // setSingleFileSize(response.data[0].certi_file_size)
//           // personalCardName1(response.data[0].pc_file_name)
//           // PersonalCardSize2(response.data[0].pc_file_size)
//           let singleFileName = {
//             name: response.data[0].certi_file_name,
//             size: response.data[0].certi_file_size,
//           }
//           let personalFileName = {
//             name: response.data[0].pc_file_name,
//             size: response.data[0].pc_file_size,
//           }
//           setSingleFile(singleFileName)
//           setPersonalCard(personalFileName)

//          } else {
//           alert(response.msg);
//           getNationalityList();
//           getLearningLevel();
//           getLearningMaterials();
//         }
//       })
//       .catch(error => console.log(error));
//   };

//   const selectOneFile = async section => {
//     if (section == "certificate") {
//       try {
//         const res = await DocumentPicker.pick({
//           type: [DocumentPicker.types.allFiles]
//         });
//         console.log("res", res)
//         // setSingleFileSize(res.size)
//         // setSingleFileName(res.name)
//         setSingleFile(res);
//         uploadPdf(res, section)
//       } catch (err) {
//         if (DocumentPicker.isCancel(err)) {
//           console.log("Canceled from single doc picker");
//         } else {
//           console.log("Unknown Error: " + JSON.stringify(err));
//           throw err;
//         }
//       }
//     }

//     if (section == "personal_card") {
//       try {
//         const res = await DocumentPicker.pick({
//           type: [DocumentPicker.types.allFiles]
//         });
//         console.log("res", res)
//         // PersonalCardSize(res.size)
//         // personalCardName(res.name)
//         setPersonalCard(res);
//         uploadPdf(res, section)
//       } catch (err) {
//         if (DocumentPicker.isCancel(err)) {
//           console.log("Canceled from single doc picker");
//         } else {
//           console.log("Unknown Error: " + JSON.stringify(err));
//           throw err;
//         }
//       }
//     }
//   };

//   // ....................................imageupload.............................................

//   const chooseFile = () => {
//     var options = {
//       title: "Select Image",
//       storageOptions: {
//         skipBackup: true,
//         path: "images"
//       }
//     };
//     try {
//       ImagePicker.showImagePicker(options, response => {
//         console.log("Response = ", response);
//         if (response.didCancel) {
//           console.log("User cancelled image picker");
//         } else if (response.error) {
//           console.log("ImagePicker Error: ", response.error);
//         } else if (response.customButton) {
//           console.log("User tapped custom button: ", response.customButton);
//           alert(response.customButton);
//         } else {
//           let source = response;
//           uploadImage(response.uri)
//           // setFilePathDefault(source);
//           setFilePath("data:image/jpeg;base64," + source.data)
//           // setImgPaths(response.fileName)
//         }
//       });
//     } catch (e) {
//       console.log('e', e);
//     }
//   };

//   //,.......................................image upolad............................................

//   const uploadImage = (image) => {
//     var file = {
//       uri: image,
//       type: 'image/png',
//       name: 'profile_image'
//     };
//     var data = new FormData();
//     data.append('file', file);
//     setIsLoading(true)
//     fetch(API_BASE_URL + '/upload_documents?type=teacher_image', {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'multipart/form-data',
//       },
//       body: data
//     }).then((res) => res.json())
//       .then(res => {
//         console.log(res)
//         setIsLoading(false)
//         if (res.path) {
//           setImgPaths(res.data)
//           Alert.alert('Upload successfully')
//         }
//       })
//       .catch((e) => {
//         console.log(e)
//         setIsLoading(false)
//       });
//   };

//   //,.......................................pdf upolad............................................

//   const uploadPdf = (res, section) => {
//     var file = {
//       uri: res.uri,
//       type: res.type,
//       name: 'pdf_file'
//     };
//     var data = new FormData();
//     data.append('file', file);
//     setIsLoading(true)
//     fetch(API_BASE_URL + `/upload_documents?type=${section}`, {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'multipart/form-data',
//       },
//       body: data
//     }).then((res) => res.json())
//       .then(res => {
//         setIsLoading(false)
//         if (res.path) {
//           Alert.alert('Upload successfully')
//           if (section == 'certificate') {
//             setPdfCertificatePath(res.data)
//           } else if (section == "personal_card") {
//             setPdfPersonalCardPath(res.data)
//           }
//         }
//       })
//       .catch((e) => {
//         console.log(e)
//         setIsLoading(false)
//       });
//   }

//   // .........................get nationality...................................................

//   const getNationalityList = () => {
//     setIsLoading(true)
//     fetch(API_BASE_URL + "/getNationality", {
//       method: "GET",
//       headers: new Headers({
//         "Content-Type": "application/json",
//         Accept: "application/json"
//       })
//     })
//       .then(res => res.json())
//       .then(response => {
//         setIsLoading(false)
//         if (response.result == "true") {
//           itemsss = [];
//           for (let item of response.data) {
//             itemsss.push({ id: item.n_id, name: item.n_name });
//           }
//           if (itemsss.length > 0) {
//             for (let nation of itemsss) {
//               if (nationalityStr == nation.name) {
//                 setNationality(nation.name)
//                 setNationalityId(nation.id)
//                 getCityList(nation.id);
//                 let rendr = uiRender
//                 if (rendr == true) {
//                   rendr = false
//                 } else {
//                   rendr = true
//                 }
//                 setUiRender(rendr);
//               }
//             }
//           }
//         } else {
//           alert(response.msg);
//         }
//       })
//       .catch(error => console.log(error));
//   };

//   //........................................ getcity..........................................

//   const getCityList = id => {
    
//     setIsLoading(true)
//     fetch(API_BASE_URL + `/getCity/${id}`, {
//       method: "GET",
//       headers: new Headers({
//         "Content-Type": "application/json",
//         Accept: "application/json"
//       })
//     })
//       .then(res => res.json())
//       .then(response => {
        
//         setIsLoading(false)
//         if (response.result == "true") {
//           tempArray = [];
//           for (let item of response.city) {
//             tempArray.push({ id: item.n_id, name: item.city_name });
//           }
//           setCityList(tempArray);
          
//           if (cityStr != '') {
//             if (cityList.length > 0) {
//               for (let city of cityList) {
//                 if (cityStr == city.name) {
//                   selectedCity(city);
//                 }
//               }
//             }
//           }
//         } else {
//           alert(response.msg);
//         }
//       })
//       .catch(error => console.log(error));
//   };

//   //............................... get learning level...........................//

//   const getLearningLevel = () => {
//     fetch(API_BASE_URL + "/getLearningLevel", {
//       method: "GET",
//       headers: new Headers({
//         "Content-Type": "application/json",
//         Accept: "application/json"
//       })
//     })
//       .then(res => res.json())
//       .then(response => {
//         if (response.result == "true") {
//           let tempArray = [];
//           for (let item of response.data) {
//             tempArray.push({
//               id: item.l_id,
//               name: item.level_name,
//               value: false
//             });
//           }
//           // setLearningList(tempArray);
//           learninglistArray = tempArray;
//           console.log('learningStr', learningStr);
//           if (learninglistArray.length > 0 || learningStr != '') {
//             nameArray = [];
//             let learning = learningStr.split(',');
//             for (let learn of learninglistArray) {
//               for (let newlearn of learning) {
//                 if (newlearn == learn.name) {
//                   var indexlearnId = learninglistArray.indexOf(learn);
//                   selectOrientation(learn, indexlearnId)
//                   console.log('learningStr', learningStr);
//                 }

//               }
//             }
//           }
//         } else {
//           console.log(response.message);
//         }
//       })
//       .catch(error => console.log(error));
//   };

//   //.............................. get learning material.......................................

//   const getLearningMaterials = () => {
//     fetch(API_BASE_URL + "/getLearningMaterial", {
//       method: "GET",
//       headers: new Headers({
//         "Content-Type": "application/json",
//         Accept: "application/json"
//       })
//     })
//       .then(res => res.json())
//       .then(response => {
//         if (response.result == "true") {
//           let tempMaterial = [];
//           for (let item of response.data) {
//             tempMaterial.push({
//               id: item.material_id,
//               name: item.material_name,
//               value: false
//             });
//           }
//           materiallist = tempMaterial;
//           // setMaterialList(tempMaterial);
//           console.log('materialStr', materialStr);
//           if (tempMaterial.length > 0 || materialStr != '') {
//             temporaryArrayname = [];
//             let material = materialStr.split(',');
//             for (let mat of tempMaterial) {
//               for (let newMat of material) {
//                 if (newMat == mat.name) {
//                   var indexlearnId = tempMaterial.indexOf(mat);
//                   selectMaterials(mat, indexlearnId)
//                   console.log('materialStr', materialStr);
//                 }
//               }
              
//             }
//           }
//         } else {
//           console.log(response.message);
//         }
//       })
//       .catch(error => console.log(error));
//   };


//   const selectOrientation = (item, index) => {
//     if (item.value == false) {
//       nameArray.push({ id: item.id, itemName: item.name });
//     } else {
//       var indexlearnId = nameArray.indexOf(item.id);
//       nameArray.splice(indexlearnId, 1);
//     }
//     let checked = learninglistArray;
//     checked[index].value = !checked[index].value;
//     // setLearningList(checked);
//     learninglistArray = checked;
//     // setNewId(nameArray);
//     // setLearningid(newArry.toString())
//     let rendr = uiRender
//     if (rendr == true) {
//       rendr = false
//     } else {
//       rendr = true
//     }
//     setUiRender(rendr);
//   };

//   const selectMaterials = (item, index) => {
//     if (item.value == false) {
//       temporaryArrayname.push({ id: item.id, itemName: item.name });
//     } else {
//       for (let id of materiallist) {
//         const materialid = id.id;
//         if (materialid === item.id) {
//           var indexsexId = temporaryArrayname.indexOf(item.id);
//           temporaryArrayname.splice(indexsexId, 1);
//         }
//       }
//     }
//     let checked = materiallist;
//     checked[index].value = !checked[index].value;
//     // setMaterialList(checked);
//     materiallist = checked
//     let rendr = uiRender
//     if (rendr == true) {
//       rendr = false
//     } else {
//       rendr = true
//     }
//     setUiRender(rendr);
//   };

//   // const cityValidate = () => {
//   //   for (var i = 0; i < tempArray.length; i++) {
//   //     if (tempArray[i].name === cityStr) {
//   //       enterData();
//   //       break;
//   //     } else {
//   //       // alert('city not found')
//   //       // break;
//   //     }
//   //   }
//   // }

//   // const countryValiudfate = () => {
//   //   for (var i = 0; i < itemsss.length; i++) {
//   //     if (itemsss[i].name === nationalityStr) {
//   //       cityValidate();
//   //       break;
//   //     } else {
//   //       // alert('country not found')
//   //       // break;
//   //     }
//   //   }
//   // }

//   const validation = async () => {
//     enterData();
//   };

//   const enterData = () => {
//     let tempArr = nameArray;
//     let tempArrId = [];
//     if (tempArr.length > 0) {
//       for (let item of tempArr) {
//         tempArrId.push(item.id)
//       }
//     }

//     let tempArrMat = temporaryArrayname;
//     let tempArrMatId = [];
//     if (tempArrMat.length > 0) {
//       for (let item of tempArrMat) {
//         tempArrMatId.push(item.id)
//       }
//     }
//     const dataSending = {
//       teacher_id: teacherid,
//       teacher_name: teacherName,
//       teacher_phone: teacherno,
//       cityId: cityId,
//       nationalityId: nationalityId,
//       bank: bank,
//       qualification: qualification,
//       image: imgPaths,
//       certificate: pdfCertificatePath,
//       personal_card: pdfPersonalCardPath,
//       teacher_gender: gender,
//       learning_levels: tempArrId.join(),
//       learning_materials: tempArrMatId.join()
//     };
//     console.log(dataSending)
//     handleSignupSec(dataSending);
//   }
//   const handleSignupSec = dataSending => {
//     console.log("teacherName", dataSending);
//     if (dataSending.teacher_name == "") {
//       alert("Please enter teacher name");
//     } 
//     else if (dataSending.teacher_gender == "") {
//       alert("Please select gender");
//     } else if (dataSending.nationality == "") {
//       alert("Please select nationality");
//     }
//      else if (dataSending.city == "") {
//       alert("Please select city");
//     }
//     else if (dataSending.bank == "") {
//       alert("Please enter bank account number");
//     }
//     else if (dataSending.learning_levels == "") {
//       alert("Please select learning levels");
//     }
//     else if (dataSending.learning_materials == "") {
//       alert("Please select learning materials");
//     }
//     else if (dataSending.certificate == "") {
//       alert("Please select certificate");
//     }
//     else if (dataSending.personal_card == "") {
//       alert("Please select personal card");
//     }
//     else if (dataSending.image == "") {
//       alert("Please select image");
//     }
//     else if (dataSending.qualification == "") {
//       alert("Please enter qualification");
//     }
//     else {
//       console.log(dataSending)
//       submitDataApi(dataSending);
//     }
//   };
//   const submitDataApi = async  dataSending => {
//     console.log(dataSending,"data")
//     setIsLoading(true)
//     var requestData = {
//       teacher_id: dataSending.teacher_id,
//       teacher_name: dataSending.teacher_name,
//       teacher_phone: dataSending.teacher_phone,
//       teacher_gender: dataSending.teacher_gender.toLowerCase(),
//       nationality: dataSending.nationalityId,
//       city: dataSending.cityId,
//       bank_acc_number: dataSending.bank,
//       learning_level: dataSending.learning_levels,
//       learning_material: dataSending.learning_materials,
//       certificate: dataSending.certificate,
//       personal_card: dataSending.personal_card,
//       qualification: dataSending.qualification,
//       image: dataSending.image,
//       certi_file_size :singleFile.size,
//      certi_file_name : singleFile.name,
//       pc_file_size : personalCard.size,
//       pc_file_name : personalCard.name
 
//     }
//     fetch(API_BASE_URL + "/setUpTeacherProfile", {

//       method: "POST",
//       headers: new Headers({
//         "Content-Type": "application/json",
//         Accept: "application/json"
//       }),
//       body: JSON.stringify(requestData)
//     })
//       .then(res => res.json())
//       .then(response => {
//         setIsLoading(false)
//         console.log(response);
//         if (response.result == "true") {
//           // alert(response.msg);
//           AsyncStorage.setItem('teacherName', dataSending.teacher_name);
//           AsyncStorage.setItem('profileSetupStatus', '1');
//           console.log("response", response);
//           // Actions.Alert();
//           Alert.alert(
//             'Alert',
//             response.msg,
//             [
//               { text: 'OK', onPress: () => Actions.Alert() },
//             ],
//             { cancelable: false },
//           );
        
//         } else {
//           alert(response.msg);
//         }
//       })
//       .catch(error => console.log(error));
//   };
//   const selectedNation = item => {
//     setCity('')
//     setNationalityId(item.id)
//     setNationality(item.name)
//     getCityList(item.id);
//     let rendr = uiRender
//     if (rendr == true) {
//       rendr = false
//     } else {
//       rendr = true
//     }
//     setUiRender(rendr);
//   };

//   const selectedCity = item => {
//     setCity(item.name)
//     setCItyId(item.id)
//   };

//   // const onChangeCountry = (text) => {
//   //   setNationality(text)
//   // }

//   // const onChangecity = (text) => {
//   //   setCity(text)
//   // }

//   return (
//     <View style={styles.mainContainer}>
//       <Loader loading={isLoading} title={loadingTitle} />
//       {HeaderWithGoBackAndOption(Actions.Alert, "")}
//       <ScrollView
//         nestedScrollEnabled={true}
//         keyboardShouldPersistTaps={"handled"}
//       >
//         <View style={{}}>
//           {
//             profileStatus == '1' ?
//               <Text style={styles.textSignUp}>Update Profile</Text>
//               :
//               <Text style={styles.textSignUp}>Setup Profile</Text>
//           }

//           <View>
//             <Text style={styles.textUser}>Teacher name</Text>
//           </View>
//           <View style={styles.inputContainer}>
//             <TextInput
//               style={styles.inputs}
//               autoCapitalize="none"
//               underlineColorAndroid="transparent"
//               onChangeText={teacherName => setteacherName(teacherName)}
//               value={teacherName}
//             />
//           </View>
//           <View>
//             <Text style={styles.textUser}>Gender</Text>
//           </View>
//           <View>
//             <ModalDropdown
//               style={styles.popupmodalStylegender}
//               textStyle={styles.popuptextStylegender}
//               dropdownStyle={styles.popupdropdownStylegender}
//               dropdownTextStyle={styles.dropdownTextstylegender}
//               options={genderList}
//               onSelect={(idx, value) => genderValue(idx, value)}
//             >
//               <View style={styles.dropIconViewgender}>
//                 <Text style={styles.popuptextStylegender}>{gender}</Text>
//                 <Image
//                   source={IconAsset.ic_down}
//                   style={styles.iconStylegender}
//                 />
//               </View>
//             </ModalDropdown>
//           </View>
//           <View>
//             <Text style={styles.textUser}>Nationality </Text>
//           </View>
//           <View style={styles.selectInputContainer}>
//             <RNPicker
//               dataSource={itemsss}
//               dummyDataSource={itemsss}
//               defaultValue={true}
//               pickerTitle={"Nationality List"}
//               showSearchBar={true}
//               disablePicker={false}
//               changeAnimation={"none"}
//               searchBarPlaceHolder={"Search....."}
//               showPickerTitle={true}
//               searchBarContainerStyle={styles.searchBarContainerStyle}
//               pickerStyle={styles.pickerStyle}
//               pickerItemTextStyle={styles.listTextViewStyle}
//               selectedLabel={nationalityStr}
//               placeHolderLabel={''}
//               selectLabelTextStyle={styles.selectLabelTextStyle}
//               placeHolderTextStyle={styles.placeHolderTextStyle}
//               dropDownImageStyle={styles.dropDownImageStyle}
//               dropDownImage={IconAsset.ic_down}
//               selectedValue={(index, item) => selectedNation(item)}
//             />
//           </View>
//           <View>
//             <Text style={styles.textUser}>City</Text>
//           </View>
//           <View style={styles.selectInputContainer}>
//             <RNPicker
//               dataSource={cityList}
//               dummyDataSource={cityList}
//               defaultValue={true}
//               pickerTitle={"City List"}
//               showSearchBar={true}
//               disablePicker={false}
//               changeAnimation={"none"}
//               searchBarPlaceHolder={"Search....."}
//               showPickerTitle={true}
//               searchBarContainerStyle={styles.searchBarContainerStyle}
//               pickerStyle={styles.pickerStyle}
//               pickerItemTextStyle={styles.listTextViewStyle}
//               selectedLabel={cityStr}
//               placeHolderLabel={''}
//               selectLabelTextStyle={styles.selectLabelTextStyle}
//               placeHolderTextStyle={styles.placeHolderTextStyle}
//               dropDownImageStyle={styles.dropDownImageStyle}
//               dropDownImage={IconAsset.ic_down}
//               selectedValue={(index, item) => selectedCity(item)}
//             />
//           </View>
//           {/* <View style={styles.selectInputContainer}>
//             <SearchableDropdown
//               selectedItems={nationality}
//               onItemSelect={item => {
//                 selectedNation(item);
//               }}
//               value={nationalityStr}
//               containerStyle={{ padding: 5 }}
//               onRemoveItem={(item, index) => {
//                 removeNation(item, index);
//               }}
//               itemStyle={{
//                 padding: 5,
//                 backgroundColor: "#fff",
//                 borderColor: "#bbb",
//                 borderWidth: 0.5,
//                 width: w(73),
//                 alignSelf: "center",
//                 borderRadius: 25
//               }}
//               itemTextStyle={{ color: "#222" }}
//               itemsContainerStyle={{ maxHeight: 140 }}
//               items={itemsss}
//               resetValue={false}
//               textInputStyle={{
//                 padding: 15,
//                 backgroundColor: "#999"
//               }}
//               textInputProps={{
//                 placeholder: "",
//                 underlineColorAndroid: "transparent",
//                 style: {
//                   padding: 8,
//                   borderWidth: 1,
//                   borderColor: "#33D1A5",
//                   borderRadius: 50,
//                   width: "100%",
//                   alignSelf: "center",
//                   height: h(7)
//                 },
//                 onTextChange: (text) => onChangeCountry(text)
//               }}
//               listProps={{
//                 nestedScrollEnabled: true
//               }}
//             />
//             <Image
//               style={styles.inputIconDrop}
//               source={IconAsset.ic_down}
//               resizeMode="contain"
//             />
//           </View> */}
//           {/* <View>
//             <Text style={styles.textUser}>City</Text>
//           </View>
//           <View style={styles.selectInputContainer}>
//             <SearchableDropdown
//               selectedItems={city}
//               onItemSelect={item => {
//                 selectedCity(item);
//               }}
//               containerStyle={{ padding: 5 }}
//               onRemoveItem={(item, index) => {
//                 removeNation(item, index);
//               }}
//               itemStyle={{
//                 padding: 5,
//                 paddingLeft: 20,
//                 backgroundColor: "#fff",
//                 borderColor: "#bbb",
//                 borderWidth: 0.5,
//                 width: w(73),
//                 alignSelf: "center",
//                 borderRadius: 25
//               }}
//               itemTextStyle={{ color: "#222" }}
//               itemsContainerStyle={{ maxHeight: 140 }}
//               items={cityList}
//               resetValue={false}
//               textInputStyle={{
//                 padding: 15,
//                 backgroundColor: "#999"
//               }}
//               textInputProps={{
//                 placeholder: "",
//                 underlineColorAndroid: "transparent",
//                 style: {
//                   padding: 8,
//                   borderWidth: 1,
//                   borderColor: "#33D1A5",
//                   borderRadius: 50,
//                   width: "100%",
//                   alignSelf: "center",
//                   height: h(7)
//                 },
//                 onTextChange: (text) => onChangecity(text)
//               }}
//               listProps={{
//                 nestedScrollEnabled: true
//               }}
//             />
//             <Image
//               style={styles.inputIconDrop}
//               source={IconAsset.ic_down}
//               resizeMode="contain"
//             />
//           </View> */}
//           <View>
//             <Text style={styles.textUser}>Bank account number</Text>
//           </View>
//           <View style={{}}>
//             <View style={styles.inputContainer}>
//               <TextInput
//                 style={styles.inputs}
//                 keyboardType={"numeric"}
//                 autoCapitalize="none"
//                 underlineColorAndroid="transparent"
//                 onChangeText={bank => setbank(bank)}
//                 value={bank}
//               />
//             </View>
//           </View>

//           <View>
//             <Text style={styles.textUser}>Select Learning Levels</Text>
//           </View>
//           <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
//             <View style={{ width: w(12) }} />
//             <View style={styles.scrolllearningMainView}>
//               <ScrollView horizontal={true} style={styles.scrollViewStyle}>
//                 {nameArray &&
//                   nameArray.map((op, i) => (
//                     <Text
//                       key={i}
//                       style={{
//                         borderWidth: 1,
//                         padding: h(0.6),
//                         borderRadius: 25,
//                         marginLeft: w(2),
//                         minWidth: w(30), textAlign: 'center',
//                         minHeight: h(4)
//                       }}
//                     >
//                       {op.itemName}
//                     </Text>
//                   ))}
//               </ScrollView>
//             </View>
//             <TouchableOpacity
//               onPress={dialogopen}
//               style={{ width: w(13), height: h(6), marginTop: h(0.5) }}>
//               <Image
//                 style={[styles.addBtn, { alignSelf: "center" }]}
//                 source={IconAsset.ic_add}
//                 resizeMode="contain"
//               />
//             </TouchableOpacity>
//           </View>
//           <View>
//             <Text style={[styles.textUser, { marginTop: h(4) }]}>Select learning materials</Text>
//           </View>
//           <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
//             <View style={{ width: w(12) }} />
//             <View style={styles.scrolllearningMainView}>
//               <ScrollView horizontal={true} style={styles.scrollViewStyle}>
//                 {temporaryArrayname &&
//                   temporaryArrayname.map((op, i) => (
//                     <Text
//                       key={i}
//                       style={{
//                         borderWidth: 1,
//                         padding: h(0.6),
//                         borderRadius: 25,
//                         marginLeft: w(2),
//                         minWidth: w(30), textAlign: 'center',
//                         minHeight: h(4)
//                       }}
//                     >
//                       {op.itemName}
//                     </Text>
//                   ))}
//               </ScrollView>
//             </View>
//             <TouchableOpacity
//               onPress={dialogopen1}
//               style={{ width: w(13), height: h(6), marginTop: h(0.5) }}>
//               <Image
//                 style={[styles.addBtn, { alignSelf: "center" }]}
//                 source={IconAsset.ic_add}
//                 resizeMode="contain"
//               />
//             </TouchableOpacity>
//           </View>

//           <View>
//             <Text style={styles.textUser}>Attach a certificate</Text>
//           </View>
//           <View style={styles.spaceContainer}>
//             <TouchableOpacity onPress={selectOneFile.bind(this, "certificate")}>
//               <View style={{ marginTop: 2 }}>
//                 <Image
//                   resizeMode={"contain"}
//                   style={styles.img}
//                   source={
//                     pdfCertificatePath !== ""
//                       ? require("../../asssets/icons/fileimg.png")
//                       : require("../../asssets/icons/ic_upload.png")
//                   }
//                 />
//               </View>
//             </TouchableOpacity>
//           </View>
//           <View style={{ marginTop: 2, marginLeft: w(15) }}>
//             <Text style={{ color: UiColor.GREEN }}>File Size</Text>
        
//             <View style={{ flexDirection: "row" }}>
//               <Text>{singleFile.name ? singleFile.name : "No File"}</Text>
//               <Text style={{ marginLeft: 10 }}>
//                 {singleFile.size ? singleFile.size : "0 kb"}
//               </Text>
//             </View>
           
            
//           </View>
//           <View>
//             <Text style={styles.textUser}>Attach a personal card</Text>
//           </View>
//           <View style={styles.spaceContainer}>
//             <TouchableOpacity
//               onPress={selectOneFile.bind(this, "personal_card")}
//             >
//               <View style={{ marginTop: 2 }}>
//                 <Image
//                   resizeMode={"contain"}
//                   style={styles.img}
//                   source={
//                     pdfPersonalCardPath !== ""
//                       ? require("../../asssets/icons/fileimg.png")
//                       : require("../../asssets/icons/ic_upload.png")
//                   }
//                 />
//               </View>
//             </TouchableOpacity>
//           </View>
//           <View style={{ marginTop: 2, marginLeft: w(15) }}>
//             <Text style={{ color: UiColor.GREEN }}>File Size</Text>
          
//   <View style={{flexDirection:'row',  width:w(60)}}>
//               <Text>{personalCard.name ? personalCard.name : "No File"}</Text>
//               <Text style={{ marginLeft: 10 }}>
//                 {personalCard.size ? personalCard.size : "0 kb"}
//               </Text>
//             </View>
           
//           </View>
//           <View>
//             <Text style={styles.textUser}>Upload Image</Text>
//           </View>
//           <View style={styles.spaceContainer}>
//             <TouchableOpacity onPress={chooseFile.bind(this)}>
//               <View style={{ marginTop: 2 }}>
//                 <Image
//                   resizeMode="contain"
//                   style={styles.img}
//                   source={
//                     filePath !== ""
//                       ? {
//                         uri: filePath
//                       }
//                       : require("../../asssets/icons/picture.png")
//                   }
//                 />
//               </View>
//             </TouchableOpacity>
//           </View>
//           <View>
//             <Text style={styles.textUser}>Qualification</Text>
//           </View>
//           <View style={styles.textArea}>
//             <TextInput
//               style={styles.inputs}
//               autoCapitalize="none"
//               multiline={true}
//               underlineColorAndroid="transparent"
//               onChangeText={qualification => setqualification(qualification)}
//               value={qualification}
//             />
//           </View>
//           <View>
//             <TouchableOpacity onPress={validation}>
//               <LinearGradient
//                 start={{ x: 0, y: 0 }}
//                 end={{ x: 1, y: 0 }}
//                 colors={["#1c83a0", "#32cba5"]}
//                 style={styles.linearGradient}
//               >
//                 <Text style={styles.signUpText}>Save</Text>
//               </LinearGradient>
//             </TouchableOpacity>
//           </View>
//           <View style={{ height: 25 }}></View>
//         </View>
//       </ScrollView>
//       <Dialog
//         visible={dialog}
//         onTouchOutside={() => {
//           setDialog(false);
//         }}
//       >
//         <DialogContent style={styles.learningDialogmain}>
//           <ScrollView>
//             {/* <TouchableOpacity
//               onPress={() => {
//                 setDialog(false);
//               }}
//             >
//               <Image
//                 resizeMode="contain"
//                 source={closeImg}
//                 style={styles.closeStyle}
//               />
//             </TouchableOpacity> */}
//             <View>
//               {learninglistArray.map((item, index) => (
//                 <TouchableOpacity
//                   key={index}
//                   onPress={() => selectOrientation(item, index)}
//                   style={[
//                     styles.nameArrayText,
//                     { backgroundColor: (item.value == true ? "#cccccc" : "#ffffff") }
//                   ]}
//                 >
//                   <Text>{item.name}</Text>
//                 </TouchableOpacity>
//               ))}
//             </View>
//             <TouchableOpacity
//               onPress={() => {
//                 setDialog(false);
//               }}
//               style={styles.okBtn}
//             >
//               <Text style={styles.okText}>Ok</Text>
//             </TouchableOpacity>
//           </ScrollView>
//         </DialogContent>
//       </Dialog>

//       <Dialog
//         visible={dialog1}
//         onTouchOutside={() => {
//           setDialog1(false);
//         }}
//       >
//         <DialogContent style={styles.learningDialogmain}>
//           <ScrollView>
//             {/* <TouchableOpacity
//               onPress={() => {
//                 setDialog1(false);
//               }}
//             >
//               <Image
//                 resizeMode="contain"
//                 source={closeImg}
//                 style={styles.closeStyle}
//               />
//             </TouchableOpacity> */}
//             <View>
//               {materiallist.map((item, index) => (
//                 <TouchableOpacity
//                   key={index}
//                   onPress={() => selectMaterials(item, index)}
//                   style={[
//                     styles.nameArrayText,
//                     { backgroundColor: item.value ? "#cccccc" : "#ffffff" }
//                   ]}
//                 >
//                   <Text>{item.name}</Text>
//                 </TouchableOpacity>
//               ))}
//             </View>
//             <TouchableOpacity
//               onPress={() => {
//                 setDialog1(false);
//               }}
//               style={styles.okBtn}
//             >
//               <Text style={styles.okText}>Ok</Text>
//             </TouchableOpacity>
//           </ScrollView>
//         </DialogContent>
//       </Dialog>
//     </View>
//   );
// };
// export default SignupSec;
import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  TouchableOpacity,
  Alert,
  TouchableWithoutFeedbackBase
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, UiColor } from "../../theme";
import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
import Dialog, { DialogContent } from "react-native-popup-dialog";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import { w, h } from "../../utils/Dimensions";
import ModalDropdown from "react-native-modal-dropdown";
import ImagePicker from "react-native-image-picker";
import SearchableDropdown from "react-native-searchable-dropdown";
import { API_BASE_URL } from "../../constant/constant";
import DocumentPicker from "react-native-document-picker";
import AsyncStorage from "@react-native-community/async-storage";
//  ---Image--- //
import closeImg from "../../asssets/icons/close.png";
import RNPicker from "rn-modal-picker";
import Loader from '../../constant/Loader';


let itemsss = [];
let newArry = [];
let nameArray = [];
let temporaryArray = [];
let temporaryArrayname = [];
let tempArray = [];
let learningStr = '';
let materialStr = '';
let materiallist = [];
let learninglistArray = [];
const SignupSec = props => {

  const [teacherName, setteacherName] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [loadingTitle, setLoadingTitle] = useState('');
  
  // const [materiallist, setMaterialList] = useState([]);
  const [city, setcity] = useState([]);
  const [cityStr, setCity] = useState('');
  const [bank, setbank] = useState("");
  const [setid, setNewId] = useState("");
  const [learningid, setLearningid] = useState("")
  const [imgPath, setPath] = useState("");
  const [newmaterial, setNewmaterial] = useState("");
  const [newmaterialname, setNewmaterialname] = useState("");
  // const [learninglistArray, setLearningList] = useState([]);
  const [dialog, setDialog] = useState(false);
  const [dialog1, setDialog1] = useState(false);
  const [personalCard, setPersonalCard] = useState({});

 
  const [gender, setGender] = useState("");
  const [teacherid, setTeacherid] = useState("");
  const [teacherno, setTeacherno] = useState("");
  const [singleFile, setSingleFile] = useState({});
  const [filePath, setFilePath] = useState("");
  const [filePathDefault, setFilePathDefault] = useState("");
  const [qualification, setqualification] = useState("");
  const [nationality, setSelectedNationality] = useState([]);
  const [nationalityStr, setNationality] = useState('');
  const [cityId, setCItyId] = useState('');
  const [nationalityId, setNationalityId] = useState('');
  const [cityList, setCityList] = useState([]);
  const [pdfCertificatePath, setPdfCertificatePath] = useState('');
  const [pdfPersonalCardPath, setPdfPersonalCardPath] = useState('');
  const [learningLevel, setlearningLevel] = useState('');
  const [imgPaths, setImgPaths] = useState('')
  const [profileStatus, setProfileStatus] = useState('');
  const [uiRender, setUiRender] = useState(false);
  // const [certificatesize,certificatefileSize] = useState("");
  // const [certificatefilename,certificatesfilename] = useState("");
  // const [personalcardsize,PersonalCardSize] = useState("");
  // const [personalcardname,personalCardName] = useState("");
  // const [singleFilesize, setSingleFileSize] = useState("");
  // const [singleFilename, setSingleFileName] = useState("");
  // const [personalcardsize1,PersonalCardSize2] = useState("");
  // const [personalcardname1,personalCardName1] = useState("");
 const genderList = ["Male", "Female"];
 useEffect(() => {
 getTeacherDetail();
    getTeacherid();
    // setTimeout(() => {
    //   getNationalityList();
    //   getTeacherid();
    //   getLearningLevel();
    //   getLearningMaterials();
    // }, 2000);
    // setTimeout(() => {
    //   setIsLoading(false)
    // }, 3000);
    const unsubscribe = props.navigation.addListener('didFocus', () => {
       getTeacherid();
      // setTimeout(() => {
      //   getTeacherDetail();
      // }, 5000);
    });
    return () => unsubscribe;
  }, []);

  const getTeacherid = async () => {
    let teacherId = await AsyncStorage.getItem("teacherId");
    let teacher_no = await AsyncStorage.getItem("teacher_phone");
    console.log(teacherId, teacher_no)
    setTeacherid(teacherId);
    setTeacherno(teacher_no);
    let profileSetup = await AsyncStorage.getItem('profileSetupStatus');
    setProfileStatus(profileSetup);
  };

  const genderValue = (id, value) => {
    setGender(value);
  };

  const dialogopen = () => {
    setDialog(true);
  };

  const dialogopen1 = () => {
    setDialog1(true);
  };

  
  // .......................................getteacher detail..........................................

  const getTeacherDetail = async () => {
    setIsLoading(true)
    fetch(API_BASE_URL + "/getTeacherProfile", {
      method: "Post",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        teacher_id: await AsyncStorage.getItem("teacherId"),
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log(response);
        if (response.result == 'true') {
          console.log(response,"teacherProfile");
          getNationalityList(response.data[0].nationality, response.data[0].city);
          getLearningLevel(response.data[0].learning_levels);
          getLearningMaterials(response.data[0].learning_materials);

          setteacherName(response.data[0].teacher_name)
          setGender(response.data[0].teacher_gender)
          setqualification(response.data[0].qualification)
          setbank(response.data[0].bank_account_no)
          setFilePath(response.data[0].teacher_image)
         learningStr = response.data[0].learning_levels;
          materialStr = response.data[0].learning_materials;
          setCity(response.data[0].city)
        
          setNationality(response.data[0].nationality)
          setImgPaths(response.data[0].teacher_image)
          setPdfCertificatePath(response.data[0].certificate)
          setPdfPersonalCardPath(response.data[0].personal_card)
          // setSingleFileName(response.data[0].certi_file_name)
          // setSingleFileSize(response.data[0].certi_file_size)
          // personalCardName1(response.data[0].pc_file_name)
          // PersonalCardSize2(response.data[0].pc_file_size)
          let singleFileName = {
            name: response.data[0].certi_file_name,
            size: response.data[0].certi_file_size,
          }
          let personalFileName = {
            name: response.data[0].pc_file_name,
            size: response.data[0].pc_file_size,
          }
          setSingleFile(singleFileName)
          setPersonalCard(personalFileName)

         } else {
          alert(response.msg);
          getNationalityList('', city);
          getLearningLevel('');
          getLearningMaterials('');
        }
      })
      .catch(error => console.log(error));
  };

  const selectOneFile = async section => {
    if (section == "certificate") {
      try {
        const res = await DocumentPicker.pick({
          type: [DocumentPicker.types.allFiles]
        });
        console.log("res", res)
        // setSingleFileSize(res.size)
        // setSingleFileName(res.name)
        setSingleFile(res);
        uploadPdf(res, section)
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          console.log("Canceled from single doc picker");
        } else {
          console.log("Unknown Error: " + JSON.stringify(err));
          throw err;
        }
      }
    }

    if (section == "personal_card") {
      try {
        const res = await DocumentPicker.pick({
          type: [DocumentPicker.types.allFiles]
        });
        console.log("res", res)
        // PersonalCardSize(res.size)
        // personalCardName(res.name)
        setPersonalCard(res);
        uploadPdf(res, section)
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          console.log("Canceled from single doc picker");
        } else {
          console.log("Unknown Error: " + JSON.stringify(err));
          throw err;
        }
      }
    }
  };

  // ....................................imageupload.............................................

  const chooseFile = () => {
    var options = {
      title: "Select Image",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    try {
      ImagePicker.showImagePicker(options, response => {
        console.log("Response = ", response);
        if (response.didCancel) {
          console.log("User cancelled image picker");
        } else if (response.error) {
          console.log("ImagePicker Error: ", response.error);
        } else if (response.customButton) {
          console.log("User tapped custom button: ", response.customButton);
          alert(response.customButton);
        } else {
          let source = response;
          uploadImage(response.uri)
          // setFilePathDefault(source);
          setFilePath("data:image/jpeg;base64," + source.data)
          // setImgPaths(response.fileName)
        }
      });
    } catch (e) {
      console.log('e', e);
    }
  };

  //,.......................................image upolad............................................

  const uploadImage = (image) => {
    var file = {
      uri: image,
      type: 'image/png',
      name: 'profile_image'
    };
    var data = new FormData();
    data.append('file', file);
    setIsLoading(true)
    fetch(API_BASE_URL + '/upload_documents?type=teacher_image', {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data
    }).then((res) => res.json())
      .then(res => {
        console.log(res)
        setIsLoading(false)
        if (res.path) {
          setImgPaths(res.data)
          Alert.alert('Upload successfully')
        }
      })
      .catch((e) => {
        console.log(e)
        setIsLoading(false)
      });
  };

  //,.......................................pdf upolad............................................

  const uploadPdf = (res, section) => {
    var file = {
      uri: res.uri,
      type: res.type,
      name: 'pdf_file'
    };
    var data = new FormData();
    data.append('file', file);
    setIsLoading(true)
    fetch(API_BASE_URL + `/upload_documents?type=${section}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data
    }).then((res) => res.json())
      .then(res => {
        setIsLoading(false)
        if (res.path) {
          Alert.alert('Upload successfully')
          if (section == 'certificate') {
            setPdfCertificatePath(res.data)
          } else if (section == "personal_card") {
            setPdfPersonalCardPath(res.data)
          }
        }
      })
      .catch((e) => {
        console.log(e)
        setIsLoading(false)
      });
  }

  // .........................get nationality...................................................

  const getNationalityList = (data, ct) => {
    console.log("nationalityStr",nationalityStr)

    setIsLoading(true)
    fetch(API_BASE_URL + "/getNationality", {
      method: "GET",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
       
        if (response.result == "true") {
          console.log(response,"dkldkg.,.,gv,g,")
          itemsss = [];
          for (let item of response.data) {
            itemsss.push({ id: item.n_id, name: item.n_name });
          }
          if (itemsss.length > 0) {
            console.log("in side if",itemsss.length)
            console.log("datadatadatadatadatadatadatadatadata",data)
            if (data != '') {
              for (let nation of itemsss) {
                if (data == nation.name) {
                  console.log("jdjdkj",data)
                  console.log("lkdl",nation.name)
                  setNationality(nation.name)
                  setNationalityId(nation.id)
                  getCityList(nation.id, ct);
                  let rendr = uiRender
                  if (rendr == true) {
                    rendr = false
                  } else {
                    rendr = true
                  }
                  setUiRender(rendr);

                  console.log('nationalityIdnationalityIdnationalityId',nationalityId,nationalityStr)
                }
              }
            }
            
          }
        } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
  };

  //........................................ getcity..........................................

  const getCityList = (id, cty) => {
     setIsLoading(true)
    fetch(API_BASE_URL + `/getCity/${id}`, {
      method: "GET",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      })
    })
      .then(res => res.json())
      .then(response => {
         setIsLoading(false)
         if (response.result == "true") {
          console.log(response,"d,lk")
          tempArray = [];
          for (let item of response.city) {
            tempArray.push({ id: item.n_id, name: item.city_name });
          }
          console.log(tempArray,"tempArray setstate")
          setCityList(tempArray);
          // if (cty !== '') {
          //   setTimeout(() => {
          //     tempFunc(cty);
          // }, 3000);
          // }
           if (cty !== '') {
             console.log(cityList,"CityList")
             if (tempArray.length > 0) {
               
              for (let city of tempArray) {
                // console.log(city,"kflfkldddddddddddddddd")
                if (cty == city.name) {
                   console.log(city.id,"city_id")
                  selectedCity(city);
                  setCItyId(city.id)
                }
              }
            }
          }
        } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
  };

  //  const tempFunc = (cty) => {
  //   console.log(cty,"cty tempFunc")
  //     console.log(cityList,"CityList tempFunc")
  //     if (cityList.length > 0) {
        
  //      for (let city of cityList) {
  //        // console.log(city,"kflfkldddddddddddddddd")
  //        if (cty == city.name) {
  //           console.log(city.id,"city_id")
  //          selectedCity(city);
  //          setCItyId(city.id)
  //        }
  //      }
  //    }
  //  }
  //............................... get learning level...........................//

  const getLearningLevel = (data) => {
    fetch(API_BASE_URL + "/getLearningLevel", {
      method: "GET",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      })
    })
      .then(res => res.json())
      .then(response => {
        if (response.result == "true") {
          let tempArray = [];
          for (let item of response.data) {
            tempArray.push({
              id: item.l_id,
              name: item.level_name,
              value: false
            });
          }
          // setLearningList(tempArray);
          learninglistArray = tempArray;
          console.log('learningStr', learningStr);
          if (learninglistArray.length > 0) {
            if (data != '') {
              nameArray = [];
              let learning = data.split(',');
              for (let learn of learninglistArray) {
                for (let newlearn of learning) {
                  if (newlearn == learn.name) {
                    var indexlearnId = learninglistArray.indexOf(learn);
                    selectOrientation(learn, indexlearnId)
                    console.log('learningStr', learningStr);
                  }

                }
              }
            }
          }
        } else {
          console.log(response.message);
        }
      })
      .catch(error => console.log(error));
  };

  //.............................. get learning material.......................................

  const getLearningMaterials = (data) => {
    fetch(API_BASE_URL + "/getLearningMaterial", {
      method: "GET",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      })
    })
      .then(res => res.json())
      .then(response => {
        if (response.result == "true") {
          let tempMaterial = [];
          for (let item of response.data) {
            tempMaterial.push({
              id: item.material_id,
              name: item.material_name,
              value: false
            });
          }
          materiallist = tempMaterial;
          // setMaterialList(tempMaterial);
          console.log('materialStr', materialStr);
          if (tempMaterial.length > 0) {
            if (data != '') {
              temporaryArrayname = [];
              let material = data.split(',');
              for (let mat of tempMaterial) {
                for (let newMat of material) {
                  if (newMat == mat.name) {
                    var indexlearnId = tempMaterial.indexOf(mat);
                    selectMaterials(mat, indexlearnId)
                    console.log('materialStr', materialStr);
                  }
                }
              }
              
            }
          }
        } else {
          console.log(response.message);
        }
      })
      .catch(error => console.log(error));
  };


  const selectOrientation = (item, index) => {
    if (item.value == false) {
      nameArray.push({ id: item.id, itemName: item.name });
    } else {
      var indexlearnId = nameArray.indexOf(item.id);
      nameArray.splice(indexlearnId, 1);
    }
    let checked = learninglistArray;
    checked[index].value = !checked[index].value;
    // setLearningList(checked);
    learninglistArray = checked;
    // setNewId(nameArray);
    // setLearningid(newArry.toString())
    let rendr = uiRender
    if (rendr == true) {
      rendr = false
    } else {
      rendr = true
    }
    setUiRender(rendr);
  };

  const selectMaterials = (item, index) => {
    if (item.value == false) {
      temporaryArrayname.push({ id: item.id, itemName: item.name });
    } else {
      for (let id of materiallist) {
        const materialid = id.id;
        if (materialid === item.id) {
          var indexsexId = temporaryArrayname.indexOf(item.id);
          temporaryArrayname.splice(indexsexId, 1);
        }
      }
    }
    let checked = materiallist;
    checked[index].value = !checked[index].value;
    // setMaterialList(checked);
    materiallist = checked
    let rendr = uiRender
    if (rendr == true) {
      rendr = false
    } else {
      rendr = true
    }
    setUiRender(rendr);
  };

  // const cityValidate = () => {
  //   for (var i = 0; i < tempArray.length; i++) {
  //     if (tempArray[i].name === cityStr) {
  //       enterData();
  //       break;
  //     } else {
  //       // alert('city not found')
  //       // break;
  //     }
  //   }
  // }

  // const countryValiudfate = () => {
  //   for (var i = 0; i < itemsss.length; i++) {
  //     if (itemsss[i].name === nationalityStr) {
  //       cityValidate();
  //       break;
  //     } else {
  //       // alert('country not found')
  //       // break;
  //     }
  //   }
  // }

  const validation = async () => {
    enterData();
  };

  const enterData = () => {
    let tempArr = nameArray;
    let tempArrId = [];
    if (tempArr.length > 0) {
      for (let item of tempArr) {
        tempArrId.push(item.id)
      }
    }
    let tempArrMat = temporaryArrayname;
    let tempArrMatId = [];
    if (tempArrMat.length > 0) {
      for (let item of tempArrMat) {
        tempArrMatId.push(item.id)
      }
    }
    const dataSending = {
      teacher_id: teacherid,
      teacher_name: teacherName,
      teacher_phone: teacherno,
      cityId: cityId,
      nationalityId: nationalityId,
      bank: bank,
      qualification: qualification,
      image: imgPaths,
      certificate: pdfCertificatePath,
      personal_card: pdfPersonalCardPath,
      teacher_gender: gender,
      learning_levels: tempArrId.join(),
      learning_materials: tempArrMatId.join()
    };
    console.log(dataSending.nationalityId,"nationalityId",dataSending.cityId)
    handleSignupSec(dataSending);
  }
  const handleSignupSec = dataSending => {
    console.log("teacherName", dataSending);
    if (dataSending.teacher_name == "") {
      alert("Please enter teacher name");
    } 
    else if (dataSending.teacher_gender == "") {
      alert("Please select gender");
    } else if (dataSending.nationality == "") {
      alert("Please select nationality");
    }
     else if (dataSending.city == "") {
      alert("Please select city");
    }
    else if (dataSending.bank == "") {
      alert("Please enter bank account number");
    }
    else if (dataSending.learning_levels == "") {
      alert("Please select learning levels");
    }
    else if (dataSending.learning_materials == "") {
      alert("Please select learning materials");
    }
    else if (dataSending.certificate == "") {
      alert("Please select certificate");
    }
    else if (dataSending.personal_card == "") {
      alert("Please select personal card");
    }
    else if (dataSending.image == "") {
      alert("Please select image");
    }
    else if (dataSending.qualification == "") {
      alert("Please enter qualification");
    }
    else {
      console.log(dataSending)
      submitDataApi(dataSending);
    }
  };
  const submitDataApi = async  dataSending => {
    console.log(dataSending,"data")
    setIsLoading(true)
    var requestData = {
      teacher_id: dataSending.teacher_id,
      teacher_name: dataSending.teacher_name,
      teacher_phone: dataSending.teacher_phone,
      teacher_gender: dataSending.teacher_gender.toLowerCase(),
      nationality: dataSending.nationalityId,
      city: dataSending.cityId,
      bank_acc_number: dataSending.bank,
      learning_level: dataSending.learning_levels,
      learning_material: dataSending.learning_materials,
      certificate: dataSending.certificate,
      personal_card: dataSending.personal_card,
      qualification: dataSending.qualification,
      image: dataSending.image,
      certi_file_size :singleFile.size,
     certi_file_name : singleFile.name,
      pc_file_size : personalCard.size,
      pc_file_name : personalCard.name
 
    }
    console.log(requestData.nationality)
    console.log(requestData.city,"kdlklklkdkldkkdc")
    fetch(API_BASE_URL + "/setUpTeacherProfile", {

      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify(requestData)
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log(response);
        if (response.result == "true") {
          // alert(response.msg);
          AsyncStorage.setItem('teacherName', dataSending.teacher_name);
          AsyncStorage.setItem('profileSetupStatus', '1');
          console.log("response", response);
          // Actions.Alert();
          Alert.alert(
            'Alert',
            response.msg,
            [
              { text: 'OK', onPress: () => Actions.Alert() },
            ],
            { cancelable: false },
          );
          } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
  };
  const selectedNation = item => {
    setCity('')
    setNationalityId(item.id)
    setNationality(item.name)
    getCityList(item.id, '');
    let rendr = uiRender
    if (rendr == true) {
      rendr = false
    } else {
      rendr = true
    }
    setUiRender(rendr);
  };
 const selectedCity = item => {
    setCity(item.name)
    setCItyId(item.id)
  };
  console.log(cityId,"dklc")
   return (
    <View style={styles.mainContainer}>
      <Loader loading={isLoading} title={loadingTitle} />
      {HeaderWithGoBackAndOption(Actions.Alert, "")}
      <ScrollView
        nestedScrollEnabled={true}
        keyboardShouldPersistTaps={"handled"}
      >
        <View style={{}}>
          {
            profileStatus == '1' ?
              <Text style={styles.textSignUp}>Update Profile</Text>
              :
              <Text style={styles.textSignUp}>Setup Profile</Text>
          }

          <View>
            <Text style={styles.textUser}>Teacher name </Text>
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputs}
              autoCapitalize="none"
              underlineColorAndroid="transparent"
              onChangeText={teacherName => setteacherName(teacherName)}
              value={teacherName}
            />
          </View>
          <View>
            <Text style={styles.textUser}> Gender</Text>
          </View>
          <View>
            <ModalDropdown
              style={styles.popupmodalStylegender}
              textStyle={styles.popuptextStylegender}
              dropdownStyle={styles.popupdropdownStylegender}
              dropdownTextStyle={styles.dropdownTextstylegender}
              options={genderList}
              onSelect={(idx, value) => genderValue(idx, value)}
            >
              <View style={styles.dropIconViewgender}>
                <Text style={styles.popuptextStylegender}>{gender}</Text>
                <Image
                  source={IconAsset.ic_down}
                  style={styles.iconStylegender}
                />
              </View>
            </ModalDropdown>
          </View>
          <View>
            <Text style={styles.textUser}> Nationality </Text>
          </View>
          <View style={styles.selectInputContainer}>
            <RNPicker
              dataSource={itemsss}
              dummyDataSource={itemsss}
              defaultValue={true}
              pickerTitle={"Nationality List"}
              showSearchBar={true}
              disablePicker={false}
              changeAnimation={"none"}
              searchBarPlaceHolder={"Search....."}
              showPickerTitle={true}
              searchBarContainerStyle={styles.searchBarContainerStyle}
              pickerStyle={styles.pickerStyle}
              pickerItemTextStyle={styles.listTextViewStyle}
              selectedLabel={nationalityStr}
              placeHolderLabel={''}
              selectLabelTextStyle={styles.selectLabelTextStyle}
              placeHolderTextStyle={styles.placeHolderTextStyle}
              dropDownImageStyle={styles.dropDownImageStyle}
              dropDownImage={IconAsset.ic_down}
              selectedValue={(index, item) => selectedNation(item)}
            />
          </View>
          <View>
            <Text style={styles.textUser}> City</Text>
          </View>
          <View style={styles.selectInputContainer}>
            <RNPicker
              dataSource={cityList}
              dummyDataSource={cityList}
              defaultValue={true}
              pickerTitle={"City List"}
              showSearchBar={true}
              disablePicker={false}
              changeAnimation={"none"}
              searchBarPlaceHolder={"Search....."}
              showPickerTitle={true}
              searchBarContainerStyle={styles.searchBarContainerStyle}
              pickerStyle={styles.pickerStyle}
              pickerItemTextStyle={styles.listTextViewStyle}
              selectedLabel={cityStr}
              placeHolderLabel={''}
              selectLabelTextStyle={styles.selectLabelTextStyle}
              placeHolderTextStyle={styles.placeHolderTextStyle}
              dropDownImageStyle={styles.dropDownImageStyle}
              dropDownImage={IconAsset.ic_down}
              selectedValue={(index, item) => selectedCity(item)}
            />
          </View>
          
          <View>
            <Text style={styles.textUser}>Bank account number </Text>
          </View>
          <View style={{}}>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                keyboardType={"numeric"}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                onChangeText={bank => setbank(bank)}
                value={bank}
              />
            </View>
          </View>

          <View>
            <Text style={styles.textUser}>"Select Learning Levels</Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={{ width: w(12) }} />
            <View style={styles.scrolllearningMainView}>
              <ScrollView horizontal={true} style={styles.scrollViewStyle}>
                {nameArray &&
                  nameArray.map((op, i) => (
                    <Text
                      key={i}
                      style={{
                        borderWidth: 1,
                        padding: h(0.6),
                        borderRadius: 25,
                        marginLeft: w(2),
                        minWidth: w(30), textAlign: 'center',
                        minHeight: h(4)
                      }}
                    >
                      {op.itemName}
                    </Text>
                  ))}
              </ScrollView>
            </View>
            <TouchableOpacity
              onPress={dialogopen}
              style={{ width: w(13), height: h(6), marginTop: h(0.5) }}>
              <Image
                style={[styles.addBtn, { alignSelf: "center" }]}
                source={IconAsset.ic_add}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <View>
            <Text style={[styles.textUser, { marginTop: h(4) }]}>Select learning materials </Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={{ width: w(12) }} />
            <View style={styles.scrolllearningMainView}>
              <ScrollView horizontal={true} style={styles.scrollViewStyle}>
                {temporaryArrayname &&
                  temporaryArrayname.map((op, i) => (
                    <Text
                      key={i}
                      style={{
                        borderWidth: 1,
                        padding: h(0.6),
                        borderRadius: 25,
                        marginLeft: w(2),
                        minWidth: w(30), textAlign: 'center',
                        minHeight: h(4)
                      }}
                    >
                      {op.itemName}
                    </Text>
                  ))}
              </ScrollView>
            </View>
            <TouchableOpacity
              onPress={dialogopen1}
              style={{ width: w(13), height: h(6), marginTop: h(0.5) }}>
              <Image
                style={[styles.addBtn, { alignSelf: "center" }]}
                source={IconAsset.ic_add}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>

          <View>
            <Text style={styles.textUser}> Attach a certificate </Text>
          </View>
          <View style={styles.spaceContainer}>
            <TouchableOpacity onPress={selectOneFile.bind(this, "certificate")}>
              <View style={{ marginTop: 2 }}>
                <Image
                  resizeMode={"contain"}
                  style={styles.img}
                  source={
                    pdfCertificatePath !== ""
                      ? require("../../asssets/icons/fileimg.png")
                      : require("../../asssets/icons/ic_upload.png")
                  }
                />
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 2, marginLeft: w(15) }}>
            <Text style={{ color: UiColor.GREEN }}> File Size</Text>
        
            <View style={{ flexDirection: "row" }}>
              <Text>{singleFile.name ? singleFile.name : "No File"}</Text>
              <Text style={{ marginLeft: 10 }}>
                {singleFile.size ? singleFile.size : "0 kb"}
              </Text>
            </View>
           
            
          </View>
          <View>
            <Text style={styles.textUser}> Attach a personal card</Text>
          </View>
          <View style={styles.spaceContainer}>
            <TouchableOpacity
              onPress={selectOneFile.bind(this, "personal_card")}
            >
              <View style={{ marginTop: 2 }}>
                <Image
                  resizeMode={"contain"}
                  style={styles.img}
                  source={
                    pdfPersonalCardPath !== ""
                      ? require("../../asssets/icons/fileimg.png")
                      : require("../../asssets/icons/ic_upload.png")
                  }
                />
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 2, marginLeft: w(15) }}>
            <Text style={{ color: UiColor.GREEN }}> File Size </Text>
          
  <View style={{flexDirection:'row',  width:w(60)}}>
              <Text>{personalCard.name ? personalCard.name : "No File"}</Text>
              <Text style={{ marginLeft: 10 }}>
                {personalCard.size ? personalCard.size : "0 kb"}
              </Text>
            </View>
           
          </View>
          <View>
            <Text style={styles.textUser}> Upload Image</Text>
          </View>
          <View style={styles.spaceContainer}>
            <TouchableOpacity onPress={chooseFile.bind(this)}>
              <View style={{ marginTop: 2 }}>
                <Image
                  resizeMode="contain"
                  style={styles.img}
                  source={
                    filePath !== ""
                      ? {
                        uri: filePath
                      }
                      : require("../../asssets/icons/picture.png")
                  }
                />
              </View>
            </TouchableOpacity>
          </View>
          <View>
            <Text style={styles.textUser}> Qualification</Text>
          </View>
          <View style={styles.textArea}>
            <TextInput
              style={styles.inputs}
              autoCapitalize="none"
              multiline={true}
              underlineColorAndroid="transparent"
              onChangeText={qualification => setqualification(qualification)}
              value={qualification}
            />
          </View>
          <View>
            <TouchableOpacity onPress={validation}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={["#1c83a0", "#32cba5"]}
                style={styles.linearGradient}
              >
                <Text style={styles.signUpText}> Save</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
          <View style={{ height: 25 }}></View>
        </View>
      </ScrollView>
      <Dialog
        visible={dialog}
        onTouchOutside={() => {
          setDialog(false);
        }}
      >
        <DialogContent style={styles.learningDialogmain}>
          <ScrollView>
             <View>
              {learninglistArray.map((item, index) => (
                <TouchableOpacity
                  key={index}
                  onPress={() => selectOrientation(item, index)}
                  style={[
                    styles.nameArrayText,
                    { backgroundColor: (item.value == true ? "#cccccc" : "#ffffff") }
                  ]}
                >
                  <Text>{item.name}</Text>
                </TouchableOpacity>
              ))}
            </View>
            <TouchableOpacity
              onPress={() => {
                setDialog(false);
              }}
              style={styles.okBtn}
            >
              <Text style={styles.okText}> Ok</Text>
            </TouchableOpacity>
          </ScrollView>
        </DialogContent>
      </Dialog>
  <Dialog
        visible={dialog1}
        onTouchOutside={() => {
          setDialog1(false);
        }}
      >
        <DialogContent style={styles.learningDialogmain}>
          <ScrollView>
            
            <View>
              {materiallist.map((item, index) => (
                <TouchableOpacity
                  key={index}
                  onPress={() => selectMaterials(item, index)}
                  style={[
                    styles.nameArrayText,
                    { backgroundColor: item.value ? "#cccccc" : "#ffffff" }
                  ]}
                >
                  <Text>{item.name}</Text>
                </TouchableOpacity>
              ))}
            </View>
            <TouchableOpacity
              onPress={() => {
                setDialog1(false);
              }}
              style={styles.okBtn}
            >
              <Text style={styles.okText}> Ok</Text>
            </TouchableOpacity>
          </ScrollView>
        </DialogContent>
      </Dialog>
    </View>
  );
};


export default SignupSec;


