import { StyleSheet } from "react-native";
import { w, h, totalSize } from "../../utils/Dimensions";
import { UiColor, TextColor, TextSize } from "../../theme";

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE
  },
  textSignUp: {
    margin: w(8),
    alignSelf: "center",
    fontSize: TextSize.bigSize,
    fontWeight: "bold"
  },
  textUser: {
    marginTop: w(3),
    fontSize: TextSize.h2,
    fontWeight: "200",
    marginLeft: w(15),
    marginVertical: w(1)
  },

  inputContainer: {
    marginTop: w(2),
    backgroundColor: UiColor.WHITE,
    borderRadius: w(8),
    width: w(75),
    height: h(7),
    marginBottom: w(4),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN,
    alignSelf: "center"
  },
  scrollViewStyle: {
    height: h(7),
    width: w(70),
    padding: h(1),
  },
  scrolllearningMainView: {
    width: w(75),
    borderRadius: w(8),
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN,
    alignItems: 'center',
    justifyContent: 'center'
  },
  selectInputContainer: {
    width: w(78),
    alignSelf: "center",
    position: "relative"
  },
  inputIconDrop: {
    position: "absolute",
    right: w(5),
    top: w(3.5),
    width: w(4),
    height: h(4),
    tintColor: UiColor.GREEN
  },
  spaceContainer: {
    marginTop: w(2),
    backgroundColor: UiColor.WHITE,
    borderRadius: w(8),
    width: w(75),
    height: h(20),
    marginBottom: w(4),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN,
    alignSelf: "center"
  },
  textArea: {
    marginTop: w(2),
    backgroundColor: UiColor.WHITE,
    borderRadius: w(8),
    width: w(75),
    height: h(20),
    marginBottom: w(4),
    // flexDirection: "row",
    // justifyContent: "center",
    alignItems: "center",
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN,
    alignSelf: "center"
  },
  img: {
    width: w(65),
    height: h(15)
  },
  process: {
    width: w(75),
    height: h(11),
    alignSelf: "center"
  },

  inputIcon: {
    width: w(3),
    height: h(3),
    marginRight: w(5),

    tintColor: UiColor.GREEN
  },
  addBtn: {
    width: w(5),
    height: h(5),
  },
  buttonContainer: {
    height: h(7),
    width: w(75),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: w(5)
  },

  signupButton: {
    marginTop: w(5),
    backgroundColor: UiColor.GREEN
  },
  linearGradient: {
    marginTop: w(10),
    height: h(7),
    width: w(60),
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: "row",
    borderRadius: w(8),
    alignSelf: "center"
  },
  item: {
    marginTop: w(1),
    color: UiColor.BLACK,
    borderRadius: w(6),
    borderColor: UiColor.GRAY,
    backgroundColor: "#D6DCE0",
    borderWidth: 1,
    padding: 5
  },
  signUpText: {
    textAlign: "center",
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    fontWeight: "bold"
  },
  inputs: {
    width: w(70),
    flexDirection: "row",
    marginLeft: 6
  },
  modalStyle: {
    width: w(75),
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#000",
    height: h(5),
    justifyContent: "center",
    borderRadius: 5
  },
  popupmodalStyle: {
    width: w(50),
    backgroundColor: "#fff",
    height: h(5),
    justifyContent: "center",
    borderRadius: 5
  },
  textStyle: {
    color: "#000",
    fontWeight: "bold",
    fontSize: TextSize.h3,
    width: w(64),
    marginLeft: w(2)
  },
  popuptextStyle: {
    color: "#000",
    fontWeight: "bold",
    fontSize: TextSize.h3,
    width: w(40),
    marginLeft: w(2)
  },
  dropdownStyle: {
    width: w(75),
    height: h(18),
    marginTop: Platform.OS === "ios" ? w(0) : w(-5)
  },
  popupdropdownStyle: {
    width: w(45),
    height: h(15),
    marginTop: h(-3)
  },
  dropdownTextstyle: {
    fontSize: TextSize.h3,
    color: "#000"
  },
  dropdownView: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: w(4)
  },
  dropIconView: {
    flexDirection: "row"
  },
  iconStyle: {
    height: 15,
    width: 15,
    tintColor: "#000",
    alignSelf: "center"
  },

  popupmodalStylegender: {
    width: w(75),
    backgroundColor: "#fff",
    height: h(7),
    justifyContent: "center",
    borderRadius: 5,
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN,
    alignSelf: "center",
    borderRadius: 30
  },
  popupdropdownStylegender: {
    width: w(75),
    height: h(13),

    marginTop: Platform.OS === "ios" ? h(1) : h(1)
  },
  dropdownMainviewgender: {
    marginTop: h(3)
  },
  dropdownTextstylegender: {
    fontSize: TextSize.h3,
    color: "#000"
  },
  dropdownViewgender: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: w(5)
  },
  dropIconViewgender: {
    flexDirection: "row"
  },
  iconStylegender: {
    height: 15,
    width: 15,

    tintColor: UiColor.GREEN,
    alignSelf: "center"
  },
  popuptextStylegender: {
    color: "#000",
    fontSize: h(2.2),
    width: w(66),
    marginLeft: w(2)
  },
  //Learning Dialog //
  learningDialogmain: {
    height: h(55),
    width: w(80)
  },
  closeStyle: {
    height: h(3),
    width: w(3),
    alignSelf: "flex-end",
    marginTop: 5
  },
  nameArrayText: {
    height: h(5.5),
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: h(1)
  },
  okBtn: {
    height: h(6),
    width: w(40),
    backgroundColor: UiColor.GREEN,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginTop: h(3),
    borderRadius: 5
  },
  okText: {
    color: "#fff",
    fontSize: h(3),
    fontWeight: "bold"
  },
  searchBarContainerStyle: {
    marginBottom: 10,
    flexDirection: "row",
    height: 40,
    shadowOpacity: 1.0,
    shadowRadius: 5,
    shadowOffset: {
      width: 1,
      height: 1
    },
    backgroundColor: "rgba(255,255,255,1)",
    shadowColor: "#d3d3d3",
    borderRadius: 10,
    elevation: 3,
    marginLeft: 10,
    marginRight: 10
  },

  selectLabelTextStyle: {
    color: "#000",
    textAlign: "left",
    width: "99%",
    padding: 10,
    flexDirection: "row"
  },
  placeHolderTextStyle: {
    color: "#D3D3D3",
    padding: 10,
    textAlign: "left",
    width: "99%",
    flexDirection: "row"
  },
  dropDownImageStyle: {
    marginLeft: 4,
    width: 15,
    height: 15,
    alignSelf: "center",
    tintColor: UiColor.GREEN
  },
  listTextViewStyle: {
    color: "#000",
    marginVertical: 10,
    flex: 0.9,
    marginLeft: 20,
    marginHorizontal: 10,
    textAlign: "left"
  },
  pickerStyle: {
    marginLeft: 18,
    paddingRight: 25,
    marginRight: 10,
    marginBottom: 5,
    marginTop: 5,
    borderWidth:1,
    shadowRadius: 10,
    backgroundColor: "rgba(255,255,255,1)",
    borderColor: UiColor.GREEN,
    borderRadius: w(8),
    flexDirection: "row"
  }
});
