import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
  TextInput
} from "react-native";
import { Actions } from "react-native-router-flux";
import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import AsyncStorage from "@react-native-community/async-storage";
import Dialog, { DialogContent } from "react-native-popup-dialog";
import { API_BASE_URL } from "../../constant/constant";
import Loader from "../../constant/Loader";
import MapView, { Marker } from "react-native-maps";
//  ---Image--- //
import Geocoder from "react-native-geocoding";
import closeImg from "../../asssets/icons/close.png";
import { w, h } from "../../utils/Dimensions";

const Request = props => {
  // console.log(props.requestId)
  const [isLoading, setIsLoading] = useState(false);
  const [teacherid, setTeacherid] = useState("");
  const [requesDetail, setRequestdetail] = useState("");
  const [dialog, setDialog] = useState(false);
  const [price, setPrice] = useState("");
  const [requestId, setRequestId] = useState("");
  const [isStatus, setIsStatus] = useState("1");
  const [isDate, setDate] = useState("");
  const [isservice_selector, setServiceSelector] = useState("");
  const [issubject, setSubject] = useState("");
  const [isservice_type, setServiceType] = useState("");
  const [istime, setTime] = useState("");
  const [student, setStudent] = useState("");
  // const [isDate, setDate] = useState();
  const [isStrage, setStarge] = useState("");
  const [latitude, setLat] = useState("");
  const [longitude, setLng] = useState("");
  const [showLocationStatus, setShowLocation] = useState(false);

  useEffect(() => {
    getTeacherid();
    getCurrentRequest();
    getData();
  }, [props]);
  const getData = () => {
    Geocoder.init("AIzaSyAPoCcrEh3C8K-UBbYccf7zGHNpet1A3CM"); // use a valid API key
    Geocoder.from(22.7525266, 75.8653457)
      .then(json => {
        var addressComponent = json.results[0].address_components[0];
        console.log(addressComponent,"dkkcdkc");
        alert(addressComponent)
      })
      .catch(error => console.warn(error));
    Geocoder.from({
      latitude: 22.7525266,
      longitude: 75.8653457
    });
   Geocoder.from({
      lat: 22.7525266,
      lng: 75.8653457
    });
  Geocoder.from([22.7525266, 75.8653457]);
  };

  const getTeacherid = async () => {
    let teacherId = await AsyncStorage.getItem("teacherId");
    setTeacherid(teacherId);
    console.log(teacherId, "teacherId");
  };

  const dialogopen = () => {
    setDialog(true);
  };
  const showLoaction = () => {
    setShowLocation(true);
  };
  const getCurrentRequest = () => {
    console.log("props.requestId", props.requestId);
    setIsLoading(true);
    fetch(API_BASE_URL + "/getRequestDetails", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        request_id: props.requestId
      })
    })
      .then(res => res.json())
      .then(response => {
        console.log("response", response);

        setIsLoading(false);
        if (response.result == "true") {
          setDate(response.data[0].date);
          setServiceSelector(response.data[0].service_selector);
          setStarge(response.data[0].stage);
          setSubject(response.data[0].subject);
          setServiceType(response.data[0].service_type);
          setTime(response.data[0].time);
          setIsStatus(response.data[0].status);
          setRequestId(response.data[0].request_id);
          setRequestdetail(response.data[0].order_details);
          setStudent(response.data[0].student_number);
          setLat(response.data[0].lat);
          setLng(response.data[0].long);
          setTimeout(() => {
            // console.log("stat req", isStatus, requestId);
          }, 1000);
        } else {
          alert(response.msg);
          console.log("errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr", response);
        }
      })
      .catch(error => {
        setIsLoading(false);
      });
  };

  const getStatus = async () => {
    if (price == "") {
      alert("Please enter price");
    } else {
      setIsLoading(true);
      console.log("dghcgdchg", teacherid, props.requestId, isStatus, price);
      fetch(API_BASE_URL + "/requestStatus", {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/json",
          Accept: "application/json"
        }),
        body: JSON.stringify({
          teacher_id: teacherid,
          request_id: props.requestId,
          // is_status: isStatus,
          is_status: "1",
          price: price
        })
      })
        .then(res => res.json())
        .then(response => {
          setIsLoading(false);
          console.log(response, "response");
          if (response.result == "true") {
            setDialog(false);
            setPrice("");
            console.log("response", response);
            // setRequestList(response.data[0]);
            alert(response.msg);
            Actions.Operations();
          } else {
            alert(response.msg);
          }
        })
        .catch(error => {
          setIsLoading(false);
        });
    }
  };
  const getReject = async () => {
    console.log("dghcgdchg", teacherid, props.requestId, isStatus, price);
    setIsLoading(true);
    fetch(API_BASE_URL + "/requestStatus", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        teacher_id: teacherid,
        request_id: props.requestId,
        is_status: "2",
        price: ""
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false);

        if (response.result == "true") {
          alert(response.msg);
          Actions.Operations();
        } else {
          alert(response.msg);
        }
      })
      .catch(error => {
        setIsLoading(false);
      });
  };

  return (
    <View style={styles.mainContainer}>
      <Loader loading={isLoading} />
      {HeaderWithGoBackAndOption(Actions.Alert, "")}
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.container}>
            <View style={styles.card}>
              <Text style={styles.textSignUp}>Request</Text>
              <View style={{ flexDirection: "row" }}>
                <View>
                  <Text style={styles.textUser}>Date</Text>
                  <View style={styles.inputContainer}>
                    <Text>{isDate}</Text>
                  </View>
                </View>
                <View>
                  <Text style={styles.textUser}>Service Selector</Text>
                  <View style={styles.inputContainer}>
                    <Text>{isservice_selector}</Text>
                  </View>
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View>
                  <Text style={styles.textUser}>Stage</Text>
                  <View style={styles.inputContainer}>
                    <Text>{isStrage}</Text>
                  </View>
                </View>
                <View>
                  <Text style={styles.textUser}>Subject</Text>
                  <View style={styles.inputContainer}>
                    <Text>{issubject}</Text>
                  </View>
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View>
                  <Text style={styles.textUser}>Service Type </Text>
                  <View style={styles.inputContainer}>
                    <Text>{isservice_type}</Text>
                  </View>
                </View>
                <View>
                  <Text style={styles.textUser}>Time</Text>
                  <View style={styles.inputContainer}>
                    <Text>{istime}</Text>
                  </View>
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View>
                  <Text style={styles.textUser}>Number of Student </Text>
                  <View style={styles.inputContainer}>
                    <Text>{student}</Text>
                  </View>
                </View>
              </View>
              <View>
                <Text style={styles.textorder}>Order Details</Text>
                <View style={styles.orderDetails}>
                  <Text>{requesDetail}</Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignSelf: "flex-end",
                  marginRight: w(10)
                }}
              >
                <TouchableOpacity onPress={showLoaction}>
                  <Text
                    style={{
                      textAlign: "right",
                      textDecorationLine: "underline",
                      color: "#278876"
                    }}
                  >
                    Click here
                  </Text>
                </TouchableOpacity>
                <Text
                  style={{
                    textAlign: "right",
                    marginLeft: w(5),
                    fontWeight: "bold"
                  }}
                >
                  Location
                </Text>
              </View>
              {showLocationStatus === true && (
                <View style={{ marginTop: 30 }}>
                  <MapView
                    style={{ height: h(30) }}
                    initialRegion={{
                      latitude: Number(latitude),
                      longitude: Number(longitude),
                      latitudeDelta: 0.0922,
                      longitudeDelta: 0.0421
                    }}
                    showsUserLocation={true}
                  >
                    <Marker
                      coordinate={{
                        latitude: Number(latitude),
                        longitude: Number(longitude)
                      }}
                    />
                  </MapView>
                </View>
              )}

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-evenly",
                  marginLeft: 31,
                  marginRight: 31
                }}
              >
                <TouchableOpacity onPress={getReject}>
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={["#1c83a0", "#32cba5"]}
                    style={styles.linearGradient}
                  >
                    <Text style={styles.signUpText}>Refusal</Text>
                  </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={dialogopen}
                  // onPress={dialogopen}
                >
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={["#1c83a0", "#32cba5"]}
                    style={styles.linearGradient}
                  >
                    <Text style={styles.signUpText}>Acceptance</Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <Dialog
        visible={dialog}
        onTouchOutside={() => {
          setDialog(false);
        }}
      >
        <DialogContent style={styles.learningDialogmain}>
          <ScrollView>
            <TouchableOpacity
              onPress={() => {
                setDialog(false);
              }}
              // onPress={getStatus}
            >
              <Image
                resizeMode="contain"
                source={closeImg}
                style={styles.closeStyle}
              />
            </TouchableOpacity>
            <Text
              style={{
                textAlign: "center",
                fontSize: w(5),
                fontWeight: "bold"
              }}
            >
              Set a price
            </Text>
            <TextInput
              style={styles.inputs}
              autoCapitalize="none"
              keyboardType="number-pad"
              underlineColorAndroid="transparent"
              placeholder="Enter Price"
              onChangeText={price => setPrice(price)}
              value={price}
            />
            <TouchableOpacity
              // onPress={() => {
              //   setDialog(false);
              // }}
              onPress={getStatus}
              style={styles.okBtn}
            >
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={["#1c83a0", "#32cba5"]}
                style={styles.linearGradient}
              >
                <Text style={styles.signUpText}>Send</Text>
              </LinearGradient>
            </TouchableOpacity>
          </ScrollView>
        </DialogContent>
      </Dialog>
    </View>
  );
};
export default Request;
