import { StyleSheet } from "react-native";
import { w, h, totalSize } from "../../utils/Dimensions";
import { UiColor, TextColor, TextSize } from "../../theme";

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE
  },
  textSignUp: {
    textAlign: "center",
    fontSize: TextSize.bigSize,
    fontWeight: "bold"
  },
  textUser: {
    marginTop: w(5),
    marginVertical: w(3.5),
    fontSize: TextSize.h3,
    fontWeight: "700",
    textAlign: "center"
  },
  container: {
    marginTop: w(5),
    justifyContent: "center",
    alignItems: "center"
  },
  inputContainer: {
    backgroundColor: "#D6DCE0",
    borderRadius: w(2),
    width: w(42),
    height: h(7),
    marginBottom: w(4),
    flexDirection: "row",
    alignItems: "center",
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN,
    marginHorizontal: w(1),
    alignItems: "center",
    justifyContent: "center"
  },
  orderDetails: {
     backgroundColor: "#D6DCE0",
     borderRadius: w(2),
     width: w(85),
     height: h(17),
     marginBottom: w(4),
    // flexDirection: "row",
    //  alignItems: "center",
     borderWidth: w(0.3),
     borderColor: UiColor.GREEN,
     marginHorizontal: w(1),
     padding: 10
  },
  textorder: {
    marginVertical: w(3.5),
    fontSize: TextSize.h3,
    fontWeight: "700",
    alignSelf: "flex-end",
    marginRight: w(12)
  },
  textLocation: {
    marginVertical: w(3.5),
    fontSize: TextSize.h1,
    fontWeight: "700",

    marginRight: w(2)
  },
  textcliclhere: {
    marginVertical: w(3.5),
    fontSize: TextSize.h3,
    fontWeight: "700",
    marginRight: w(4),
    textDecorationLine: "underline",
    color: UiColor.GREEN
  },
  inputs: {
    alignSelf: "center",
    borderWidth: 1,
    height: h(6),
    width: w(60),
    marginTop: h(3),
    textAlign: "center",
    borderRadius: 5,
    backgroundColor: '#e9f8f5',
    borderColor: '#e9f8f5',
  },
  inputIcon: {
    width: w(3),
    height: h(3),
    marginLeft: w(3),
    justifyContent: "center",
    tintColor: "#2295A1"
  },
  card: {
    shadowColor: "#00000021",
    shadowOffset: {
      width: 0,
      height: 6
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,

    marginLeft: w(2),
    marginRight: w(2),
    marginTop: 20,
    backgroundColor: "white",
    padding: 8,
    borderRadius: w(2)
  },

  linearGradient: {
    margin: w(5),
    height: h(6),
    width: w(35),
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: "row",
    borderRadius: w(8)
  },

  signUpText: {
    textAlign: "center",
    color: UiColor.WHITE,
    fontSize: TextSize.h2,
    fontWeight: "bold"
  },
  textConfirmed: {
    color: UiColor.GREEN,
    fontSize: TextSize.h2,
    textDecorationLine: "underline",
    width: w(46)
  },
  textPrevious: {
    fontSize: TextSize.h2,
    color: UiColor.GRAY,
    width: w(44),
    textDecorationLine: "underline"
  },
  learningDialogmain: {
    height: h(35),
    width: w(80)
  },
  closeStyle: {
    height: h(3),
    width: w(3),
    alignSelf: "flex-end",
    marginTop: 5
  },
  okBtn: {
    height: h(6),
    width: w(25),
    backgroundColor: UiColor.GREEN,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginTop: h(5),
    borderRadius: 5
  },
  okText: {
    color: "#fff",
    fontSize: h(3),
    fontWeight: "bold"
  },
  img: {
    height: h(30),
    width: '100%'
  }
});
