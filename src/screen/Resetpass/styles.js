import { StyleSheet } from "react-native";
import { w, h, totalSize } from "../../utils/Dimensions";
import { UiColor, TextColor, TextSize } from "../../theme";

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE
  },
  textSignUp: {
    marginVertical: w(5),
    fontSize: TextSize.bigSize,
    fontWeight: "bold"
  },
  textUser: {
    marginVertical: w(3.5),
    fontSize: TextSize.h1,
    fontWeight: "400",
    color: UiColor.BLACK
  },
  resetText: {
    marginVertical: w(3.5),
    fontSize: TextSize.h4,
    fontWeight: "400",
    color: "#1B7D9F",
    textAlign: "center"
  },
  container: {
    marginTop: w(10),
    justifyContent: "center",
    alignItems: "center"
  },
  inputContainer: {
    backgroundColor: UiColor.WHITE,
    borderRadius: w(8),
    width: w(65),
    height: h(7),
    marginBottom: w(4),
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN,
    flexDirection: "row"
  },

  inputIcon: {
    width: w(4),
    height: h(4),
    tintColor: "#1A7B9F",
    alignSelf: "center"
  },
  buttonContainer: {
    height: h(7),
    width: w(70),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: w(5)
  },
  haveAccount: {
    marginTop: w(5),
    color: UiColor.BLACK,
    fontSize: TextSize.h4,
    fontWeight: "bold",
    textDecorationLine: "underline"
  },
  forgotPass: {
    marginTop: w(5),
    color: UiColor.BLACK,
    fontSize: TextSize.h4,
    fontWeight: "bold",
    textDecorationLine: "underline"
  },

  signupButton: {
    marginTop: w(5),
    backgroundColor: UiColor.GREEN
  },
  linearGradient: {
    marginTop: w(12),
    height: h(7),
    width: w(65),
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: "row",
    borderRadius: w(8)
  },

  signUpText: {
    textAlign: "center",
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    fontWeight: "bold"
  }
});
