import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithBackLogin } from "../../components/AppHeader";
import styles from "./styles";
import LinearGradient from "react-native-linear-gradient";
import { w } from "../../utils/Dimensions";
import { API_BASE_URL } from "../../constant/constant";
import Loader from '../../constant/Loader';

import AsyncStorage from "@react-native-community/async-storage";
const Resetpass = props => {
  const [mobile, setmobile] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const validation = () => {
      if (mobile == "") {
       alert("Please enter Mobile No");
     } else {
       serverApi( mobile)
     }
  };
  const serverApi = async mobile => {
    console.log("server");
    setIsLoading(true)
    fetch(API_BASE_URL + "/reset_passward", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        mobile: mobile,
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log(response);
        if (response.status == "true" || response.status === true) {
          console.log("response", response);
          Actions.OtpVerifedForgot({teacherId: response.data.teacher_id});
          AsyncStorage.setItem('teacherId', response.data.teacher_id);
        } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
  };
  return (
    <View style={styles.mainContainer}>
      <Loader loading={isLoading} />
      <ScrollView>
        {HeaderWithBackLogin(Actions.Login, "")}
        <View style={styles.container}>
          <Text style={styles.textSignUp}>Forgot Password</Text>
          <Text style={styles.resetText}>
            Please enter your mobile number to reset your password
          </Text>
          <Text style={styles.textUser}>mobile number</Text>

          <View style={{ flexDirection: "row" }}>
            <View style={styles.inputContainer}>
              {/**********
              Confirm Password
             ***********/}
              <TextInput
                style={{ width: "85%", marginLeft: w(2) }}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                placeholder="Enter mobile no."
                keyboardType="number-pad"
                maxLength={10}
                onChangeText={mobile => setmobile(mobile)}
                value={mobile}
              />
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_mobile}
                resizeMode="contain"
              />
            </View>
          </View>
          {/**********
              LoginButton
             ***********/}
          <TouchableOpacity onPress={validation}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={["#1c83a0", "#32cba5"]}
              style={styles.linearGradient}
            >
              <Text style={styles.signUpText}>{Strings.RESET}</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default Resetpass;
