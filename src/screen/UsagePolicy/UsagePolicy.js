import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import { API_BASE_URL } from "../../constant/constant";

const UsagePolicy = () => {
  const [desc, setDesc] = useState("");
  // const [password, setpassword] = useState("");
  useEffect(() => {
    getPrivancy();
   }, []);

   const getPrivancy = () => {
    fetch(API_BASE_URL + "/getUsagePolicy", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        "type" : "Teacher"
      })
    })
      .then(res => res.json())
      .then(response => {
        console.log(response.data,"data")
        if (response.result == 'true') {
          setDesc(response.data.description)
        }
      })
      .catch(error => console.log(error));
  };

  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
      <View style={styles.mainContainer}>
        <ScrollView>
        {HeaderWithGoBackAndOption(Actions.Settings,"")}
          <View style={styles.container}>
            <Text style={styles.textSignUp}>Usage Policy </Text>
            
            <Text style={{justifyContent:"center",fontSize:20,color:"grey", marginTop:10,padding:15}}>"Get Teacher Policy Successfully","description":"Lorem ipsum dolor sit amet,
consectetur adipiscing elit. "</Text>
          

         
            
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};
export default UsagePolicy;
