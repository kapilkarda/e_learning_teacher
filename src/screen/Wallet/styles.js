import {StyleSheet} from 'react-native';
import {w, h, totalSize} from '../../utils/Dimensions';
import {UiColor, TextColor, TextSize} from '../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE,
  },
  textSignUp: {
    // marginVertical: w(),
    fontSize: TextSize.bigSize,
    fontWeight: '600',
  },
  textUser: {
    marginTop: w(10),
    marginBottom:w(4),
    fontSize: TextSize.h1,
    fontWeight: 'bold',
    color:UiColor.BLACK,
    textAlign:"center",
   
  },
  balance: {
    marginTop: w(10),
    marginVertical: w(2.3),
    fontSize: TextSize.bigSize,
    fontWeight: '400',
    color:UiColor.BLACK,
    textAlign:"center"
  },
  orderDetails:{
    marginTop: w(5),
    marginVertical: w(2.3),
    fontSize: TextSize.bigSize,
    fontWeight: 'bold',
    color:UiColor.BLACK,
    textAlign:"center",
    marginBottom:w(8)
  },
  container: {
    margin:w(8),
    justifyContent: 'center',
    alignItems: 'center',
  },
  

  inputIcon: {
    width: w(4),
    height: h(4),
    marginLeft: w(3),
    justifyContent: 'center',
    tintColor:UiColor.GREEN
  },
 

  buttonContainer: {
    height: h(7),
    width: w(70),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: w(5),
  },
 

  signupButton: {
    marginTop: w(5),
    backgroundColor: UiColor.GREEN,
  },
  linearGradient: {
    margin:w(20),
    height:h(7),
    width:w(55),
    alignItems:'center',
    justifyContent:'center',
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection:"row",
    borderRadius: w(10)
  },
  
  signUpText: {
   textAlign: 'center',
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    fontWeight: 'bold',
     
   
  },
});
