import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithOption } from "../../components/AppHeader";
import styles from "./styles";
import LinearGradient from "react-native-linear-gradient";
import AsyncStorage from "@react-native-community/async-storage";
import Loader from '../../constant/Loader';
import { API_BASE_URL } from "../../constant/constant";

const Wallet = (props) => {
  const [amount, setWallet] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getWalletAmount();
    const unsubscribe = props.navigation.addListener('didFocus', () => {
      getWalletAmount();
    });
    return () => unsubscribe;
  }, []);
  const getWalletAmount = async () => {
    const teacherId = await AsyncStorage.getItem("teacherId");
    setIsLoading(true)
    fetch(API_BASE_URL + "/getTeacherWallet", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        teacher_id: teacherId,
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log(response);
        if (response.result == "true") {
          setWallet(response.data)
          
        } else {
          alert(response.msg);
        }
      })
      .catch(error => {setIsLoading(false)});
  };
  const withdrawMyAmount = async () => {
    const teacherId = await AsyncStorage.getItem("teacherId");
    setIsLoading(true)
    fetch(API_BASE_URL + "/withDrawRequest", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        teacher_id: teacherId,
        amount: amount
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log(response);
        if (response.result == "true") {
          alert(response.msg);
         
        } else {
          alert(response.msg);
        }
      })
      .catch(error => {setIsLoading(false)});
  };
  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
      <Loader loading={isLoading} />
      <View style={styles.mainContainer}>
        <ScrollView>
        {HeaderWithOption()}
          <View style={styles.container}>
          <Text style={styles.orderDetails}>Wallet</Text>
          <View style={{flexDirection:"row",backgroundColor:"#D6DCE0",width:"100%",justifyContent:"center",borderRadius:5}}>


          <Text style={styles.textUser}>
                  SAR <Text style={{ fontWeight: "700",fontSize:25 }}> {amount == '' ? '0' : amount}</Text>
                </Text>
                
          </View>
          <View
              style={{
                borderColor: "green",
                borderBottomWidth: 1,
                width: "100%",
                alignSelf: "center",
                backgroundColor:"#ccc",
              
                
              }}
            ></View>

               <View style={{backgroundColor:"#D6DCE0",width:"100%",borderRadius:5}}>
               <Text style={styles.balance}>Available Balance</Text>
               </View>

            {/**********
              LoginButton
             ***********/}
            {
              amount > '0' &&
              <TouchableOpacity onPress={withdrawMyAmount}>
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                  colors={["#1c83a0", "#32cba5"]}
                  style={styles.linearGradient}
                >
                  <Text style={styles.signUpText}>Withdraw</Text>
                </LinearGradient>
              </TouchableOpacity>
            }
            
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};
export default Wallet;
