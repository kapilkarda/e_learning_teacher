import React, { useState, useEffect  } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  StatusBar,
  Alert,
  BackHandler,
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithOptionLogin } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import { w, h } from "../../utils/Dimensions";
import firebase from "react-native-firebase";
import AsyncStorage from "@react-native-community/async-storage";
import Loader from '../../constant/Loader';
import { API_BASE_URL } from "../../constant/constant";
const Login = props => {
  const [mobile, setmobile] = useState("");
  const [password, setpassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [loginData, setLoginData] = useState({});

  useEffect(() => {
    getToken();
  });
  const validation = () => {
    handleLogin(mobile, password);
  };

  const getToken = async () => {
    const fcmToken = await firebase.messaging().getToken();
    console.log(fcmToken)
    AsyncStorage.setItem("device_token", fcmToken);
    let token = await AsyncStorage.getItem('auth_token');
    if (token) {
      if (loginData) {
        if (loginData.profile_status == '0') {
          Actions.SignupSec();
  
        } else {
          Actions.Alert();
        }
      } else {
        Actions.Alert();
      }
      
    }
  };
  var padding
  Platform.select({
    ios: () => {
      padding = 'padding'
    },
    android: () => {
      padding = 0
    }
  })();
  const handleLogin = (mobileNo, password) => {
    if (mobileNo == "") {
      alert("Please enter mobile No");
    } else if (password == "") {
      alert("Please enter password");
    } else {
      serverApi(mobileNo, password)
    }
  };
  const serverApi = async (mobileNo, password) => {
    setIsLoading(true)
    const token = await AsyncStorage.getItem("device_token")
    // console.log("token", token)
    fetch(API_BASE_URL + "/Teacherlogin", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        password: password,
        usermobile: mobileNo,
        device_type: "android",
        device_token: token
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        if (response.result == 'true') {
          console.log(response);
          AsyncStorage.setItem('teacherId', response.data.teacher_id);
          AsyncStorage.setItem('teacher_phone', response.data.teacher_phone);
          AsyncStorage.setItem('teacherName', response.data.teacher_name);
          AsyncStorage.setItem('profileSetupStatus', response.data.profile_status);
          AsyncStorage.setItem('auth_token', response.token);
          setIsLoading(false)
          setLoginData(response.data)
          if (response.data.profile_status == "0") {
            // Alert.alert(
            //   'Alert',
            //   response.msg,
            //   [
            //     { text: 'OK', onPress: () => Actions.SignupSec() },
            //   ],
            //   { cancelable: false },
            // );
            Actions.SignupSec()
          } else if (response.data.profile_status == "1") {
            Actions.Alert()
            // Alert.alert(
            //   'Alert',
            //   response.msg,
            //   [
            //     { text: 'OK', onPress: () => Actions.Alert() },
            //   ],
            //   { cancelable: false },
            // );
          }
          else {
            Alert.alert(response.msg);
          }
        } else {
          Alert.alert(response.msg);
          setIsLoading(false)
        }
      })
      .catch(error => console.log(error));
  };
  return (
    <KeyboardAvoidingView
      behavior={padding}
      enabled style={{ flex: 1 }}>
      <Loader loading={isLoading} />
      <View style={styles.mainContainer}>
        <StatusBar />
        {HeaderWithOptionLogin()}
        <ScrollView>
          <View style={styles.container}>
            <Text style={styles.textSignUp}>Login</Text>
            <Text style={styles.textUser}>Mobile number</Text>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon1}
                source={IconAsset.ic_mobile}
                resizeMode="contain"
              />
              {/**********
              MobileNumber
             ***********/}
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                keyboardType="number-pad"
                maxLength={10}
                onChangeText={mobile => setmobile(mobile)}
                value={mobile}
              />
            </View>
            <Text style={styles.textUser}>Password</Text>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_lock}
                resizeMode="contain"
              />
              {/**********
               Password
              ***********/}
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                secureTextEntry={true}
                underlineColorAndroid="transparent"
                onChangeText={password => setpassword(password)}
                value={password}
              />
            </View>

            {/**********
              LoginButton
             ***********/}

            <View style={{ marginTop: 25 }}>
              <TouchableOpacity onPress={validation}>
                {/* <Image source={require("../../asssets/icons/button.png")} /> */}

                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                  colors={["#1c83a0", "#32cba5"]}
                  style={styles.linearGradient}
                >
                  <Text style={styles.signUpText}>{Strings.SIGN_IN}</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
            {/**********
              FogotText
             ***********/}
            <TouchableOpacity
              onPress={Actions.Resetpass}
              style={styles.forgetTouch}
            >
              <Text style={styles.forgotPass}>{Strings.FORGOT_PASS}</Text>
            </TouchableOpacity>
            {/**********
              SignUp
             ***********/}
            <TouchableOpacity onPress={Actions.SignUp}>
              <Text style={styles.haveAccount}>{Strings.HAVE_AN_ACCOUNT}</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};
export default Login;
