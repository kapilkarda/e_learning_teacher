import React from "react";
import { View, Animated } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { Actions } from "react-native-router-flux";
import styles from "./styles";

class Splash extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fadeValue: new Animated.Value(0)
    }
  }

  componentWillMount() {
    try {
      setTimeout(async () => {
        let token = await AsyncStorage.getItem('auth_token');
        console.log('token', token);
        if (token) {
          let setupStatus = await AsyncStorage.getItem('profileSetupStatus');
          if (setupStatus == '0') {
            Actions.SignupSec();
          } else {
            Actions.Alert();
          }
        } else {
          Actions.Login();
        }
      }, 1500);
    } catch (error) {
      console.log('error' + error);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Animated.Image
          source={require("../../asssets/icons/logo.png")}
          style={styles.splashImg}
        />
      </View>
    );
  }

}

export default Splash;
