import { StyleSheet } from "react-native";
import { h, w } from "../../utils/Dimensions";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    justifyContent: "center",
    alignContent: "center"
  },
  splashImg: {
  
    height: h(100),
    width: w(100),
    alignSelf: "center"
  }
});
