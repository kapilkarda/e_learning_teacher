import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithGoBackAndOption,HeaderWithBackLogin } from "../../components/AppHeader";
import styles from "./styles";
import LinearGradient from "react-native-linear-gradient";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import AsyncStorage from "@react-native-community/async-storage";
import { API_BASE_URL } from "../../constant/constant";
import Loader from '../../constant/Loader';

const otpVerifed = (props) => {
  const [code, setcode] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const validation = async () => {
    if (code == "") {
      alert("Please enter code");
    }  else {
      setIsLoading(true);
      let mobileNo = await AsyncStorage.getItem("teacherPhone");
      let teacherId = await AsyncStorage.getItem("teacherId");
      fetch(API_BASE_URL + "/Check_teacher_otp", {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/json",
          Accept: "application/json"
        }),
        body: JSON.stringify({
          mobile: mobileNo,
          otp: code.code,
          teacher_id: teacherId
        })
      })
        .then(res => res.json())
        .then(response => {
          setIsLoading(false);
          if (response.result == "true") {
            console.log("response", response);
            Actions.createPass();
          } else {
            alert(response.msg);
          }
        })
        .catch(error => {
          setIsLoading(false);
          console.log(error)
        });
    }
  };

  
  return (
    <View style={styles.mainContainer}>
      <Loader loading={isLoading} />
      <ScrollView>
        {HeaderWithBackLogin(Actions.Login, "")}
        <View style={styles.container}>
          <Image
            style={styles.inputIcon}
            source={IconAsset.ic_right}
            resizeMode="contain"
          />
          <Text style={styles.textUser}>
            Please enter the number sent on your phone
          </Text>
          <View style={styles.inputContainer}>
            {/**********
              MobileNumber
             ***********/}

            <OTPInputView
              style={{
                width: "80%",
                height: 100,
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center"
              }}
              pinCount={4}
              // code={code}
              onCodeChanged = {code => { setcode({code})}}
              autoFocusOnLoad
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              onCodeFilled={code => {
                console.log(`Code is ${code}, you are good to go!`);
              }}
            />
          </View>

          {/**********
              LoginButton
             ***********/}
          <TouchableOpacity onPress={validation}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={["#1c83a0", "#32cba5"]}
              style={styles.linearGradient}
            >
              <Text style={styles.signUpText}>{Strings.CONFIRM}</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default otpVerifed;
