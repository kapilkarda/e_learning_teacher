import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Alert
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import Loader from '../../constant/Loader';
import { API_BASE_URL } from "../../constant/constant";
import AsyncStorage from "@react-native-community/async-storage";

const AccountSetting = (props) => {
  const [userName, setUserName] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getAccount();
    const unsubscribe = props.navigation.addListener('didFocus', () => {
      getAccount();
    });
    return () => unsubscribe;
  }, []);
  const getAccount = async () => {
    const teacherId = await AsyncStorage.getItem("teacherId");
    setIsLoading(true)
    fetch(API_BASE_URL + "/getTeacherBankDetails", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        teacher_id: teacherId,
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log(response);
        if (response.result == "true") {
          setUserName(response.data.username)
          setMobileNumber(response.data.usermobile)
        } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
  };
  const updateUserAcount = async () => {
    if (userName == "") {
      alert("Please enter user name");
    } else if (mobileNumber == "") {
      alert("Please enter mobile number");
    } else {
      const teacherId = await AsyncStorage.getItem("teacherId");
      setIsLoading(true)
      fetch(API_BASE_URL + "/accountSetting", {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/json",
          Accept: "application/json"
        }),
        body: JSON.stringify({
          teacher_id: teacherId,
          username: userName,
          usermobile: mobileNumber,
        })
      })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log(response);
        if (response.result == "true") {
          AsyncStorage.setItem('teacherName', userName);
          Alert.alert(
            'Alert',
            response.msg,
            [
              { 
                text: 'OK'
              },
            ],
            { cancelable: true },
          );
        } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
    }
  };
  return (
   
      <View style={styles.mainContainer}>
        <Loader loading={isLoading} />
        <ScrollView>
        {HeaderWithGoBackAndOption(Actions.Settings,"")}
          <View style={styles.container}>
            <Text style={styles.textSignUp}> Account Settings </Text>
           <View style={{marginTop:35}}>
           <Text style={styles.textUser}>User Name</Text>
           </View>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_user}
                resizeMode="contain"
              />
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                // placeholder={"Enter number"}
                onChangeText={userName => setUserName(userName)}
                value={userName}
              />
            </View>
            <Text style={styles.textUser}>Mobile number</Text>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_mobile}
                resizeMode="contain"
              />
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                keyboardType="number-pad"
                underlineColorAndroid="transparent"
                editable={false}
                onChangeText={mobileNumber => setMobileNumber(mobileNumber)}
                value={mobileNumber}
              />
            </View>
            <TouchableOpacity onPress={updateUserAcount}>
              {/* <Image source={require("../../asssets/icons/button.png")} /> */}
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={["#1c83a0", "#32cba5"]}
                style={styles.linearGradient}
              >
               <Text style={styles.signUpText}>Update</Text>

              </LinearGradient>
              
            </TouchableOpacity>
           
          </View>
        </ScrollView>
      </View>

  );
};
export default AccountSetting;
