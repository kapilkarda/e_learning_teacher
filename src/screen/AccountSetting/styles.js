import {StyleSheet} from 'react-native';
import {w, h, totalSize} from '../../utils/Dimensions';
import {UiColor, TextColor, TextSize} from '../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE,
  },
  textSignUp: {
   
    fontSize: TextSize.bigSize,
    fontWeight: 'bold',
  },
  textUser: {
    marginVertical: w(3.5),
    fontSize: TextSize.h1,
    fontWeight: '200',
    margin:45
  },
  container: {
    marginTop: w(12),
    justifyContent: 'center',
    alignItems: 'center',
  
  },
  inputContainer: {
    backgroundColor: UiColor.WHITE,
    borderRadius: w(8),
    width: w(65),
    height: h(7),
    marginBottom: w(4),
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: w(0.3),
    borderColor:UiColor.GREEN,
 
  },

  inputIcon: {
    width: w(4),
    height: h(4),
    marginLeft: w(3),
    justifyContent: 'center',
    tintColor:UiColor.GREEN
  },
  buttonContainer: {
    height: h(7),
    width: w(65),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: w(5),
  },
  haveAccount: {
    marginTop: w (5),
    color:TextColor.BLUE,
    fontSize: TextSize.h4,
    fontWeight: "bold",
    textDecorationLine: "underline"
  },
  forgotPass: {
    marginTop:w(1),
    color: UiColor.BLACK,
    fontSize: TextSize.h4,
    fontWeight: "bold",
    textDecorationLine: "underline",
    marginLeft:w(45),
    color:TextColor.GREEN
   
  },
  linearGradient: {
    margin:w(5),
    height:h(7),
    width:w(65),
    alignItems:'center',
    justifyContent:'center',
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection:"row",
    borderRadius: w(8)
  },
  
  signUpText: {
   textAlign: 'center',
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    fontWeight: 'bold',
     
   
  },
  inputs:{
    width: w(55),
    paddingLeft: w(4)
  },
});
