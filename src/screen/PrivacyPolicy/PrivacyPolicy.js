import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import { API_BASE_URL } from "../../constant/constant";

const PrivacyPolicy = () => {

  const [desc, setDesc] = useState('')
  
  useEffect(() => {
   getPrivancy();
  }, []);

  const getPrivancy = () => {
    fetch(API_BASE_URL + "/getPrivacyPolicy", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        "type" : "Teacher"
      })

    })
      .then(res => res.json())
      .then(response => {
        if (response.result == 'true') {
          setDesc(response.description.description)
        }
      })
      .catch(error => console.log(error));
  };
  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
      <View style={styles.mainContainer}>
        <ScrollView>
        {HeaderWithGoBackAndOption(Actions.Settings,"")}
          <View style={styles.container}>
            <Text style={styles.textSignUp}> PrivacyPolicy </Text>
  <Text style={{justifyContent:"center",fontSize:20,color:"grey", marginTop:10,padding:15}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</Text>
         
          

         
            
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};
export default PrivacyPolicy;
