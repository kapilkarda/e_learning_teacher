import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings} from '../../theme';
import {HeaderWithGoBackAndOption,HeaderWithBackLogin} from '../../components/AppHeader';
import styles from './styles';
import withError from './withError'



const createPass = (props) => {
  const [password, setpassword] = useState('');
  const [cpassword, setcpassword] = useState('');

  const validation = () => {
    withError.handleCnfPass(password,cpassword);
  };

  return (
    
      <View style={styles.mainContainer}>
        <ScrollView>
        {HeaderWithBackLogin(Actions.Login,"")}
          <View style={styles.container}>
            <Text style={styles.textSignUp}>Create a password</Text>
            <Text style={styles.textUser}>Password</Text>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_lock}
                resizeMode="contain"
              />
              {/**********
             Password
             ***********/}

              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                secureTextEntry={true}
                onChangeText={password => setpassword(password)}
                value={password}
              />
            </View>
            <Text style={styles.textUser}>Confirm password</Text>
            <View style={styles.inputContainer}>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_lock}
                resizeMode="contain"
              />
              {/**********
               Confirm Password
              ***********/}
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                secureTextEntry={true}
                underlineColorAndroid="transparent"
                onChangeText={cpassword => setcpassword(cpassword)}
                value={cpassword}
              />
            </View>

            {/**********
              Enter Arrow
             ***********/}
             <TouchableOpacity onPress={validation}>
            <Image
              style={styles.inputArrowIcon}
              source={IconAsset.ic_right}
              resizeMode="contain"
            />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
  
  );
};
export default createPass;
