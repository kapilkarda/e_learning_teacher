import serverCall from "./serverCall";

const handleCnfPass = (password,cpassword) => {
  console.log(password, cpassword)
  if (password == "") {
    alert("Please enter password");
  } else if (password.length < 6) {
    alert("Please enter minimum 6 digit password");
  } else if (cpassword == ""){
    alert("Please enter confirm password");
  }  else if (password != cpassword){
    alert("Password doesn't match");
  }
  else {
    serverCall.serverApi(password,cpassword)
  }
};

export default {
  handleCnfPass
};
