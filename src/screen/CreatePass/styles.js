import {StyleSheet} from 'react-native';
import {w, h, totalSize} from '../../utils/Dimensions';
import {UiColor, TextColor, TextSize} from '../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE,
  },
  textSignUp: {
  marginTop: w(15),
    fontSize: TextSize.bigSize,
    fontWeight: 'bold',
  },
  textUser: {
    // marginVertical: w(15),
    marginTop:w(10),
    fontSize: TextSize.h1,
    fontWeight: '400',
    marginBottom:w(5)
  },
  container: {
   
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    backgroundColor: UiColor.WHITE,
    borderRadius: w(8),
    width: w(65),
    height: h(7),
    marginBottom: w(4),
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: w(0.3),
    borderColor:UiColor.GREEN
  },

  inputIcon: {
    width: w(3),
    height: h(3),
    marginLeft: w(3),
    justifyContent: 'center',
    tintColor:"#1A7B9F"
   
  },
  inputArrowIcon: {
    width: w(20),
    height: h(20),
    marginLeft: w(3),
    justifyContent: 'center',
    
  },
  buttonContainer: {
    height: h(7),
    width: w(65),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: w(5),
  },

  signupButton: {
    marginTop: w(5),
    backgroundColor: UiColor.GREEN,
  },
  signUpText: {
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    fontWeight: 'bold',
  },
  inputs:{
    width: w(53),
    marginLeft: w(3)
  },
});
