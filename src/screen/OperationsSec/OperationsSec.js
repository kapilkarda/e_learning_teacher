import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings} from '../../theme';
import {HeaderWithGoBackAndOption} from '../../components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';

const OperationsSec = () => {
  const [mobile, setmobile] = useState('');
  const [password, setpassword] = useState('');

  return (
    <View style={styles.mainContainer}>
      {HeaderWithGoBackAndOption(Actions.Settings, '')}
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.textSignUp}>Operation</Text>

          <View style={styles.container}>
            <View style={styles.card}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: '95%',
                }}>
                <Text style={styles.textConfirmed}>Confirmed operations </Text>
                <Text style={styles.textPrevious}> Previous operations </Text>
              </View>
            </View>

            <View style={styles.card}>
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Text style={styles.textUser}>Date</Text>
                  <View style={styles.inputContainer}>
                    {/**********
            Teacher Name
           ***********/}

                    <TextInput
                      style={styles.inputs}
                      autoCapitalize="none"
                      underlineColorAndroid="transparent"
                      placeholder="6 October 2019"
                    />
                  </View>
                </View>
                <View>
                  <Text style={styles.textUser}>Service Selector</Text>
                  <View style={styles.inputContainer}>
                    {/**********
            Teacher Name
           ***********/}

                    <TextInput
                      style={styles.inputs}
                      autoCapitalize="none"
                      underlineColorAndroid="transparent"
                      placeholder="Student"
                    />
                  </View>
                </View>
              </View>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_down}
                resizeMode="contain"
              />
            </View>
            <View style={styles.card}>
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Text style={styles.textUser}>Date</Text>
                  <View style={styles.inputContainer}>
                    {/**********
            Teacher Name
           ***********/}

                    <TextInput
                      style={styles.inputs}
                      autoCapitalize="none"
                      underlineColorAndroid="transparent"
                      placeholder="6 October 2019"
                    />
                  </View>
                </View>
                <View>
                  <Text style={styles.textUser}>Service Selector</Text>
                  <View style={styles.inputContainer}>
                    {/**********
            Teacher Name
           ***********/}

                    <TextInput
                      style={styles.inputs}
                      autoCapitalize="none"
                      underlineColorAndroid="transparent"
                      placeholder="Student"
                    />
                  </View>
                </View>
              </View>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_down}
                resizeMode="contain"
              />
            </View>

            <View style={styles.card}>
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Text style={styles.textUser}>Date</Text>
                  <View style={styles.inputContainer}>
                    {/**********
            Teacher Name
           ***********/}

                    <TextInput
                      style={styles.inputs}
                      autoCapitalize="none"
                      underlineColorAndroid="transparent"
                      placeholder="6 October 2019"
                    />
                  </View>
                </View>
                <View>
                  <Text style={styles.textUser}>Service Selector</Text>
                  <View style={styles.inputContainer}>
                    {/**********
            Teacher Name
           ***********/}

                    <TextInput
                      style={styles.inputs}
                      autoCapitalize="none"
                      underlineColorAndroid="transparent"
                      placeholder="Student"
                    />
                  </View>
                </View>
              </View>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_down}
                resizeMode="contain"
              />
            </View>
            <View style={styles.card}>
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Text style={styles.textUser}>Date</Text>
                  <View style={styles.inputContainer}>
                    {/**********
            Teacher Name
           ***********/}

                    <TextInput
                      style={styles.inputs}
                      autoCapitalize="none"
                      underlineColorAndroid="transparent"
                      placeholder="6 October 2019"
                    />
                  </View>
                </View>
                <View>
                  <Text style={styles.textUser}>Service Selector</Text>
                  <View style={styles.inputContainer}>
                    {/**********
            Teacher Name
           ***********/}

                    <TextInput
                      style={styles.inputs}
                      autoCapitalize="none"
                      underlineColorAndroid="transparent"
                      placeholder="Student"
                    />
                  </View>
                </View>
              </View>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_down}
                resizeMode="contain"
              />
            </View>
            <View style={styles.card}>
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Text style={styles.textUser}>Date</Text>
                  <View style={styles.inputContainer}>
                    {/**********
            Teacher Name
           ***********/}

                    <TextInput
                      style={styles.inputs}
                      autoCapitalize="none"
                      underlineColorAndroid="transparent"
                      placeholder="6 October 2019"
                    />
                  </View>
                </View>
                <View>
                  <Text style={styles.textUser}>Service Selector</Text>
                  <View style={styles.inputContainer}>
                    {/**********
            Teacher Name
           ***********/}

                    <TextInput
                      style={styles.inputs}
                      autoCapitalize="none"
                      underlineColorAndroid="transparent"
                      placeholder="Student"
                    />
                  </View>
                </View>
              </View>
              <Image
                style={styles.inputIcon}
                source={IconAsset.ic_down}
                resizeMode="contain"
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
export default OperationsSec;
