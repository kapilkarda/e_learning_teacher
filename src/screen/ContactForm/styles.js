import { StyleSheet } from "react-native";
import { w, h, totalSize } from "../../utils/Dimensions";
import { UiColor, TextColor, TextSize } from "../../theme";

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: UiColor.WHITE
  },
  textSignUp: {
    fontSize: TextSize.bigSize,
    fontWeight: "bold",
    marginTop: w(15)
  },
  textUser: {
    marginVertical: w(3.5),
    fontSize: TextSize.h1,
    fontWeight: "200"
  },
  container: {
    //  marginTop: w(30),
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 35
  },
  inputContainer: {
    backgroundColor: "#F1F2F4",
    marginTop: w(1),
    borderRadius: w(2),
    width: w(75),
    height: h(7),
    marginBottom: w(4),
    flexDirection: "row",
    alignItems: "center",
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN,
  
  },

  inputIcon: {
    width: w(3),
    height: h(3),
    marginLeft: w(3),
    justifyContent: "center",
    tintColor: UiColor.GREEN
  },
  buttonContainer: {
    height: h(7),
    width: w(75),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: w(5)
  },
  haveAccount: {
    marginTop: w(5),
    color: TextColor.BLUE,
    fontSize: TextSize.h4,
    fontWeight: "bold",
    textDecorationLine: "underline"
  },
  forgotPass: {
    marginTop: w(1),
    color: UiColor.BLACK,
    fontSize: TextSize.h4,
    fontWeight: "bold",
    textDecorationLine: "underline",
    marginLeft: w(45),
    color: TextColor.GREEN
  },
  linearGradient: {
    margin: w(5),
    height: h(7),
    width: w(55),
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: "row",
    borderRadius: w(8)
  },

  signUpText: {
    textAlign: "center",
    color: UiColor.WHITE,
    fontSize: TextSize.h1,
    fontWeight: "bold"
  },
  textAreaContainer: {
    backgroundColor: "#F4F1F0",
    marginTop: w(5),
    borderRadius: w(4),
    width: w(75),
    height: h(24),
    borderWidth: w(0.3),
    borderColor: UiColor.GREEN
  },
  inputs: {
    width: w(50),
    paddingLeft: 10,
    paddingRight: 10,
  },
  TextInputStyleClass: {
    paddingLeft: 10,
    paddingRight: 10

  }
});
