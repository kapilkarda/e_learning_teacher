import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Alert
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithOption } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import { API_BASE_URL } from "../../constant/constant";
import Loader from '../../constant/Loader';

const ContactForm = () => {
  const [name, setname] = useState("");
  const [email, setemail] = useState("");
  const [message, setcontent] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const validation = () => {
    handleValidation(name, email, message);
  };

  var padding
  Platform.select({
      ios: () => {
          padding = 'padding'
      },
      android: () => {
          padding = 0
      }
  })();
  const handleValidation = ( name,email,message) => {
    if (name == "") {
      alert("Please enter name");
    } else if (email == "") {
      alert("Please enter email");
    } else if (message == "") {
      alert("Please enter message");
    } else {
      serverApi(name, email,message)
    }
  };
  const serverApi = (name, email,message) => {
    setIsLoading(true)
    fetch(API_BASE_URL + "/contact_form", {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
      }),
      body: JSON.stringify({
        name: name,
        email: email,
        message    : message,
        type       : "teacher"
      })
    })
      .then(res => res.json())
      .then(response => {
        setIsLoading(false)
        console.log(response);
        if (response.result == "true") {
          Alert.alert(
            'Alert',
            response.msg,
            [
              { text: 'OK', onPress: () => Actions.Alert() },
            ],
            { cancelable: false },
          );
        } else {
          alert(response.msg);
        }
      })
      .catch(error => console.log(error));
  };
  return (
    <KeyboardAvoidingView
    behavior = {padding}
   enabled style={{flex:1}}>
      <Loader loading={isLoading} />
      <View style={styles.mainContainer}>
        <ScrollView>
          {HeaderWithOption()}
          <View style={styles.container}>
            <Text style={styles.textSignUp}> Contact Form</Text>
           <View style={{marginTop:35}}>
           
           </View>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                placeholder={"Name"}
                onChangeText={name => setname(name)}
                value={name}
              />
            </View>
      
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                autoCapitalize="none"
                placeholder={"Email"}
                underlineColorAndroid="transparent"
                onChangeText={email => setemail(email)}
                value={email}
              />
            </View>
            <View style={styles.textAreaContainer}>
            
              <TextInput
                   style={styles.TextInputStyleClass}
                   underlineColorAndroid="transparent"
                   placeholder={"Message content"}
                   placeholderTextColor={"#9E9E9E"}
                   onChangeText={message => setcontent(message)}
                   value={message}
                   multiline={true}
              />
            
            </View>
            
            <TouchableOpacity onPress={validation}>
              {/* <Image source={require("../../asssets/icons/button.png")} /> */}
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={["#1c83a0", "#32cba5"]}
                style={styles.linearGradient}
              >
               <Text style={styles.signUpText}>Send</Text>

              </LinearGradient>
              
            </TouchableOpacity>
           
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};
export default ContactForm;
