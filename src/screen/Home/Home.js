import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import { IconAsset, Strings } from "../../theme";
import { HeaderWithGoBackAndOption } from "../../components/AppHeader";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";

const Home = () => {
  

  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
      <View style={styles.mainContainer}>
        <ScrollView>
          {HeaderWithGoBackAndOption(Actions.SelectRegister,"")}
          <View style={styles.container}>
            <Text style={styles.textSignUp}> Home </Text>
            
         
          

         
            
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};
export default Home;
