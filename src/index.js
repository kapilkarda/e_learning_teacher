import React from "react";
import { Scene, Router, Drawer, Stack, Actions } from "react-native-router-flux";
import { Dimensions, Image, YellowBox, BackHandler, Alert } from "react-native";
// import Loader from "../../components/Loader"
import AsyncStorage from "@react-native-community/async-storage";
import Splash from "./screen/Splash";
import SignUp from "./screen/SignUp";
import Login from "./screen/Login";
import otpVerifed from "./screen/OtpVerifed";
import createPassword from "./screen/CreatePass";
import Resetpass from "./screen/Resetpass";
import ConfirmPass from "./screen/ConfirmPass";
import AlertComponent from "./screen/Alert";
import Wallet from "./screen/Wallet";
import ContactForm from "./screen/ContactForm";
import AccountSetting from "./screen/AccountSetting";
import BankAccount from "./screen/BankAccount";
import UsagePolicy from "./screen/UsagePolicy";
import PrivacyPolicy from "./screen/PrivacyPolicy";
import RateApp from "./screen/RateApp";
import Settings from "./screen/Settings";
import DrawerBar from "./screen/DrawerBar";
import Operations from "./screen/Operations";
import OperationsSec from "./screen/OperationsSec";
import OperationConf from "./screen/OperationConf";
import ChangePassword from "./screen/ChangePassword";
import Request from "./screen/Request";
import { connect } from "react-redux";
import SignupSec from "./screen/SignupSec";
import firebase from "react-native-firebase";
import Favorite from "./screen/Favorite";
import OtpVerifedForgot from "./screen/OtpVerifedForgot"
var width = Dimensions.get("window").width;
const RouterWithRedux = connect()(Router);

class Root extends React.Component {
  constructor(props) {
    super(props);
    firebase.notifications().onNotificationOpened(this.onPressNotificacion);
    this.state = {

    };
  }
  handleBackButton = () => {
    Alert.alert(
      "Teacher App",
      "Are you sure you want to exit from App?",
      [
        { text: "cancel", onPress: () => console.log("Cancel Pressed") },
        { text: "OK", onPress: () => BackHandler.exitApp() }
      ],
      { cancelable: false }
    );
    return true;
  };
  onPressNotificacion = async (res) => {
    console.log(res);
    // this.navigator.dispatch(NavigationActions.navigate({ routeName: 'somescreen', params: someParams }));
    let token = await AsyncStorage.getItem('auth_token');
    if (token) {
      Actions.Alert();
    } else {
      Actions.Login();
    }
  }
  async componentDidMount() {
    //for when app is opened / in background and notification received
    this.removeNotificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const notification = notificationOpen.notification;
      console.log('notification', notification)
      if (notification) {
        AsyncStorage.setItem("notify", "notify")
      }
    });

    
    //for when app is killed / not running and notification is clicked
    firebase.notifications().getInitialNotification()
      .then((notificationOpen) => {
        if (notificationOpen) {
          // Get information about the notification that was opened
          const notification = notificationOpen.notification;
          console.log('notification', notification)
          // this.handleNotification(notification);
        }
      });

    //for when app is opened / in foreground and notification received
    this.removeNotificationListener = firebase.notifications().onNotification((notification) => {
      console.log('notification', notification)
      // console.log("notification", notification)
      // const channel = new firebase.notifications.Android.Channel(notification._body, notification._title, firebase.notifications.Android.Importance.High)
      // firebase.notifications().android.createChannel(channel);
      // let notification_to_be_displayed = new firebase.notifications.Notification({
      //   data: notification.data,
      //   sound: 'default',
      //   show_in_foreground: true,
      //   title: notification.title,
      //   body: notification.body,
      // });
      // if (Platform.OS == "android") {
      //   notification_to_be_displayed
      //     .android.setPriority(firebase.notifications.Android.Priority.High)
      //     .android.setChannelId("Default")
      //     .android.setVibrate(1000);
      // }
      // firebase.notifications().displayNotification(notification_to_be_displayed);
      const channelId = new firebase.notifications.Android.Channel("Default", "Default", firebase.notifications.Android.Importance.High);
      firebase.notifications().android.createChannel(channelId);

      let notification_to_be_displayed = new  firebase.notifications.Notification({
          data: notification.data,
          sound: 'default',
          show_in_foreground: true,
          title: notification.title,
          body: notification.body,
      });

      if(Platform.OS == "android") {
        notification_to_be_displayed
        .android.setPriority(firebase.notifications.Android.Priority.High)
        .android.setChannelId("Default")
        .android.setVibrate(1000);
      }

      firebase.notifications().displayNotification(notification_to_be_displayed);
    });
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount = () => {
    this.removeNotificationOpenedListener();
    this.removeNotificationListener();
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  };

  render() {
    return (
      <RouterWithRedux>
        <Scene key="root" hideTabBar hideNavBar>
          <Stack key="app">
            <Scene
              component={Splash}
              hideNavBar={true}
              key="Splash"
              title="Splash"
              initial={true}
            />
            <Scene
              component={Login}
              hideNavBar={true}
              key="Login"
              title="Login"

            />
            <Scene
              component={SignUp}
              hideNavBar={true}
              key="SignUp"
              title="SignUp"
            />
            <Scene
              component={otpVerifed}
              hideNavBar={true}
              key="otpVerifed"
              title="otpVerifed"
            />
            <Scene
              component={createPassword}
              hideNavBar={true}
              key="createPass"
              title="createPassword"
            // initial={true}
            />
            <Scene
              component={Resetpass}
              hideNavBar={true}
              key="Resetpass"
              title="Resetpass"
            />
            <Scene
              component={OtpVerifedForgot}
              hideNavBar={true}
              key="OtpVerifedForgot"
              title="OtpVerifedForgot"
            />
            <Scene
                component={ChangePassword}
                hideNavBar={true}
                key="ChangePassword"
                title="ChangePassword"
                wrap={false}
              />
            <Drawer
              hideNavBar
              key="drawer"
              onExit={() => {
                console.log("Drawer closed");
              }}
              onEnter={() => {
                console.log("Drawer opened");
              }}
              contentComponent={DrawerBar}
              drawerWidth={width - 100}
            >
              <Scene
                component={Login}
                hideNavBar={true}
                key="Login"
                title="Login"
                wrap={false}
              />
              <Scene
                component={ConfirmPass}
                hideNavBar={true}
                key="ConfirmPass"
                title="ConfirmPass"
                wrap={false}
              />

              <Scene
                component={Wallet}
                hideNavBar={true}
                key="Wallet"
                title="Wallet"
                wrap={false}
              />

              <Scene
                component={ContactForm}
                hideNavBar={true}
                key="ContactForm"
                title="ContactForm"
                wrap={false}
              />
              <Scene
                component={AccountSetting}
                hideNavBar={true}
                key="AccountSetting"
                title="AccountSetting"
                wrap={false}
              />
              <Scene
                component={BankAccount}
                hideNavBar={true}
                key="BankAccount"
                title="BankAccount"
                wrap={false}
              />
              <Scene
                component={UsagePolicy}
                hideNavBar={true}
                key="UsagePolicy"
                title="UsagePolicy"
                wrap={false}
              />
              <Scene
                component={PrivacyPolicy}
                hideNavBar={true}
                key="PrivacyPolicy"
                title="PrivacyPolicy"
                wrap={false}
              />
              <Scene
                component={RateApp}
                hideNavBar={true}
                key="RateApp"
                title="RateApp"
                wrap={false}
              />
              <Scene
                component={Settings}
                hideNavBar={true}
                key="Settings"
                title="Settings"
                wrap={false}
              />
              <Scene
                component={AlertComponent}
                hideNavBar={true}
                key="Alert"
                title="Alert"
                wrap={false}
              />
              <Scene
                component={Operations}
                hideNavBar={true}
                key="Operations"
                title="Operations"
                wrap={false}

              />
              <Scene
                component={OperationsSec}
                hideNavBar={true}
                key="OperationsSec"
                title="OperationsSec"
                wrap={false}
              />
              <Scene
                component={OperationConf}
                hideNavBar={true}
                key="OperationConf"
                title="OperationConf"
                wrap={false}
              />
              <Scene
                component={Request}
                hideNavBar={true}
                key="Request"
                title="Request"
                wrap={false}
              />
              <Scene
                component={SignupSec}
                hideNavBar={true}
                key="SignupSec"
                title="SignupSec"
                wrap={false}
              />
              {/* <Scene
                component={ChangePassword}
                hideNavBar={true}
                key="ChangePassword"
                title="ChangePassword"
                wrap={false}
              /> */}
              <Scene
                component={Favorite}
                hideNavBar={true}
                key="Favorite"
                title="Favorite"
                wrap={false}
              />
              
            </Drawer>
          </Stack>
        </Scene>
      </RouterWithRedux>
    );
  };
}

// const mapStateToProps = state => {
//   return {
//     login: state.login
//   };
// };

// export default connect(mapStateToProps)(Root);
// console.disableYellowBox = true;
// YellowBox.ignoreWarnings([
//   "DrawerLayoutAndroid drawerPosition",
//   "componentWillReceiveProps",
//   "componentWillReceiveProps is deprecated",
//   "DrawerLayoutAndroid.positions"
// ]);
export default Root;
console.disableYellowBox = true;
// YellowBox.ignoreWarnings([
//   "DrawerLayoutAndroid drawerPosition",
//   "componentWillReceiveProps",
//   "componentWillReceiveProps is deprecated",
//   "DrawerLayoutAndroid.positions"
// ]);
