import React from "react";
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  ImageBackground
} from "react-native";
import { IconAsset, Strings, UiColor } from "../../theme";
import { styles } from "./styles";
import { Actions } from "react-native-router-flux";
import LinearGradient from 'react-native-linear-gradient';
export const HeaderView = (onMenuClicked, leftIcon, title, onPerformSearch) => {
  return (
    <View style={styles.headerView}>
      <TouchableOpacity onPress={onMenuClicked}>
        <Image
          source={leftIcon}
          style={styles.icon_menu}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <View style={{ position: "absolute", left: 0, right: 0 }}>
        <Text style={styles.header_title}>{title}</Text>
      </View>
      <View style={styles.right_items}>
        <TouchableOpacity onPress={onPerformSearch}>
          <Image
            source={IconAsset.ic_card}
            style={styles.icon_card}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};
export const HeaderWithTitle = (onGoBack, title, customStyle = {}) => {
  return (
    <View style={[styles.headerView, customStyle]}>
      <View style={{ position: "absolute", left: 0, right: 0 }}>
        <Text style={styles.header_title}>{title}</Text>
      </View>
    </View>
  );
};
export const HeaderWithGoBack = (onGoBack, title, customStyle = {}) => {
  return (
    <View style={[styles.headerView, customStyle]}>
      <TouchableOpacity onPress={onGoBack}>
        <Image
          source={IconAsset.ic_left}
          style={styles.icon_menu}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <View style={{ position: "absolute", left: 0, right: 0 }}>
        <Text style={styles.header_title}>{title}</Text>
      </View>
    </View>
  );
};

export const HeaderWithGoBackAndOption = (onGoBack, title, customStyle = {}) => {
  return (
    <View style={[styles.headerView1, customStyle]}>
    
      <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 0}}  colors={['#1c83a0', '#32cba5', ]} style={styles.headerImg}></LinearGradient>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          width: "100%",
         
        }}
      >
        <View>
          <TouchableOpacity onPress={() => Actions.drawerOpen()}>
            <Image
              source={IconAsset.ic_menu}
              style={styles.icon_menu}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>

        <View>
          <TouchableOpacity onPress={onGoBack}>
            <Image
              source={IconAsset.ic_right}
              style={styles.icon_list_type}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
export const HeaderWithOptionLogin = (onGoBack, title, customStyle = {}) => {
  return (
    <View style={[styles.headerView1, customStyle]}>
    
 <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 0}}  colors={['#1c83a0', '#32cba5', ]} style={styles.headerImg}></LinearGradient>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          width: "100%",
         
        }}
      >
      </View>
    </View>
  );
};
export const HeaderWithBackLogin = (onGoBack, title, customStyle = {}) => {
  return (
    <View style={[styles.headerView1, customStyle]}>
      <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 0}}  colors={['#1c83a0', '#32cba5', ]} style={styles.headerImg}></LinearGradient>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          width: "100%",
         
        }}
      >
        <View style={styles.icon_menu}></View>

        <View>
          <TouchableOpacity onPress={onGoBack}>
            <Image
              source={IconAsset.ic_right}
              style={styles.icon_list_type}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
export const HeaderWithOption = (customStyle = {}) => {
  return (
    <View style={[styles.headerView1, customStyle]}>
    
      <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 0}}  colors={['#1c83a0', '#32cba5', ]} style={styles.headerImg}></LinearGradient>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          width: "100%",
         
        }}
      >
        <View>
          <TouchableOpacity onPress={() => Actions.drawerOpen()}>
            <Image
              source={IconAsset.ic_menu}
              style={styles.icon_menu}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>

        <View style={styles.icon_list_type}></View>
      </View>
    </View>
  );
};