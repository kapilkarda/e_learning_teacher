export default {
  NETWORK_CONNECTION_CHANGED: "networkconnectionchanged",
  REFRESH_HOME_SCREEN: "refreshhomescreen",
  IS_LOGGED_IN: "isLoggedIn"
};
