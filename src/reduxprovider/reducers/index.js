import { combineReducers } from "redux";
import ReducerHomeScreen from "./ReducerHomeScreen";


// combine all reducers
const rootReducer = combineReducers({

  homeScreen: ReducerHomeScreen
});

export default rootReducer;
