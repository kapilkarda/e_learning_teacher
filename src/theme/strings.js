/***
 * APP strings goes here
 *
 */

export const english = {
  APP_NAME: "eLearning",
  HAVE_AN_ACCOUNT: "SIGN UP",
  SIGN_UP: "Sign Up",
  SIGN_IN: "Login",
  FORGOT_PASS: "Forgot Password ?",
  REMEMBER_ME: "Remember Me",
 CONFIRM:"Confirm",
 RESET:"Reset",
 CONFIRMATION:"Confirmation"
  
};
