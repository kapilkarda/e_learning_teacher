import { BASE_URL, CREATE_ADVERTISEMENT } from "../../constants/Constant";
import User from "../../models/User";

export const createAdvertisement = props => {
  console.log("createAdvertisement props",props);
  
  props.setLoadingState(true);

  return new Promise((resolve, reject) => {
    fetch(BASE_URL + CREATE_ADVERTISEMENT, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        AUTHTOKEN: currentUser.auth_token
      },
      body: getBody(props)
    })
      .then(response => response.json())
      .then(resJson => {
        props.setLoadingState(false);
        console.log("responseJson", resJson);
        if (resJson.response) {
          if (resJson.response.responseStatus) {
            reject(resJson.response.responseMessage);
          } 
        } else {
          resolve(resJson.advertisement);
        }
      })
      .catch(error => {
        props.setLoadingState(false);
        console.log("error", error);
        reject(error);
      });
  });
};

const getBody = ({ title, description }) => {
  var bodyparam = {
    "advertisement": {
      "title": title,
      "content": description
    }
  }
  return JSON.stringify(bodyparam);
};